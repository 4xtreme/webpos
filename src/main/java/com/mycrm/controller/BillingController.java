/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.dao.OrderBillinglistDAO;
import com.mycrm.dao.BillingDAO;
import com.mycrm.dao.InvoiceDAO;
import com.mycrm.dao.OrderInvoicelistDAO;
import com.mycrm.domain.Billing;

import com.mycrm.domain.Contact;
import com.mycrm.domain.Invoice;
import com.mycrm.domain.OrderBillinglist;
import com.mycrm.domain.OrderInvoicelist;
import com.mycrm.domain.Product;
import com.mycrm.domain.ReportManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/billing")
public class BillingController {

    @Autowired
    BillingDAO billingDAO;

    @Autowired
    OrderBillinglistDAO orderbillinglistDAO;

    @Autowired
    ProductDAO productDAO;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    InvoiceDAO invoiceDAO;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    OrderInvoicelistDAO orderInvoicelistDAO;
    private int user_id = 10;

    ReportManager getReport = new ReportManager();
    
    @Autowired
    EmailManager emailManager;
    public String errormessage;
    public String completemessage;
    
    List<OrderBillinglist> orderBillinglist = null;

    @RequestMapping(value = "/billinglist", method = RequestMethod.GET)
    public String billingList(Model model) {
        List<Billing> billinglist = billingDAO.getBillingList();
        model.addAttribute("completemessage", completemessage);
        model.addAttribute("errMessage", errormessage);
        errormessage = null;
        completemessage = null;
        System.out.println("Billing Size: " + billinglist.size());
        model.addAttribute("billinglist", billinglist);
        return "billing/billingList";
    }

    @RequestMapping(value = "/billingcreate", method = RequestMethod.GET)
    public String billingCreate(Model model, HttpServletRequest request, HttpServletResponse response) {
        List<Contact> contactlist = contactDAO.getcontactList(user_id);
        List<Product> productgetlist = productDAO.getproductList();

        System.out.println("Contactlist size: " + contactlist.size());
        System.out.println("Productgetlist size: " + productgetlist.size());

        model.addAttribute("productgetlist", productgetlist);
        model.addAttribute("contactlists", contactlist);
        // GenReport(request, response);

        return "billing/billingCreate";
    }

    @RequestMapping(value = "/tablelist", method = RequestMethod.POST)
    public String postdata(@RequestParam("duedate") String duedate, @RequestParam("selectcontact") int contactID, @RequestParam("itemslist") List<String> itemlist, Model model) throws ParseException {
        int billingID = 10;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date dueDate = formatter.parse(duedate);
        Billing addbilling = new Billing(0, contactID, user_id, 0, 0, false, new Date(), dueDate, false);
        billingID = billingDAO.insert(addbilling);
        System.out.println("Due-date: " + duedate);
        System.out.println("Selectcontact: " + contactID);
        System.out.println("BillingID: " + billingID);
        for (String number : itemlist) {
            System.out.print("Itemlist: ");
            String[] objSp = number.split(",");
            System.out.print(objSp[0]);
            System.out.print("|| Name: " + objSp[1]);
            System.out.print("|| Pirce: " + objSp[2]);
            System.out.print("|| QTY: " + objSp[3]);
            System.out.print("|| Sum: " + objSp[4]);
            System.out.print("|| Product ID: " + objSp[5]);
            System.out.println("");
            OrderBillinglist newItem = new OrderBillinglist(0, billingID, Integer.parseInt(objSp[5]), objSp[1], Integer.parseInt(objSp[2]), Integer.parseInt(objSp[3]), Integer.parseInt(objSp[4]));
            orderbillinglistDAO.insert(newItem);
        }
        return "redirect:/billinglist";
    }

    @RequestMapping(value = "/selectprint", method = RequestMethod.POST)
    public String selectedit(@RequestParam("selectid") String select, Model model, HttpServletRequest request, HttpServletResponse response) {
        System.out.println("Selected ID:" + select);
        getReport.genReport("Billing", select, request, response);
        return "null";
    }

    @RequestMapping(value = "/changeToInvoice", method = RequestMethod.POST)
    public String changestatus(@RequestParam("selectid") String select, Model model) {
        int selectID = Integer.parseInt(select);
        billingDAO.updateStatus(selectID);
        // Change Status 

        System.out.println("Selected ID:" + select);
        Billing billingInfo = billingDAO.findBillingId(selectID);

        Invoice copyInfo = new Invoice(0, billingInfo.getCustomer_id(), billingInfo.getUser_id(), billingInfo.getBilling_id(), billingInfo.getDiscount(), false, new Date(), new Date(), false);
        int invoiceId = invoiceDAO.insert(copyInfo);
        // Copy Info   Need to Set Due

        System.out.println("Id info: " + billingInfo.toString());
        System.out.println("Copy info: " + copyInfo.toString());
        System.out.println("");
        List<OrderBillinglist> orderlist = orderbillinglistDAO.getorderbillinglist(selectID);
        for (OrderBillinglist orderBillinglist : orderlist) {
            System.out.println("item: " + orderBillinglist.toString());
            OrderInvoicelist newItem = new OrderInvoicelist(0, invoiceId, orderBillinglist.getProduct_id(), orderBillinglist.getProduct_name(), orderBillinglist.getProduct_price(), orderBillinglist.getQuantity(), orderBillinglist.getSum_proce());
            orderInvoicelistDAO.insert(newItem);
            // Copy Product list
        }
        System.out.println("");

        return "redirect:/billinglist";
    }

    @RequestMapping(value = "/sendemail", method = RequestMethod.POST)
    public String sendmail(@RequestParam("selectid") final int selectID, Model model, final HttpServletRequest request) throws MessagingException {
             System.out.println("Email method");
        Billing billingInfo = billingDAO.findBillingId(selectID);
       
        // Change Email
        Contact contactinfo = contactDAO.findcontactInfo(billingInfo.getCustomer_id());
        String contactemail = contactinfo.getEmail();
        System.out.println("Selected ID: " + selectID);
        System.out.println("Send email to: " + contactinfo.getEmail());
        byte[] bytes = getReport.genReportEmail("Billing", selectID + "", request);
        DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
        String mailRespond = emailManager.sendReport(contactemail, "Billing", dataSource,  user_id);        
         if (mailRespond.equals("Complete")) {
                 billingDAO.updateEmail(selectID);
                 completemessage = "Send email "+mailRespond;
        }else{
          errormessage = mailRespond;
        }
        return "redirect:/billinglist";
    }

    @RequestMapping(value = "/searchbilling", method = RequestMethod.POST)
    public String searchinvoice(@RequestParam(value = "billingid", required = false) Integer billingid, @RequestParam(value = "customerid", required = false) Integer customerid, Model model) {
        List<Billing> billinglist = null;

        if (billingid == null && customerid != null) {
            billinglist = billingDAO.getCustomerID(customerid, user_id);
            System.out.println("in if" + billinglist);
        } else if (billingid != null && customerid == null) {
            System.out.println("in else");
            billinglist = billingDAO.getBillingListInVoiceID(billingid, user_id);
            System.out.println("in else**************" + billinglist);
        } else if (billingid == null && customerid == null) {
            System.out.println("in else if**************");
            billinglist = null;
        }

        System.out.println("Data is" + billinglist);
        model.addAttribute("billinglist", billinglist);

        return "billing/billingList";

    }

    @RequestMapping(value = "/editdatebilling", method = RequestMethod.POST)
    public String editdate(@RequestParam("selectbilling") int select, @RequestParam("setdate") String setdate, Model model) throws ParseException {
        System.out.println("Edit Create Date");
        System.out.println("Selected ID:" + select);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date fomatDate = formatter.parse(setdate);
        System.out.println("Set Date:" + fomatDate);
        billingDAO.setNewDate(select, fomatDate);

        return "redirect:/billinglist";

    }
    
       @RequestMapping(value = "/editbilling", method = RequestMethod.POST)
    public String selectedit(@RequestParam("editid") int editid, Model model) {
      orderBillinglist = orderbillinglistDAO.getorderbillinglist(editid);
         System.out.println("****************"+orderBillinglist);
         model.addAttribute("orderBillinglist", orderBillinglist);
         return "billing/billingEdit";
    }
    
      @RequestMapping(value = "/editsave", method = RequestMethod.POST)
    public String selecteditsave(@RequestParam("productname") List<String>  productname,@RequestParam("productprice") List<Integer>  productprice,@RequestParam("quantity") List<Integer>  quantity,@RequestParam("sumproce") List<Integer> sumproce,@RequestParam("orid") List<Integer> orid,@RequestParam("qtid") List<Integer> qtid,@RequestParam("prid") List<Integer> prid,Model model) {
    
          for (int i=0; i<productname.size(); i++) {
             
           OrderBillinglist orlist = new OrderBillinglist(orid.get(i), qtid.get(i), prid.get(i), productname.get(i),productprice.get(i), quantity.get(i), sumproce.get(i));
             System.out.println("****************"+orlist.toString());
             //DAO UPDATE
             orderbillinglistDAO.update(orlist);
          }
          completemessage = "Edit Complete";
     
    return "redirect:/billinglist";
    }
    
    @RequestMapping(value="/customerbase", method = RequestMethod.GET)
    public String billingCustomerbase(Model model){
        return "billing/customerbase";
    }
    
    @RequestMapping(value="/addcustomer", method = RequestMethod.GET)
    public String billingAddcustomer(Model model){
        return "billing/addcustomer";
    }
    
    @RequestMapping(value="/addcustomerbase", method = RequestMethod.GET)
    public String billingAddcustomerbase(Model model){
        return "billing/addcustomerbase";
    }
    
    @RequestMapping(value="/notificationsystem", method = RequestMethod.GET)
    public String billingNotificationsystem(Model model){
        return "billing/notificationsystem";
    }
    
    @RequestMapping(value="/addnotifications", method = RequestMethod.GET)
    public String billingAddnotifications(Model model){
        return "billing/addnotifications";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.ContactDAO;
import com.mycrm.domain.Contact;
import com.mycrm.domain.Product;
import java.text.ParseException;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping(value = "/contact")
public class ContactController {

    @Autowired
    ContactDAO contactDAO;
    private int user_id = 10;

    @RequestMapping(value = "/contactAll", method = RequestMethod.GET)
    public String contactAll(Model model) {
        List<Contact> contactlist = contactDAO.getcontactList(user_id);
        System.out.println("Controler" + contactlist.toString());

        model.addAttribute("contactlists", contactlist);
        return "contact/contactAll";
    }

    @RequestMapping(value = "/contactCreate", method = RequestMethod.GET)
    public String contactCreate(Model model) {
        model.addAttribute("Contact", new Contact());
        model.addAttribute("action", "contactAdd");
        return "contact/contactCreate";
    }

    @RequestMapping(value = "/contactAdd", method = RequestMethod.POST)
    public String create(Model model, @Valid @ModelAttribute("Contact") Contact contact, BindingResult result) {
        System.out.println("Contact**************" + contact.toString());
        contact.setUser_id(user_id);
        contact.setIs_delete(false);
        contactDAO.insert(contact);
        return "contact/contactCreate";

    }

    @RequestMapping(value = "/selectedit", method = RequestMethod.POST)
    public String selectedite(@RequestParam("editid") int editid, Model model) {

        System.out.println("Selectedit ID:" + editid);
        Contact editContact = contactDAO.findcontactInfo(editid);
        System.out.println("controler" + editContact.toString());
        model.addAttribute("Contact", editContact);
        model.addAttribute("action", "contactedit");
        model.addAttribute("customerId", editContact.getCustomer_id());
        model.addAttribute("userId", editContact.getUser_id());

        return "contact/contactCreate";
    }

    @RequestMapping(value = "/contactedit", method = RequestMethod.POST)
    public String edite(Model model, @Valid @ModelAttribute("Contact") Contact contact, BindingResult result) {
        System.out.println("controler" + contact.toString());
        contactDAO.update(contact);
        return "redirect:/contactAll";
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(@RequestParam("id") Integer id) {
        
        System.out.println("contact id =" + id);
         contactDAO.deleteContact(id);
       
        
        return "redirect:/contactAll";
    }
    
     @RequestMapping(value="/product_sales", method = RequestMethod.GET)
    public String contactProduct_sales(Model model){
        return "contact/product_sales";
    }

    @RequestMapping(value="/receipt", method = RequestMethod.GET)
    public String contactReceipt(Model model){
        return "contact/receipt";
    }

     @RequestMapping(value="/salesby_category", method = RequestMethod.GET)
    public String contactSalesby_category(Model model){
        return "contact/salesby_category";
    }

     @RequestMapping(value="/salesby_additional", method = RequestMethod.GET)
    public String contactSalesby_additional(Model model){
        return "contact/salesby_additional";
    }

     @RequestMapping(value="/salesby_payment", method = RequestMethod.GET)
    public String contactSalesby_payment(Model model){
        return "contact/salesby_payment";
    } 
    
     @RequestMapping(value="/discount", method = RequestMethod.GET)
    public String contactDiscount(Model model){
        return "contact/discount";
    }  
    
     @RequestMapping(value="/tax", method = RequestMethod.GET)
    public String contactTax(Model model){
        return "contact/tax";
    } 
     @RequestMapping(value="/employeereport", method = RequestMethod.GET)
    public String contactEmployeereport(Model model){
        return "contact/employeereport";
    } 
     @RequestMapping(value="/showcontact", method = RequestMethod.GET)
    public String contactShowcontactt(Model model){
        return "contact/showcontact";
    } 
    
             
}

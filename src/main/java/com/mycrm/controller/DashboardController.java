/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.BillingDAO;
import com.mycrm.dao.InvoiceDAO;
import com.mycrm.dao.QuotationDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Invoice;
import com.mycrm.domain.Quotation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DashboardController {
 private int user_id = 10;
    @Autowired
    BillingDAO billingDAO;

    @Autowired
    InvoiceDAO invoiceDAO;

    @Autowired
    QuotationDAO quatationDAO;

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String Dashboard(Model model) {
        List<Billing> billinglist = billingDAO.getBillingList();
        System.out.println("*****************************" + billinglist);
        model.addAttribute("billinglist", billinglist);

        List<Invoice> invoicelist = invoiceDAO.getInvoiceList(user_id);
        System.out.println("*****************************" + invoicelist);
        model.addAttribute("invoicelist", invoicelist);

        List<Quotation> quatationlist = quatationDAO.getQuotationList();
        System.out.println("*****************************" + quatationlist);
        model.addAttribute("quatationlist", quatationlist);

        return "dashboard/dashboard";

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;
import com.mycrm.dao.EmailTopicDAO;
import com.mycrm.dao.EmailSenderDAO;
import com.mycrm.domain.EmailSender;
import com.mycrm.domain.EmailSender;
import com.mycrm.domain.EmailTopic;
import java.util.Properties;
import javax.activation.DataSource;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Lenovo
 */
public class EmailManager {

    public EmailManager() {
    }
    
    @Autowired
    private JavaMailSender mailSender;
    
    @Autowired
    EmailSenderDAO emailsenderDAO;
    
    @Autowired
    EmailTopicDAO emailtopicDAO;
    
      public EmailSender checkUserMail(int userid){
           System.out.println("Check UserMail" +userid);
        EmailSender getemail = emailsenderDAO.findUserId(userid);        
        System.out.println("Get info: s" + getemail);
        if(getemail.getEmail() == null){
            System.out.println("Null emaill");
             return null;
        }else{
            System.out.println("Have email");        
             return getemail;
        }
        
    }
    
       public EmailTopic checkTopic(String reportType){
           EmailTopic getTopic = emailtopicDAO.findTopic_id(1);       
            if (getTopic.getTitle() != null){                              
                System.out.println("Use this Topic \nGet info:" + getTopic);
                return getTopic;
           } else {               
                 System.out.println("Use Default");
               return new  EmailTopic(0, "MyCRM"+reportType+"Report", "This is Report PDF file", reportType);
           }   
        } 
        public String sendReport(final String toEmail,final String reportType, final DataSource dataSource, int  userid){
                EmailSender userInfo =  checkUserMail(userid);
                System.out.println("EmailManager Get data: " + toEmail+", "+reportType);
                JavaMailSender emailTransform = mailSender;    
                final EmailTopic getTopic = checkTopic(reportType);
       
            
                 if (userInfo != null) {
                     System.out.println("UserInfo Not null chang JavaMailSender properties.");
                     emailTransform = emailConfig(userInfo);
                 }
        try {
            emailTransform.send(new MimeMessagePreparator() {
                   @Override
                   public void prepare(MimeMessage mimeMessage) throws Exception {
                       MimeMessageHelper toemail = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                       toemail.setTo(toEmail);
                       toemail.setSubject(getTopic.getTitle());
                       toemail.setText(getTopic.getDetail());  
                       toemail.addAttachment("RepoetCRM_"+reportType+".pdf", dataSource);
                    }
                   });
            
        } catch (Exception e) {
            
            System.out.println("Error Can't send email!\n" + e);
             return  "Can't send email!  Please check your \" Email sender \" on Setting menu";
        }
        System.out.println("**********End EmailManager System**********");
        return  "Complete";
    }
        
        public JavaMailSender emailConfig(final EmailSender settingEmail){
    
        JavaMailSenderImpl maillconfig = new JavaMailSenderImpl();
        System.out.println("Set new email");
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", settingEmail.getPort()+""); // 587 or 465
        Session session = Session.getInstance(properties,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(settingEmail.getEmail(), settingEmail.getPassword());
			}
		  });
    
        maillconfig.setJavaMailProperties(properties);
        maillconfig.setSession(session);
        System.out.println("end set Start send");
        
        return maillconfig;
    }    
}

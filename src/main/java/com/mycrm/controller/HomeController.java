/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.UserDAO;

import com.mycrm.domain.User;
import javax.inject.Scope;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
 
@Controller
@Component
public class HomeController {
     private static int user_id = 0;

    public static int getUser_id() {
        return user_id;
    }

   
    @Autowired
    UserDAO userDAO;
       
     @RequestMapping(value = "/", method = RequestMethod.GET)
	public String loginpage() {
		return "jsp/login";
	}
        
      @RequestMapping(value = "/home", method = RequestMethod.GET)
	public String printWelcome() {
            System.out.println("Get user id" + user_id);
		return "jsp/home";
	}   
         @RequestMapping(value = "/login", method = RequestMethod.POST)
	public String check(@RequestParam(value = "email", required = true) String email,@RequestParam(value = "password", required = true) String password){
             System.out.println("---------------Login---------------");
             System.out.println("Email:    " + email);
             System.out.println("password: " + password);
//             User userlogin = new User();
            try {
                  User userprofile = userDAO.emailfindUser(email);                 
                  System.out.println("User ID: "+userprofile.getUser_id()+"\nUser name: " + userprofile.fullName() + "\nPass: " + userprofile.getPassword());
                  if(password.equals(userprofile.getPassword())){
                      this.user_id = userprofile.getUser_id();
                      System.out.println("-------------------------------------");
                      return "redirect:/home";
                  }
             } catch (Exception e) {
                 return "redirect:/";
             }
             
                return "redirect:/";
        }
    
  
}

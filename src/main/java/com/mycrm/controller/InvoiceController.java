/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.EmailSenderDAO;
import com.mycrm.dao.InvoiceDAO;
import com.mycrm.dao.OrderInvoicelistDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Contact;
import com.mycrm.domain.EmailSender;
import com.mycrm.domain.Invoice;
import com.mycrm.domain.OrderBillinglist;
import com.mycrm.domain.OrderInvoicelist;
import com.mycrm.domain.Orderlist;
import com.mycrm.domain.Product;
import com.mycrm.domain.Quotation;
import com.mycrm.domain.ReportManager;
import com.mysql.jdbc.Connection;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
 
@Controller
@RequestMapping(value = "/invoice")
public class InvoiceController {
 private int user_id = 10;

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    ProductDAO productDAO;

    @Autowired
    InvoiceDAO invoiceDAO;

    @Autowired
    OrderInvoicelistDAO orderInvoicelistDAO;
    
    @Autowired
    private JavaMailSender mailSender;
    
    @Autowired
    EmailSenderDAO emailsenderDAO;
    
    ReportManager getReport = new ReportManager();
    
    @Autowired
    EmailManager emailManager;
 
    public String errormessage;
    public String completemessage;
    List<OrderInvoicelist> orderinvoicelist = null;
    @RequestMapping(value="/invoicelist", method = RequestMethod.GET)
    public String invoiceList(Model model){
       List<Invoice> invoicelist = invoiceDAO.getInvoiceList(user_id);

       
//        EmailSender addnewmail = new EmailSender(10, 3, "hunterauthen@gmail.com", "mim12470", 587);
//        emailsenderDAO.insert(addnewmail);
        model.addAttribute("completemessage", completemessage);
        model.addAttribute("errMessage", errormessage);
        errormessage = null;
        completemessage = null;
        System.out.println("Invoice size: " + invoicelist.size());
        System.out.println("Invoice ID(index0): " + invoicelist.get(0).getInvoice_id());
         model.addAttribute("invoicelist", invoicelist);
        return "invoice/invoiceList";
    }
    @RequestMapping(value="/invoicecreate", method = RequestMethod.GET)
    public String invoiceCreate(Model model){
              List<Contact> contactlist = contactDAO.getcontactList(user_id);
         List<Product> productgetlist = productDAO.getproductList();
         
         System.out.println("Contactlist size: " + contactlist.size());
         System.out.println("Productgetlist size: " + productgetlist.size());
         
         model.addAttribute("productgetlist", productgetlist);
         model.addAttribute("contactlists", contactlist);
        
        return "invoice/invoiceCreate";
    }
    
    @RequestMapping(value="/invoicetablelist", method = RequestMethod.POST)
       public String selectedit(@RequestParam("duedate") String duedate,@RequestParam("selectcontact") int contactID,@RequestParam("testcreatename") String contact, Model model) throws ParseException {
        int invoiceId;
        System.out.println("duedate" +duedate);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date dueDate = formatter.parse(duedate);
        Invoice qq = new Invoice(0, contactID, user_id, 0, 0, false,new Date(),dueDate,false);
        invoiceId = invoiceDAO.insert(qq);
        System.out.println("Selectcontact: " + contact);
        String[] parts = contact.split(",#,");
        for (String part : parts) {
            System.out.println("Get: " + part);
             String[] objSp = part.split(",");
             OrderInvoicelist newItem = new OrderInvoicelist(0, invoiceId, Integer.parseInt(objSp[5]), objSp[1], Integer.parseInt(objSp[2]), Integer.parseInt(objSp[3]), Integer.parseInt(objSp[4]));
             orderInvoicelistDAO.insert(newItem);             
             System.out.println("ID: "+ objSp[0]);
             System.out.println("Name: "+ objSp[1]);
             System.out.println("Pirce: "+ objSp[2]);
             System.out.println("QTY: "+ objSp[3]);
             System.out.println("Sum: "+ objSp[4]);
             System.out.println("Product ID: "+ objSp[5]);
              objSp = null;
              System.out.println("--------------------");
             
        }
        
        //selectcontact
         return "invoice/invoiceCreate";
          
    }
     
      
    @RequestMapping(value = "/printinvoice", method = RequestMethod.POST)
    public String selectedit(@RequestParam("selectid") String select, Model model,HttpServletRequest request,HttpServletResponse response) {

        System.out.println("Selected ID:" + select);
        getReport.genReport("Invoice", select, request, response);
        System.out.println("End Print");
        return "redirect:/invoicelist";

    }
    
        @RequestMapping(value = "/sendemailinvoice", method = RequestMethod.POST)
    public String sendmail(@RequestParam("selectid") final int selectID, Model model,final HttpServletRequest request) throws MessagingException {
        System.out.println("Email method");

        Invoice invoiceInfo = invoiceDAO.findinvoiceId(selectID);        

        System.out.println("Get invoice Info: " + invoiceInfo);
        Contact contactinfo = contactDAO.findcontactInfo(invoiceInfo.getCustomer_id());
        
        String contactemail = contactinfo.getEmail();
        System.out.println("Selected ID: " + selectID);       
        System.out.println("Send email to: " + contactemail); 
        
        byte[] bytes = getReport.genReportEmail("Invoice", selectID+"", request);
        DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
           System.out.println("-----------------------------------------------------------");
        String mailRespond = emailManager.sendReport(contactemail, "Invoice", dataSource,  user_id);
        if (mailRespond.equals("Complete")) {
                invoiceDAO.updateEmail(selectID);  
                completemessage = "Send email "+mailRespond;
        }else{
          errormessage = mailRespond;
        }
        return "redirect:/invoicelist";
    }    
 
    @RequestMapping(value = "/changestatusinvoice", method = RequestMethod.POST)
    public String changestatus(@RequestParam("selectid") String select, Model model) {
        int selectID = Integer.parseInt(select);
         invoiceDAO.updateStatus(selectID);    
      
              
          return "redirect:/invoicelist";
    }
    
    @RequestMapping(value = "/editdate", method = RequestMethod.POST)
    public String editdate(@RequestParam("selectinvoice") int select,@RequestParam("setdate") String setdate, Model model) throws ParseException {
        System.out.println("Edit Create Date");
        System.out.println("Selected ID:" + select);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date fomatDate = formatter.parse(setdate);
        System.out.println("Set Date:" + fomatDate);
        invoiceDAO.setNewDate(select, fomatDate);
        
        return "redirect:/invoicelist";

    }
    
      @RequestMapping(value = "/searchinvoice", method = RequestMethod.POST)
    public String searchinvoice(@RequestParam(value = "invoiceid", required = false) Integer invoiceid, @RequestParam(value = "customerid", required = false) Integer customerid, Model model) {
        List<Invoice> invoicelist =null;
        
        System.out.println("Selected ID:" + invoiceid);  
        System.out.println("Selected ID:" + customerid);
        
         if (invoiceid == null && customerid != null) {
            invoicelist = invoiceDAO.getInvoiceListCustomerID(customerid, user_id);
            System.out.println("in if" + invoicelist);
        } else if (invoiceid != null && customerid == null) {
            System.out.println("in else");
            invoicelist = invoiceDAO.getInvoiceListInVoiceID(invoiceid, user_id);
            System.out.println("in else**************" + invoicelist);
        } else if (invoiceid == null && customerid == null) {
            System.out.println("in else if**************");
            invoicelist = null;
        }
       
        System.out.println("Data is"+invoicelist);
       model.addAttribute("invoicelist", invoicelist);
          
        
        return "invoice/invoiceList";

    }
    
        @RequestMapping(value = "/editinvoice", method = RequestMethod.POST)
    public String selectedit(@RequestParam("editid") int editid, Model model) {
      orderinvoicelist = orderInvoicelistDAO.getorderinvoicelistt(editid);
         System.out.println("****************"+orderinvoicelist);
         model.addAttribute("orderinvoicelist", orderinvoicelist);
         return "invoice/invoiceEdit";
    }
    
      @RequestMapping(value = "/editqtsave", method = RequestMethod.POST)
    public String selecteditsave(@RequestParam("productname") List<String>  productname,@RequestParam("productprice") List<Integer>  productprice,@RequestParam("quantity") List<Integer>  quantity,@RequestParam("sumproce") List<Integer> sumproce,@RequestParam("orid") List<Integer> orid,@RequestParam("qtid") List<Integer> qtid,@RequestParam("prid") List<Integer> prid,Model model) {
    
          for (int i=0; i<productname.size(); i++) {
             
           OrderInvoicelist orlist = new OrderInvoicelist(orid.get(i), qtid.get(i), prid.get(i), productname.get(i),productprice.get(i), quantity.get(i), sumproce.get(i));
             System.out.println("****************"+orlist.toString());
             //DAO UPDATE
             orderInvoicelistDAO.update(orlist);
          }
          completemessage = "Edit Complete";
     
    return "redirect:/invoicelist";
    }
    
       @RequestMapping(value = "/rating", method = RequestMethod.GET)
    public String invoiceRating (Model model) {
      return "invoice/rating";
    }
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String invoiceTest (Model model) {
      return "invoice/test";
    }
}

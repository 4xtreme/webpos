/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.mycrm.dao.MachineDAO;
import com.mycrm.domain.Machine;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author pop
 */
@Controller
public class MachineController {

    @Autowired
    MachineDAO machineDAO;
    
    @RequestMapping(value = "/addmachine", method = RequestMethod.GET)
    public String addMachine(Model model) {
        model.addAttribute("machine", new Machine());

        return "machine/addmachine";
    }

    @RequestMapping(value = "/addmachine", method = RequestMethod.POST)
    public String addMachineAction(Model model,  Machine machine, BindingResult result,
            @RequestParam(value = "machinepicture")MultipartFile machine_pic,
            @RequestParam(value = "machinefile")MultipartFile machine_file) {
        machine.setMachine_pic(uploadFile(machine_pic));
        machine.setMachine_file(uploadFile(machine_file));
        System.out.println("info ========> " + machine.toString());
        machineDAO.insert(machine);
        return "machine/addmachine";
    }
    
    @RequestMapping(value = "/addtest", method = RequestMethod.POST)
    public String addTest(@RequestParam(value = "jame")String jame) {
        System.out.println("info ========> " + jame);
        return "machine/addmachine";
    }

    public String uploadFile(MultipartFile file) {

        //Amazon set Credentials 
        AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials("AKIAIVWJITLXBJK5M2UQ", "vIpbKGkoY3vJa7KP/Z8V5VjGm7SFXMxeBn/0iJcf"));
        s3client.setRegion(Region.getRegion(Regions.fromName("ap-southeast-1")));
        String bucketname = "fidesla";

        //Rename Name: file + datetime + filetype
        String filename = file.getOriginalFilename();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMddHHmmssSSS");
        String nowTime = dateFormat.format(cal.getTime());
        int spaceIndex = filename.indexOf(".");
        int setendName = spaceIndex;
        if (setendName > 15) {
            setendName = 15;
        }
        String newfilename = filename.substring(0, setendName) + nowTime + filename.substring(spaceIndex);
        System.out.println("datetime: " + nowTime);
        System.out.println("Original name: " + filename);
        System.out.println("New file name: " + newfilename);
        String filelink = "";

        //Start upload process
        try {
            s3client.putObject(new PutObjectRequest(bucketname, newfilename, file.getInputStream(), new ObjectMetadata())
                    .withCannedAcl(CannedAccessControlList.Private));
            System.out.println("Upload complete!");
            S3Object s3Object = s3client.getObject(new GetObjectRequest(bucketname, newfilename));

            filelink = s3Object.getObjectContent().getHttpRequest().getURI().toString();

            System.out.println("Link: " + filelink);

        } catch (IOException ex) {
            ;
        }

        return filelink;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.ProductDAO;
import com.mycrm.domain.Product;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
 
@Controller
@RequestMapping(value = "/product")
public class ProductController {
   private int userId = 10;
    
   @Autowired
    ProductDAO productDAO;
 
    @RequestMapping(value="/productList", method = RequestMethod.GET)
    public String productList(Model model){

         System.out.println("------------Start  List-------------");
         List<Product> productgetlist =productDAO.getproductList();
         System.out.println("Get list size:" + productgetlist.size());
         model.addAttribute("productgetlist", productgetlist);
         System.out.println("Model: " + model.toString());
         
        return "product/productList";
    }
    
    @RequestMapping(value="/productCreate", method = RequestMethod.GET)
    public String productCreate(Model model){
        model.addAttribute("product", new Product());
        model.addAttribute("producttitle", "Create new product");
        model.addAttribute("butt", "add");
        model.addAttribute("action", "productCreate");
        return "product/productCreate";
    }

    @RequestMapping(value = "/productCreate", method = RequestMethod.POST)
    public String create(@Valid Product product, BindingResult result,@RequestParam("startdate") String startD,@RequestParam("enddate") String endD) throws ParseException {
        System.out.println("Post data: " + product.toString());              
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date startdate = formatter.parse(startD);
        Date endtdate = formatter.parse(endD);
        product.setUser_id(userId);
        product.setCreate_date(new Date());
        product.setSell_start_date(startdate);
        product.setSell_end_date(endtdate);
//        Product testp = new Product(10, 10, "TestName", "detail", "Red", "15x28", 1000, 20, new Date(), new Date(), new Date());
        productDAO.insert(product);
 

        return "redirect:/product/productList";
    }
    
    
    @RequestMapping(value = "/selectedit", method = RequestMethod.POST)
    public String selectedit(@RequestParam("editid") int editid, Model model) {

        System.out.println("Selectedit ID:" + editid);
        Product editP = productDAO.findByProductId(editid);
        System.out.println("Before Edit Get info: " + editP.toString());
        model.addAttribute("product", editP);
        model.addAttribute("producttitle", "Edit product");
        model.addAttribute("butt", "save");
        model.addAttribute("action", "productedit");
        model.addAttribute("productid", editP.getProduct_id());
        model.addAttribute("userid", editP.getUser_id());
         
        return "product/productCreate";
    }
    
     @RequestMapping(value = "/productedit", method = RequestMethod.POST)
    public String edit(@Valid Product product, BindingResult result,@RequestParam("startdate") String startD,@RequestParam("enddate") String endD) throws ParseException {
        System.out.println("Set Edit data: " + product.toString());              
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date startdate = formatter.parse(startD);
        Date endtdate = formatter.parse(endD);
        product.setUser_id(userId);
        product.setCreate_date(new Date());
        product.setSell_start_date(startdate);
        product.setSell_end_date(endtdate);
        productDAO.update(product);
 

        return "redirect:/product/productList";
    }
    
    @RequestMapping(value="/discountcreate", method = RequestMethod.GET)
    public String productDiscountcreate(Model model){
        return "product/discountcreate";
    }
    
     @RequestMapping(value="/discountlist", method = RequestMethod.GET)
    public String productDiscountlist(Model model){
        return "product/discountlist";
    }

      @RequestMapping(value="/modifiers", method = RequestMethod.GET)
    public String productModifiers(Model model){
        return "product/modifiers";
    }
    
       @RequestMapping(value="/modifiercreate", method = RequestMethod.GET)
    public String productModifiercreate(Model model){
        return "product/modifiercreate";
    }
    
      @RequestMapping(value="/categories", method = RequestMethod.GET)
    public String productCategories(Model model){
        return "product/categories";
    }
    
       @RequestMapping(value="/categorycreate", method = RequestMethod.GET)
    public String productCategorycreate(Model model){
        return "product/categorycreate";
    }
         @RequestMapping(value="/dashboardnew", method = RequestMethod.GET)
    public String productDashboardnew(Model model){
        return "product/dashboardnew";
    }
         @RequestMapping(value="/goods", method = RequestMethod.GET)
    public String productGoods(Model model){
        return "product/goods";
    }

  
}

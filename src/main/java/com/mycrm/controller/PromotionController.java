/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.BillingDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.InvoiceDAO;
import com.mycrm.dao.PromotionDAO;
import com.mycrm.dao.QuotationDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Contact;
import com.mycrm.domain.Invoice;
import com.mycrm.domain.Product;
import com.mycrm.domain.Promotion;
import com.mycrm.domain.Quotation;

import com.mycrm.domain.User;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Scope;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@Component
public class PromotionController {
 @Autowired
    private JavaMailSender mailSender;
   @Autowired
    ContactDAO contactDAO;
   
    @Autowired
    PromotionDAO promotionDAO;
    private int user_id = 10;

    @RequestMapping(value = "/promotion", method = RequestMethod.GET)
    public String serch(Model model) {
           List<Contact> contactlist = contactDAO.getcontactList(user_id);
        System.out.println("Controler" + contactlist.toString());

        model.addAttribute("contactlists", contactlist);
      
        return "promotion/promotion";
    }
       
    
     @RequestMapping(value = "/productlist", method = RequestMethod.GET)
    public String serchlist(Model model) {
        
      List<Promotion> promotionlist =   promotionDAO.getpromotiontList();
       model.addAttribute("promotionlist", promotionlist);
           
      
        return "promotion/promotionList";
    }

 @RequestMapping(value = "/sendemailcontact", method = RequestMethod.POST)
    public String sendmail(@RequestParam("topice")  final String topice,@RequestParam("detail")  final String detail,@RequestParam("typect")  final String typect, Model model) throws MessagingException {
        System.out.println("***************"+detail);
        List<Contact> contactlist = contactDAO.getemailList(typect);   
        System.out.println("Size" + contactlist.size());
         String emailName = "";

       for (Contact contact : contactlist) {
           
             emailName += contact.getEmail(); 
             
            emailName = emailName+",";
         
     }
       emailName = emailName.substring(0, emailName.length()-1);      
       final String[] test = emailName.split(",");
       System.out.println("email = " +test[0]); 
   
        mailSender.send(new MimeMessagePreparator() {
            @Override
            protected Object clone() throws CloneNotSupportedException {
                return super.clone(); //To change body of generated methods, choose Tools | Templates.
            }

            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper toemail = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                toemail.setTo(test);
                toemail.setSubject(topice);
                toemail.setText(detail);

            }
        });
        
         Promotion promotion = new Promotion(1,typect,topice,detail,new Date());
        promotionDAO.insert(promotion);
       
        return "promotion/promotion";
    }
    
      @RequestMapping(value = "/selecteditpromotion", method = RequestMethod.POST)
    public String selectedit(@RequestParam("editid") int editid, Model model) {

      
        Promotion editP = promotionDAO.findByProductId(editid);
        System.out.println("Before Edit Get info: " + editP.toString());
         model.addAttribute("customertype", editP.getCustomer_type());
          model.addAttribute("topic", editP.getTopic());
           model.addAttribute("detail", editP.getDetail());
         return "promotion/promotionDetail";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.BillingDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.OrderBillinglistDAO;
import com.mycrm.dao.OrderlistDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.dao.QuotationDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Contact;
import com.mycrm.domain.OrderBillinglist;
import com.mycrm.domain.Orderlist;
import com.mycrm.domain.Product;
import com.mycrm.domain.Quotation;
import com.mycrm.domain.ReportManager;
import com.mysql.jdbc.Connection;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
 
@Controller
@RequestMapping(value = "/quotation")
public class QuotationController {
    @Autowired
    ContactDAO contactDAO;

    @Autowired
    ProductDAO productDAO;
    
    @Autowired
    OrderlistDAO orderlistDAO;
    
    @Autowired
    QuotationDAO quatationDAO;
    private int user_id = 10;
    
    @Autowired
    BillingDAO billingDAO;
    
     @Autowired
    OrderBillinglistDAO orderbillinglistDAO;
     
     @Autowired
    private JavaMailSender mailSender;
    
    ReportManager getReport = new ReportManager();
    
    @Autowired
    EmailManager emailManager;
    public String errormessage;
    public String completemessage;
 List<Orderlist> orderlist = null;
    @RequestMapping(value="/quotationList", method = RequestMethod.GET)
    public String quotationList(Model model){
        model.addAttribute("completemessage", completemessage);
        model.addAttribute("errMessage", errormessage);
        errormessage = null;
        completemessage = null;
        List<Quotation> quatationlist = quatationDAO.getQuotationList();
        System.out.println("quatationlist Size: " + quatationlist.size());
        model.addAttribute("quatationlist", quatationlist);
        
        
       
        return "quotation/quotationList";
    }
     @RequestMapping(value="/quotationCreate", method = RequestMethod.GET)
    public String quotationCreate(Model model){
         List<Contact> contactlist = contactDAO.getcontactList(user_id);
         List<Product> productgetlist = productDAO.getproductList();
         
         System.out.println("Contactlist size: " + contactlist.size());
         System.out.println("Productgetlist size: " + productgetlist.size());
         
         model.addAttribute("productgetlist", productgetlist);
         model.addAttribute("contactlists", contactlist);
        return "quotation/quotationCreate";
    }
    
    @RequestMapping(value="/tablelist", method = RequestMethod.POST)
       public String selectedit(@RequestParam("duedate") String duedate,@RequestParam("selectcontact") int contactID,@RequestParam("testcreatename") String contact, Model model) throws ParseException {
        int qId;
        System.out.println("duedate" +duedate);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date dueDate = formatter.parse(duedate);
        Quotation qq = new Quotation(0, contactID, user_id, 0, false,new Date(),dueDate,false);
        qId = quatationDAO.insert(qq);
        System.out.println("Selectcontact: " + contact);
        String[] parts = contact.split(",#,");
        for (String part : parts) {
            System.out.println("Get: " + part);
             String[] objSp = part.split(",");
             Orderlist newItem = new Orderlist(0, qId, Integer.parseInt(objSp[5]), objSp[1], Integer.parseInt(objSp[2]), Integer.parseInt(objSp[3]), Integer.parseInt(objSp[4]));
             orderlistDAO.insert(newItem);             
             System.out.println("ID: "+ objSp[0]);
             System.out.println("Name: "+ objSp[1]);
             System.out.println("Pirce: "+ objSp[2]);
             System.out.println("QTY: "+ objSp[3]);
             System.out.println("Sum: "+ objSp[4]);
             System.out.println("Product ID: "+ objSp[5]);
              objSp = null;
              System.out.println("--------------------");
             
        }        
        //selectcontact
             return "redirect:/quotation/quotationList";
    }
 
    @RequestMapping(value = "/selectprint", method = RequestMethod.POST)
    public String selectedit(@RequestParam("selectid") String select, Model model,HttpServletRequest request,HttpServletResponse response) {
        

        System.out.println("Selected ID:" + select);        
        System.out.println("Print response" + response);        
        getReport.genReport("Quotation",select,request, response);
         
        return "null";
    }
    
    @RequestMapping(value = "/changeToBilling", method = RequestMethod.POST)
    public String changestatus(@RequestParam("selectid") String select, Model model) {
        int selectID = Integer.parseInt(select);
         quatationDAO.updateStatus(selectID);    
        // Change Status 
        
        System.out.println("Selected ID:" + select);        
        Quotation billingInfo = quatationDAO.findQuotationId(selectID);
        
        Billing copyInfo = new Billing(0, billingInfo.getCustomer_id(), billingInfo.getUser_id(), billingInfo.getQuotation_id(), billingInfo.getDiscount(), false,new Date(),new Date(),false);
        int billingId = billingDAO.insert(copyInfo);
         // Copy Info   Need to Set Due
         
        System.out.println("Id info: " + billingInfo.toString());
        System.out.println("Copy info: " + copyInfo.toString());
        System.out.println("");
        List<Orderlist> orderlist = orderlistDAO.getorderlist(selectID);
        for (Orderlist orderQuatationlist : orderlist) {
            System.out.println("item: " + orderQuatationlist.toString());            
            OrderBillinglist newItem = new OrderBillinglist(0, billingId, orderQuatationlist.getProduct_id(), orderQuatationlist.getProduct_name(), orderQuatationlist.getProduct_price(), orderQuatationlist.getQuantity(), orderQuatationlist.getSum_proce());
            orderbillinglistDAO.insert(newItem);
            // Copy Product list
        }
        System.out.println("");
              
         return "redirect:/billinglist";
    }
    
     @RequestMapping(value = "/sendemailQT", method = RequestMethod.POST)
    public String sendmail(@RequestParam("selectid") final int selectID, Model model,final HttpServletRequest request) throws MessagingException {
        System.out.println("Email method");
        Quotation quotationInfo = quatationDAO.findQuotationId(selectID);
        
         
        // Change Email 
        Contact contactinfo = contactDAO.findcontactInfo(quotationInfo.getCustomer_id());
        final String contactemail = contactinfo.getEmail();
        System.out.println("Selected ID: " + selectID);       
        System.out.println("Send email to: " + contactinfo.getEmail()); 
        byte[] bytes = getReport.genReportEmail("Quotation", selectID+"", request);
        final DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
        String mailRespond = emailManager.sendReport(contactemail, "Quotation", dataSource,  user_id);        
        if (mailRespond.equals("Complete")) {
                quatationDAO.updateEmail(selectID);  
                completemessage = "Send email "+mailRespond;
        }else{
          errormessage = mailRespond;
        }
     
        return "redirect:/quotation/quotationList";
    }    
    
    
       @RequestMapping(value = "/editdatequuotation", method = RequestMethod.POST)
    public String editdate(@RequestParam("selectquotation") int select,@RequestParam("setdate") String setdate, Model model) throws ParseException {
        System.out.println("Edit Create Date");
        System.out.println("Selected ID:" + select);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date fomatDate = formatter.parse(setdate);
        System.out.println("Set Date:" + fomatDate);
        quatationDAO.setNewDate(select, fomatDate);
        
        return "redirect:/quotation/quotationList";

    }
    
      @RequestMapping(value = "/searchquotation", method = RequestMethod.POST)
    public String searchinvoice(@RequestParam(value = "quotationid", required = false) Integer quotationid, @RequestParam(value = "customerid", required = false) Integer customerid, Model model){
        
        System.out.println("Search Invoice");
        System.out.println("Selected ID:" + quotationid);
        
        List<Quotation> quatationlist = null;
           if (quotationid == null && customerid != null) {
            quatationlist = quatationDAO.getCustomerID(customerid, user_id);
            System.out.println("in if" + quatationlist);
        } else if (quotationid != null && customerid == null) {
            System.out.println("in else");
            quatationlist = quatationDAO.getQuotationID(quotationid, user_id);
            System.out.println("in else**************" + quatationlist);
        } else if (quotationid == null && customerid == null) {
            System.out.println("in else if**************");
            quatationlist = null;
        }

        System.out.println("Data is"+quatationlist);
       model.addAttribute("quatationlist", quatationlist);
          
        
        return "quotation/quotationList";

    }
     @RequestMapping(value = "/editquotation", method = RequestMethod.POST)
    public String selectedit(@RequestParam("editid") int editid, Model model) {
       orderlist = orderlistDAO.getorderlist(editid);
         System.out.println("****************"+orderlist);
         model.addAttribute("orderlist", orderlist);
         return "quotation/quotationEdit";
    }
    
      @RequestMapping(value = "/editqtsave", method = RequestMethod.POST)
    public String selecteditsave(@RequestParam("productname") List<String>  productname,@RequestParam("productprice") List<Integer>  productprice,@RequestParam("quantity") List<Integer>  quantity,@RequestParam("sumproce") List<Integer> sumproce,@RequestParam("orid") List<Integer> orid,@RequestParam("qtid") List<Integer> qtid,@RequestParam("prid") List<Integer> prid,Model model) {
    
          for (int i=0; i<productname.size(); i++) {
             
           Orderlist orlist = new Orderlist(orid.get(i), qtid.get(i), prid.get(i), productname.get(i),productprice.get(i), quantity.get(i), sumproce.get(i));
             System.out.println("****************"+orlist.toString());
             //DAO UPDATE
             orderlistDAO.update(orlist);
          }
          completemessage = "Edit Complete";
     
      return "redirect:/quotation/quotationList";
    }
    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public String quotatuonEmployee (Model model) {
         return "quotation/employee";
    }
     @RequestMapping(value = "/employeeAdd", method = RequestMethod.GET)
    public String quotatuonEmployeeadd (Model model) {
         return "quotation/employeeAdd";
    }
     @RequestMapping(value = "/employeeStatus", method = RequestMethod.GET)
    public String quotatuonEmployeestatus (Model model) {
         return "quotation/employeeStatus";
    }
     @RequestMapping(value = "/employeeAddstatus", method = RequestMethod.GET)
    public String quotatuonEmployeeaddstatus (Model model) {
         return "quotation/employeeAddstatus";
    }
    
}
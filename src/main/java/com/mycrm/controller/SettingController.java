/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.EmailTopicDAO;
import com.mycrm.dao.EmailSenderDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.Contact;
import com.mycrm.domain.User;
import java.util.Date;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.mycrm.domain.EmailSender;
import com.mycrm.domain.EmailTopic;
 
@Controller
@RequestMapping(value = "/setting")
public class SettingController {
    private int userid =10;
    @Autowired
    UserDAO userDAO;
    
    @Autowired
    EmailSenderDAO emailsenderDAO;
    
    @Autowired
    EmailManager emailManager;
    
    @Autowired
    EmailTopicDAO emailtopicDAO;
    
    @RequestMapping(value="/profile", method = RequestMethod.GET,produces = {"application/json; charset=UTF-8","*/*;charset=UTF-8"})
    public String viewHome(Model model){
//        Test insert
//        User a = new User(0, "J", "DBC", "Pizza", "DDD@email.com", "1234756", "1112", "507/1", "BKKK", "sub", "DSI", "222200", "Mark", "LINE", new Date(), new Date());
//        userDAO.insert(a);
        User userprofile = userDAO.findUserId(userid);
        System.out.println("Get user full name: " + userprofile.fullName());
        model.addAttribute("fullname", userprofile.fullName());
         model.addAttribute("fullphone", userprofile.fullPhone());
        model.addAttribute("profile", userprofile);
        return "setting/showprofile";
    }
    
      @RequestMapping(value="/editprofile", method = RequestMethod.GET)
    public String editprofile(Model model){
        
        User userprofile = userDAO.findUserId(userid);
        System.out.println("Get user full name: " + userprofile.fullName());
         model.addAttribute("fullname", userprofile.fullName());
        model.addAttribute("profile", userprofile);
        return "setting/editprofile";
    }
    
       @RequestMapping(value="/editprofile", method = RequestMethod.POST,produces = {"application/json; charset=UTF-8","*/*;charset=UTF-8"})
    public String editprofile(Model model,@Valid User user,BindingResult result,
            @RequestParam(value="oldpass",required = false) String oldpass,
            @RequestParam(value="newpass",required = false) String newpass,
            @RequestParam(value="renewpass",required = false) String renewpass){
           User olduserprofile = userDAO.findUserId(userid);
           user.setUser_id(userid);
           user.setPassword(olduserprofile.getPassword());
           System.out.println("********************Post user: " + user.toString());
           
           if(oldpass.equals("") || newpass.equals("") || renewpass.equals("")){
               System.out.println("User don't want to change password OR some filde is empty.");
           }else{
               System.out.println("Old check : "+oldpass.equals(user.getPassword()));
               if(!oldpass.equals(user.getPassword())){
                   
                   model.addAttribute("profile", user);
                   model.addAttribute("message", "Old password is not correct!");
                   return "setting/editprofile";
                  
               }
               if (!newpass.equals(renewpass)) {
                   model.addAttribute("profile", user);
                   model.addAttribute("message", "Re-New password is wrong!");
                   return "setting/editprofile";
               }
               user.setPassword(newpass);
               System.out.println("Password change pass");
           }
           user.setLast_activeDate(new Date());
           user.setLast_login(new Date());
           userDAO.update(user);
           
        return "redirect:/setting/profile";
    }
    
      @RequestMapping(value="/emailsenderedit", method = RequestMethod.GET)
    public String emailsenderedit(Model model){
        EmailSender getemail = emailsenderDAO.findUserId(userid);    
        EmailTopic getTopic = emailtopicDAO.findTopic_id(1);
        System.out.println("Topic: " + getTopic.toString());
        System.out.println("Email: "  + getemail.getEmail());
        model.addAttribute("profile", getemail);
        model.addAttribute("topic", getTopic);
        return "setting/emailsenderedit";
    }
    
      @RequestMapping(value="/emailsenderedit", method = RequestMethod.POST)
    public String emailsenderedit(Model model,EmailSender emailSender){
       emailSender.setUser_id(userid);
       System.out.println("Post Data: "  + emailSender.toString());
       EmailSender getemail = emailsenderDAO.findUserId(userid);
       String oldemail = getemail.getEmail();
          if (oldemail == null) {
              System.out.println("Null use insert " );
              emailsenderDAO.insert(emailSender);
              
          }else{
              System.out.println("Not Null use update ");
              emailSender.setEmail_sender_id(getemail.getEmail_sender_id());
              emailsenderDAO.update(emailSender);
              
          }
        return "redirect:/setting/emailsenderedit";
    }
    
    @RequestMapping(value="/emailtopicedit", method = RequestMethod.POST)
    public String emailtopicedit(Model model,EmailTopic emailTopic){
     
       System.out.println("Post Data: "  + emailTopic.toString());
       emailTopic.setTopic_id(1);
       emailTopic.setType("General");
       System.out.println("After set Data: "  + emailTopic.toString());
       EmailTopic  getTopic = emailtopicDAO.findTopic_id(1);
       String oldTopic = getTopic.getTitle();
          if (oldTopic == null) {
              System.out.println("Null use insert " );
              emailtopicDAO.insert(emailTopic);
              
          }else{
              System.out.println("Not Null use update ");
              
              emailtopicDAO.update(emailTopic);
              
          }
        return "redirect:/setting/emailsenderedit";
    }
    

    @RequestMapping(value="/tax_reciepts", method = RequestMethod.GET)
    public String settingTax_reciepts(Model model){
        return "setting/tax_reciepts";
    }
   
    @RequestMapping(value="/tax", method = RequestMethod.GET)
    public String settingTax(Model model){
        return "setting/tax";
    }
    
    @RequestMapping(value="/tax_addtax", method = RequestMethod.GET)
    public String settingAddtax(Model model){
        return "setting/tax_addtax";
    }
   
    @RequestMapping(value="/paytypes", method = RequestMethod.GET)
    public String settingPaytypes(Model model){
        return "setting/paytypes";
    }
    
    @RequestMapping(value="/paytypes_addpaytypes", method = RequestMethod.GET)
    public String settingPaytypes_addpaytypes(Model model){
        return "setting/paytypes_addpaytypes";
    }
    
    @RequestMapping(value="/pos", method = RequestMethod.GET)
    public String settingpos(Model model){
        return "setting/pos";
    }
  
     @RequestMapping(value="/pos_addpos", method = RequestMethod.GET)
    public String settingPos_addpos(Model model){
        return "setting/pos_addpos";
    }
    
     @RequestMapping(value="/store", method = RequestMethod.GET)
    public String settingStore(Model model){
        return "setting/store";
    }
    
     @RequestMapping(value="/profilex", method = RequestMethod.GET)
    public String settingProfilex(Model model){
        return "setting/profilex";
    }  
    
     @RequestMapping(value="/store_addstore", method = RequestMethod.GET)
    public String settingStore_addstore(Model model){
        return "setting/store_addstore";
    }
    
     @RequestMapping(value="/account", method = RequestMethod.GET)
    public String settingAccount(Model model){
        return "setting/account";
    }
    
     @RequestMapping(value="/account_editdetails", method = RequestMethod.GET)
    public String settingAccount_editdetails(Model model){
        return "setting/account_editdetails";
    } 
    
    
     @RequestMapping(value="/account_addpayment", method = RequestMethod.GET)
    public String settingAccount_addpayment(Model model){
        return "setting/account_addpayment";
    }
    
    @RequestMapping(value="/loyaltyx", method = RequestMethod.GET)
    public String settingLoyaltyx(Model model){
        return "setting/loyaltyx";
    } 
    
     
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

/**
 *
 * @author Lenovo
 */
import com.mycrm.dao.OrderlistDAO;
import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.domain.Contact;
import com.mycrm.domain.Orderlist;
import com.mycrm.domain.Product;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
 
@Controller
@RequestMapping(value = "/testform")
public class TestFormController {
    @Autowired
    ContactDAO contactDAO;

    @Autowired
    ProductDAO productDAO;
    
    
    @Autowired
    OrderlistDAO orderlistDAO;
    
    private  int user_id = HomeController.getUser_id();
    
    

     @RequestMapping(method = RequestMethod.GET)
    public String quotationCreate(Model model){
        // Log in check 
         user_id = HomeController.getUser_id();
         System.out.println("Check User ID: " + user_id);
         if (user_id==0) {
             System.out.println("Plese log in");
             return "redirect:/";
         }
         
         
         List<Contact> contactlist = contactDAO.getcontactList(user_id);
         List<Product> productgetlist = productDAO.getproductList();
          List<Orderlist> orderlists = orderlistDAO.getorderlist(10);
         System.out.println("Ttest Get Qliset item" + orderlists);
         for (Orderlist orderlist : orderlists) {
             System.err.println("Get: " + orderlist.toString());
         } 
         System.out.println("Contactlist size: " + contactlist.size());
         System.out.println("Productgetlist size: " + productgetlist.size());
         model.addAttribute("testmodel", "TESTTTT");
         model.addAttribute("productgetlist", productgetlist);
         model.addAttribute("contactlists", contactlist);
        return "testform/testqreate";
    }
    
    
    @RequestMapping(value = "/senddata", method = RequestMethod.POST)
    public String selectedit(@RequestParam("testcreatename") String contact, Model model) {
        
List<Product> productgetlist;
        System.out.println("Selectcontact: " + contact);
        String[] parts = contact.split(",#,");
        for (String part : parts) {
            System.out.println("Get: " + part);
             String[] objSp = part.split(",");             
             //Orderlist newItem = new Orderlist(0, 10, Integer.parseInt(objSp[5]), objSp[1], Integer.parseInt(objSp[2]), Integer.parseInt(objSp[3]));
            // orderlistDAO.insert(newItem);
             System.out.println("ID: "+ objSp[0]);
             System.out.println("Name: "+ objSp[1]);
             System.out.println("Pirce: "+ objSp[2]);
             System.out.println("QTY: "+ objSp[3]);
             System.out.println("Sum: "+ objSp[4]);
              System.out.println("Product ID: "+ objSp[5]);
              objSp = null;
              System.out.println("--------------------");
             
        }
          return "redirect:/product/productList";
    }
    
    
  
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.controller;

import com.mycrm.domain.OrderWithdrawlist;
import com.mycrm.domain.Withdraw;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author pop
 */
@Controller
@RequestMapping(value = "/withdraw")
public class WithdrawController {

    @RequestMapping(value = "/addwithdraw", method = RequestMethod.GET)
    public String addWithdraw(Model model) {
        model.addAttribute("withdraw", new Withdraw());
        model.addAttribute("orderWithdrawlist", new OrderWithdrawlist());

        return "withdraw/addwithdraw";

    }

    @RequestMapping(value = "/addwithdraw", method = RequestMethod.POST)
    public String addwithdrawlist(Model model, Withdraw withdraw,OrderWithdrawlist orderWithdrawlist, BindingResult result,
            @RequestParam(value = "machinepicture") MultipartFile withdraw_file) {

        System.out.println("info ========> " + withdraw.toString());
        return "machine/addwithdraw";
    }
    
     @RequestMapping(value="/purchase", method = RequestMethod.GET)
    public String withdrawPurchase(Model model){
        return "withdraw/purchase";
    }
    
    @RequestMapping(value="/transfer", method = RequestMethod.GET)
    public String withdrawTransfer(Model model){
        return "withdraw/transfer";
    }
    
    @RequestMapping(value="/adjustment", method = RequestMethod.GET)
    public String withdrawAdjustment(Model model){
        return "withdraw/adjustment";
    }
        
    @RequestMapping(value="/counts", method = RequestMethod.GET)
    public String withdrawCounts(Model model){
        return "withdraw/counts";
    }

     @RequestMapping(value="/productionslist", method = RequestMethod.GET)
    public String withdrawProductionslist(Model model){
        return "withdraw/productionslist";
    }
    
     @RequestMapping(value="/valuation", method = RequestMethod.GET)
    public String settingValuation(Model model){
        return "withdraw/valuation";
    }
     @RequestMapping(value="/history", method = RequestMethod.GET)
    public String settingHistory(Model model){
        return "withdraw/history";
    }
}

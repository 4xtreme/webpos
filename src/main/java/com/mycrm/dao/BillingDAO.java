/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Billing;
import com.mycrm.domain.Product;
import com.mycrm.domain.Quotation;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface BillingDAO {
    public Integer insert(Billing billing);    
    public List<Billing> getBillingList();
    public Billing findBillingId(int billingId);
    public void updateStatus(int billingId);
    public void updateEmail(int billingId); 
   public List<Billing> getBillingListInVoiceID(int billingId,int user_id) ;
    public void setNewDate(int billingId,Date setdate);
    public List<Billing> getCustomerID(int customerId,int user_id);
}

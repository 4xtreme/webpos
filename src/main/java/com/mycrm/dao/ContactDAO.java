/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Contact;
import com.mycrm.domain.Product;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface ContactDAO {
   
    public Contact findcontactInfo(int contactId) ;
     public Integer insert(Contact contact);
     public List<Contact> getcontactList(int user_id);
     public void update(Contact contact);
      public void deleteContact(int contactId);
      public List<Contact> getemailList(String memtype);
}

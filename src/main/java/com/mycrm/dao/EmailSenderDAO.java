/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.EmailSender;


/**
 *
 * @author Lenovo
 */
public interface EmailSenderDAO {
    public Integer insert(EmailSender emailS);
    public EmailSender findUserId(int userId);
    public void update(EmailSender emailS);
}

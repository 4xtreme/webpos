/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.EmailTopic;


/**
 *
 * @author Lenovo
 */
public interface EmailTopicDAO {
    public Integer insert(EmailTopic emailS);
    public EmailTopic findTopic_id(int topic_id);
    public void update(EmailTopic emailS);
}

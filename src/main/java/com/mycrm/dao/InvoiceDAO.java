/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Billing;
import com.mycrm.domain.Invoice;
import com.mycrm.domain.Product;
import com.mycrm.domain.Quotation;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface InvoiceDAO {
    public Integer insert(Invoice invoice);
    
   public List<Invoice> getInvoiceList(int user_id);
    public Invoice findinvoiceId(int invoiceid);
   public void updateEmail(int invoiceid);
   public void updateStatus(int invoiceid);
   public List<Invoice> getInvoiceSearchList(int invoiceid);
    public void setNewDate(int invoiceid,Date setdate);
     public List<Invoice> getInvoiceListInVoiceID(int invoiceid,int user_id);
      public List<Invoice> getInvoiceListCustomerID(int customerid,int user_id);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.OrderBillinglist;
import com.mycrm.domain.OrderInvoicelist;
import com.mycrm.domain.Orderlist;
import com.mycrm.domain.Product;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface OrderInvoicelistDAO {
    public Integer insert(OrderInvoicelist orderinvoicelist);
    public List<OrderInvoicelist> getorderinvoicelistt(int invoiceid);
     public void update(OrderInvoicelist orderinvoicelist);
}

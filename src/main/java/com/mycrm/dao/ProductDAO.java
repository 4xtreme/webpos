/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Product;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface ProductDAO {
    public Integer insert(Product customer);
    public Product findByProductId(int productId);
    public List<Product> getproductList();
    public void update(Product product);
}

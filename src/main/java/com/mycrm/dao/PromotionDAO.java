/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Contact;
import com.mycrm.domain.Product;
import com.mycrm.domain.Promotion;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface PromotionDAO {
  
       public Integer insert(Promotion promotion);
   public List<Promotion> getpromotiontList();
    public Promotion findByProductId(int promotionId);
}

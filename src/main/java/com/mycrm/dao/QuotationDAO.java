/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.Product;
import com.mycrm.domain.Quotation;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface QuotationDAO {
    public Integer insert(Quotation quotation);
    
    public List<Quotation> getQuotationList();
    public Quotation findQuotationId(int quotationId);
     public void updateStatus(int quotationId); 
     public void updateEmail(int quotationId);
     public void setNewDate(int quotationId,Date setdate);
     public List<Quotation> getQuotationID(int quotationId,int user_id);
    public List<Quotation> getCustomerID(int customerId,int user_id);
}

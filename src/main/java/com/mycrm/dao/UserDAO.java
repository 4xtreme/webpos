/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao;

import com.mycrm.domain.User;


/**
 *
 * @author Lenovo
 */
public interface UserDAO {
    public Integer insert(User user);
    public User findUserId(int userId);
    public User emailfindUser(String useremail);
    public void update(User user);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.BillingDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Invoice;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class BillingDAOImpl extends NamedParameterJdbcDaoSupport implements BillingDAO {

    private final static String SQL_INSERT
            = "insert into billing ( customer_id, user_id, quotation_id, discount, status, create_date, due_date, sent_email ) values( :customer_id, :user_id, :quotation_id, :discount, :status, :create_date, :due_date, :sent_email )";

    private final static String SQL_SELECT_LIST
            = "select * from billing ";
    private final static String SQL_SELECT_INSERT_ID
                = "SELECT `billing _id` FROM billing ORDER by `billing _id` DESC LIMIT 1";
   private final static String SQL_SELECT_ID
            = "select * from billing where `billing _id` = ?";
    private final static String SQL_UPDATE_STATUS
            = "update billing set  status = true  where `billing _id` = :billing_id";
     private final static String SQL_UPDATE_EMAIL
            = "update billing set  sent_email = true  where `billing _id` = :billing_id";
     
     private final static String SQL_SELECT_LIST_IDBILLING
            = "select * from billing where `billing _id` = ? AND `user_id` = ?";
     private final static String SQL_SELECT_LIST_IDCUSTOMER
            = "select * from billing where `customer_id`  = ? AND `user_id` = ?";
      private final static String SQL_UPDATE_date
            = "update billing set  create_date = :create_date  where `billing _id` = :billing_id";

    @Override
    public Integer insert(Billing billing) {
        int key = -1;

        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    billing);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            key = getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class);
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("Key =" + key);
        return key;
    }

    @Override
    public List<Billing> getBillingList() {
        BeanPropertyRowMapper<Billing> bookRowMapper = BeanPropertyRowMapper.newInstance(Billing.class);
        List<Billing> billing = new ArrayList<Billing>();
        billing = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper);
        System.out.println("Get Data Table is" + billing.toString());

        return billing;
    }
    
     @Override
    public Billing findBillingId(int billingId) {
        System.out.println("DAO Find UserId: "+ billingId);
        BeanPropertyRowMapper<Billing> bookRowMapper = BeanPropertyRowMapper.newInstance(Billing.class);
        Billing user = getJdbcTemplate().queryForObject(SQL_SELECT_ID, bookRowMapper, billingId);
        return user;
    }
    
       @Override
        public void updateStatus(int billingId) {
        Billing b = new Billing();
        b.setBilling_id(billingId);
            System.out.println("B : " +b.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_STATUS, parameterSource);
        System.out.println("----------DAO END update---------");
    }
         @Override
        public void updateEmail(int billingId) {
        Billing b = new Billing();
        b.setBilling_id(billingId);
            System.out.println("B : " +b.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_EMAIL, parameterSource);
        System.out.println("----------DAO END update---------");
    }
        
            @Override
    public List<Billing> getBillingListInVoiceID(int billingId,int user_id) {
        BeanPropertyRowMapper<Billing> bookRowMapper = BeanPropertyRowMapper.newInstance(Billing.class);
        List<Billing> billing = new ArrayList<Billing>();
        billing = getJdbcTemplate().query(SQL_SELECT_LIST_IDBILLING, bookRowMapper,billingId,user_id);
        System.out.println("Get Data Table is" + billing.toString());

        return billing;
    }
    
             @Override
    public List<Billing> getCustomerID(int customerId,int user_id) {
        BeanPropertyRowMapper<Billing> bookRowMapper = BeanPropertyRowMapper.newInstance(Billing.class);
        List<Billing> billing = new ArrayList<Billing>();
        billing = getJdbcTemplate().query(SQL_SELECT_LIST_IDCUSTOMER, bookRowMapper,customerId,user_id);
        System.out.println("Get Data Table is" + billing.toString());

        return billing;
    }
        @Override
      public void setNewDate(int billingId,Date setdate) {
        Billing b = new Billing();
        b.setBilling_id(billingId);
        b.setCreate_date(setdate);

        System.out.println("Set new date : " +b.getCreate_date());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_date, parameterSource);
        System.out.println("----------DAO END update---------");
    }
}

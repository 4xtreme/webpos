/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Contact;
import com.mycrm.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class ContactDAOImpl extends NamedParameterJdbcDaoSupport implements ContactDAO{
    private final static String SQL_SELECT_ID
            = "select * from customer where customer_id = ?";
    private final static String SQL_SELECT
            = "select * from customer where is_delete = false AND user_id = ?"; 
    private final static String SQL_INSERT
            = "insert into customer (customer_id, firstname, lastname, email, address1, address2, district, city, country, postalcode, phone1, phone2, facebook, line, is_delete, user_id, member_type) values(:customer_id, :firstname, :lastname, :email, :address1, :address2, :district, :city, :country, :postalcode, :phone1, :phone2, :facebook, :line, :is_delete, :user_id, :member_type)";
    private final static String SQL_UPDATE
            = "update customer set  firstname =:firstname, lastname =:lastname, email =:email, address1 =:address1, address2 =:address2, district =:district, city =:city, country =:country, postalcode =:postalcode, phone1 =:phone1, phone2 =:phone2, facebook =:facebook, line =:line, is_delete = false, user_id = :user_id, member_type = :member_type where customer_id = :customer_id";
    
    private final static String SQL_DELETE
            = "update customer set  is_delete = true  where customer_id = :customer_id "; 
    private final static String SELETE_EMAIL
    ="SELECT email FROM customer WHERE member_type = ?";
      private final static String SELETE_EMAIL_ALL
    ="SELECT email FROM customer ";

    @Override
    public Contact findcontactInfo(int contactId) {
        BeanPropertyRowMapper<Contact> bookRowMapper = BeanPropertyRowMapper.newInstance(Contact.class);
        Contact contact = getJdbcTemplate().queryForObject(SQL_SELECT_ID, bookRowMapper, contactId);
        System.out.println("DAO Get contact: "+contact.getFirstname()+ " " + contact.getLastname());
        return contact;
    }
    
    @Override
    public Integer insert(Contact contact) {
        System.out.println("DAO" + contact.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(contact);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }
    
        @Override
    public List<Contact> getcontactList(int user_id) {
        BeanPropertyRowMapper<Contact> bookRowMapper = BeanPropertyRowMapper.newInstance(Contact.class);
        List<Contact> contact = new ArrayList<Contact>();
        contact = getJdbcTemplate().query(SQL_SELECT, bookRowMapper,user_id);
        System.out.println("Get Data Table is" + contact.toString());

        return contact;
    }
      @Override
    public void update(Contact contact) {
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                contact);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update: " + contact.toString());
        System.out.println("----------DAO END update---------");
    }

        @Override
        public void deleteContact(int contactId) {
     Contact c = new Contact();
     c.setCustomer_id(contactId);
        
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(c);
        getNamedParameterJdbcTemplate().update(SQL_DELETE, parameterSource);
        System.out.println("----------DAO Delete---------");
    }
             @Override
    public List<Contact> getemailList(String memtype) {
        BeanPropertyRowMapper<Contact> bookRowMapper = BeanPropertyRowMapper.newInstance(Contact.class);
        List<Contact> contact = new ArrayList<Contact>();
                 if (memtype.equals("all")) {
                 contact = getJdbcTemplate().query(SELETE_EMAIL_ALL, bookRowMapper);     
                 }else{
                  contact = getJdbcTemplate().query(SELETE_EMAIL, bookRowMapper,memtype);
                 }
       
        System.out.println("Get Data Table is" + contact.toString());

        return contact;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.EmailSenderDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.EmailSender;
import com.mycrm.domain.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class EmailSenderDAOImpl extends NamedParameterJdbcDaoSupport implements EmailSenderDAO{
    
    private final static String SQL_SELECT
            = "select * from email_sender where user_id = ?";

    private final static String SQL_INSERT
            = "insert into email_sender (user_id, email, password, port) values(:user_id, :email, :password, :port)";

    private final static String SQL_UPDATE
            = "update email_sender set email = :email, password = :password, port = :port where email_sender_id = :email_sender_id";

    @Override
    public Integer insert(EmailSender emailS) {
        int key = -1;
        try {
            System.out.println("Start insert mail: " + emailS.toString());
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    emailS);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            
            key = 0;
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }

    @Override
    public EmailSender findUserId(int userId) {
        System.out.println("DAO Find UserId: "+ userId);
        BeanPropertyRowMapper<EmailSender> bookRowMapper = BeanPropertyRowMapper.newInstance(EmailSender.class);
        EmailSender emailinfo = new EmailSender();
        try {
            emailinfo = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, userId);
            
        } catch (Exception e) {
            System.out.println("Error not found this ID! \n"+e);
        }
        return emailinfo;
    }
    
       @Override
    public void update(EmailSender emailS) {
        System.out.println("DAO Edit:" + emailS.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(emailS);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("----------DAO END update---------");
    }	
    
}

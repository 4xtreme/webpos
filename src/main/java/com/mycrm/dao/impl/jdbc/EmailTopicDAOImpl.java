/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.EmailSenderDAO;
import com.mycrm.dao.EmailTopicDAO;
import com.mycrm.dao.UserDAO;
import com.mycrm.domain.EmailTopic;
import com.mycrm.domain.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class EmailTopicDAOImpl extends NamedParameterJdbcDaoSupport implements EmailTopicDAO{
    
    private final static String SQL_SELECT
            = "select * from email_topic where topic_id = ?";

    private final static String SQL_INSERT
            = "insert into email_topic (title, detial, type) values(:title, :detial, :type)";

    private final static String SQL_UPDATE
            = "update email_topic set title = :title,  detail = :detail, type = :type where topic_id = :topic_id";

    @Override
    public Integer insert(EmailTopic emailS) {
        int key = -1;
        try {
            System.out.println("Start insert mail: " + emailS.toString());
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    emailS);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            
            key = 0;
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }

    @Override
    public EmailTopic findTopic_id(int topic_id) {
        System.out.println("DAO Find topic_id: "+ topic_id);
        BeanPropertyRowMapper<EmailTopic> bookRowMapper = BeanPropertyRowMapper.newInstance(EmailTopic.class);
        EmailTopic emailinfo = new EmailTopic();
        try {
            emailinfo = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, topic_id);
            
        } catch (Exception e) {
            System.out.println("Error not found this ID! \n"+e);
        }
        return emailinfo;
    }
    
       @Override
    public void update(EmailTopic emailS) {
        System.out.println("DAO Edit:" + emailS.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(emailS);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("----------DAO END update---------");
    }	
    
}

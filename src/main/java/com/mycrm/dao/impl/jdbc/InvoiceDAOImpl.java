/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.BillingDAO;
import com.mycrm.dao.InvoiceDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Invoice;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class InvoiceDAOImpl extends NamedParameterJdbcDaoSupport implements InvoiceDAO {

    private final static String SQL_INSERT
            = "insert into invoice ( customer_id, user_id, billing_id, discount, status, create_date, due_date , sent_email ) values( :customer_id, :user_id, :billing_id, :discount, :status, :create_date, :due_date, :sent_email )";

    private final static String SQL_SELECT_LIST
            = "select * from invoice where `user_id` = ?";
    private final static String SQL_SELECT_LIST_IDINVOICE
            = "select * from invoice where `Invoice_id` = ? AND `user_id` = ?";
    private final static String SQL_SELECT_INSERT_ID
            = "SELECT Invoice_id FROM invoice ORDER by Invoice_id DESC LIMIT 1 ";
       private final static String SQL_SELECT_ID
            = "select * from invoice where `Invoice_id` = ?";
       private final static String SQL_UPDATE_EMAIL
            = "update invoice set  sent_email = true  where `Invoice_id` = :Invoice_id";
       private final static String SQL_UPDATE_STATUS
            = "update invoice set  status = true  where `Invoice_id` = :Invoice_id";
       private final static String SQL_SELECT_SEARCH
            = "select * from invoice where Invoice_id = ? ";
       private final static String SQL_UPDATE_date
            = "update invoice set  create_date = :create_date  where `Invoice_id` = :Invoice_id";
        private final static String SQL_SELECT_LIST_IDCUSTOMER
            = "select * from invoice where `customer_id` = ? AND `user_id` = ?";
       
       

    @Override
    public Integer insert(Invoice invoice) {
        int key = -1;

        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    invoice);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            key = getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class);
        } catch (Exception e) {
            System.out.println("Insert error: " + e.getMessage());
        }
        System.out.println("Key =" + key);
        return key;
    }

    @Override
    public List<Invoice> getInvoiceList(int user_id) {
        BeanPropertyRowMapper<Invoice> bookRowMapper = BeanPropertyRowMapper.newInstance(Invoice.class);
        List<Invoice> invoice = new ArrayList<Invoice>();
        invoice = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper,user_id);
        System.out.println("Get Data Table is" + invoice.toString());

        return invoice;
    }
    
    @Override
    public Invoice findinvoiceId(int invoiceid) {
        System.out.println("DAO Find invoiceid: "+ invoiceid);
        BeanPropertyRowMapper<Invoice> bookRowMapper = BeanPropertyRowMapper.newInstance(Invoice.class);
        Invoice invoiceinfo = getJdbcTemplate().queryForObject(SQL_SELECT_ID, bookRowMapper, invoiceid);
        return invoiceinfo;
    }
  @Override
        public void updateEmail(int invoiceid) {
        Invoice b = new Invoice();
        b.setInvoice_id(invoiceid);
            System.out.println("B : " +b.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_EMAIL, parameterSource);
        System.out.println("----------DAO END update---------");
    }
        
           @Override
        public void updateStatus(int invoiceid) {
        Invoice b = new Invoice();
        b.setInvoice_id(invoiceid);
            System.out.println("B : " +b.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_STATUS, parameterSource);
        System.out.println("----------DAO END update---------");
    }
        
         @Override
    public List<Invoice> getInvoiceSearchList(int invoiceid) {
        BeanPropertyRowMapper<Invoice> bookRowMapper = BeanPropertyRowMapper.newInstance(Invoice.class);
        List<Invoice> invoice = new ArrayList<Invoice>();
        invoice = getJdbcTemplate().query(SQL_SELECT_SEARCH, bookRowMapper,invoiceid);
        System.out.println("Get Data Table is" + invoice.toString());

        return invoice;
    }
    
    @Override
      public void setNewDate(int invoiceid,Date setdate) {
        Invoice b = new Invoice();
        b.setInvoice_id(invoiceid);
        b.setCreate_date(setdate);
         System.out.println("---------SET DATE ON DAO-------\nSet this ID: " +b.getInvoice_id());
        System.out.println("Set new date : " +b.getCreate_date());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_date, parameterSource);
        System.out.println("----------DAO END update---------");
    }
      
       @Override
    public List<Invoice> getInvoiceListInVoiceID(int invoiceid,int user_id) {
        BeanPropertyRowMapper<Invoice> bookRowMapper = BeanPropertyRowMapper.newInstance(Invoice.class);
        List<Invoice> invoice = new ArrayList<Invoice>();
        invoice = getJdbcTemplate().query(SQL_SELECT_LIST_IDINVOICE, bookRowMapper,invoiceid,user_id);
        System.out.println("Get Data Table is" + invoice.toString());

        return invoice;
    }
           @Override
    public List<Invoice> getInvoiceListCustomerID(int customerid,int user_id) {
        BeanPropertyRowMapper<Invoice> bookRowMapper = BeanPropertyRowMapper.newInstance(Invoice.class);
        List<Invoice> invoice = new ArrayList<Invoice>();
        invoice = getJdbcTemplate().query(SQL_SELECT_LIST_IDCUSTOMER, bookRowMapper,customerid,user_id);
        System.out.println("Get Data Table is" + invoice.toString());

        return invoice;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.MachineDAO;
import com.mycrm.domain.Machine;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author pop
 */
public class MachineDAOImpl extends NamedParameterJdbcDaoSupport implements MachineDAO {
    
    private final static String SQL_INSERT
            = "insert into machine ( name, car_regis_num, brand, model, engine_size, size_type, machine_num, body_num, type, category, file, picture, start_date, buy_from, driver, machine_status ) "
            + "values(:machine_name, :car_regis_num, :brand, :mechine_model, :engine_size, :size_type, :machine_num, :body_num, :type, :category, :machine_file, :machine_pic, :start_date, :buy_form, :driver, :machine_status )";
    
    
    
    @Override
    public Integer insert(Machine machine) {
        System.out.println("DAO" + machine.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(machine);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.OrderBillinglistDAO;
import com.mycrm.dao.OrderlistDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.domain.OrderBillinglist;
import com.mycrm.domain.Orderlist;
import com.mycrm.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class OrderBillinglistDAOImpl extends NamedParameterJdbcDaoSupport implements OrderBillinglistDAO{
    


    private final static String SQL_INSERT
            = "insert into order_billinglist (billing_id, product_id, product_name, product_price, quantity, sum_proce) values(:billing_id, :product_id, :product_name, :product_price, :quantity, :sum_proce)";

    private final static String SQL_SELECT_LIST
            = "select * from order_billinglist where billing_id = ?";
    private final static String SQL_SELECT_SUM
   = "SELECT * FROM billing.order_billinglist WHERE billing_id =?";
    private final static String SQL_UPDATE
            = "update order_billinglist set  billing_id= :billing_id, product_id = :product_id,product_name = :product_name, product_price = :product_price, quantity = :quantity , sum_proce = :sum_proce where id = :id";

   



    @Override
    public Integer insert(OrderBillinglist orderbillinglist) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    orderbillinglist);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }

   
    
        @Override
     public List<OrderBillinglist> getorderbillinglist(int billingid) {
        BeanPropertyRowMapper<OrderBillinglist> bookRowMapper = BeanPropertyRowMapper.newInstance(OrderBillinglist.class);
        List<OrderBillinglist> orderbillinglist = new ArrayList<OrderBillinglist>();
        orderbillinglist = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper,billingid);
        System.out.println("Get Data Table is" + orderbillinglist.toString());

        return orderbillinglist;
    }
            @Override
    public void update(OrderBillinglist orderbillinglist) {
           System.out.println("DAO Edit:" + orderbillinglist.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                orderbillinglist);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update: " + orderbillinglist.toString());
        System.out.println("----------DAO END update---------");
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.OrderBillinglistDAO;
import com.mycrm.dao.OrderInvoicelistDAO;
import com.mycrm.dao.OrderlistDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.domain.OrderBillinglist;
import com.mycrm.domain.OrderInvoicelist;
import com.mycrm.domain.Orderlist;
import com.mycrm.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class OrderInvoicelistDAOImpl extends NamedParameterJdbcDaoSupport implements OrderInvoicelistDAO{
    


    private final static String SQL_INSERT
            = "insert into order_invoicelist (Invoice_id, product_id, product_name, product_price, quantity, sum_proce) values(:Invoice_id, :product_id, :product_name, :product_price, :quantity, :sum_proce)";

    private final static String SQL_SELECT_LIST
            = "select * from order_invoicelist where Invoice_id = ?";
    private final static String SQL_SELECT_SUM
   = "SELECT * FROM billing.order_invoicelist WHERE Invoice_id =?";
    private final static String SQL_UPDATE
            = "update order_invoicelist set  Invoice_id= :Invoice_id, product_id = :product_id,product_name = :product_name, product_price = :product_price, quantity = :quantity , sum_proce = :sum_proce where id = :id";

   



    @Override
     public Integer insert(OrderInvoicelist orderinvoicelist) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    orderinvoicelist);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }

   
    
        @Override
      public List<OrderInvoicelist> getorderinvoicelistt(int invoiceid) {
        BeanPropertyRowMapper<OrderInvoicelist> bookRowMapper = BeanPropertyRowMapper.newInstance(OrderInvoicelist.class);
        List<OrderInvoicelist> orderinvoicelist = new ArrayList<OrderInvoicelist>();
        orderinvoicelist = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper,invoiceid);
        System.out.println("Get Data Table is" + orderinvoicelist.toString());

        return orderinvoicelist;
    }
             @Override
    public void update(OrderInvoicelist orderinvoicelist) {
           System.out.println("DAO Edit:" + orderinvoicelist.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                orderinvoicelist);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update: " + orderinvoicelist.toString());
        System.out.println("----------DAO END update---------");
    }
    
}

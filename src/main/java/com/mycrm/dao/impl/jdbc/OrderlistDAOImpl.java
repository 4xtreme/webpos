/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.OrderlistDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.domain.Orderlist;
import com.mycrm.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class OrderlistDAOImpl extends NamedParameterJdbcDaoSupport implements OrderlistDAO{
    


    private final static String SQL_INSERT
            = "insert into order_list (quotation_id, product_id, product_name, product_price, quantity, sum_proce) values(:quotation_id, :product_id, :product_name, :product_price, :quantity, :sum_proce)";

    private final static String SQL_SELECT_LIST
            = "select * from order_list where quotation_id = ?";
    private final static String SQL_SELECT_SUM
   = "SELECT * FROM billing.order_list WHERE quotation_id =?";
private final static String SQL_UPDATE
            = "update order_list set  quotation_id= :quotation_id, product_id = :product_id,product_name = :product_name, product_price = :product_price, quantity = :quantity , sum_proce = :sum_proce where id = :id";
   



    @Override
    public Integer insert(Orderlist orderlist) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    orderlist);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }

   
    
        @Override
     public List<Orderlist> getorderlist(int quotationid) {
        BeanPropertyRowMapper<Orderlist> bookRowMapper = BeanPropertyRowMapper.newInstance(Orderlist.class);
        List<Orderlist> product = new ArrayList<Orderlist>();
        product = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper,quotationid);
        System.out.println("Get Data Table is" + product.toString());

        return product;
    }
     
         @Override
    public void update(Orderlist orderlist) {
           System.out.println("DAO Edit:" + orderlist.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                orderlist);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update: " + orderlist.toString());
        System.out.println("----------DAO END update---------");
    }
    
    
}

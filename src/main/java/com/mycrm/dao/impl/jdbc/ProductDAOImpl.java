/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ProductDAO;
import com.mycrm.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class ProductDAOImpl extends NamedParameterJdbcDaoSupport implements ProductDAO{
    
    private final static String SQL_SELECT
            = "select * from product where product_id = ?";

    private final static String SQL_INSERT
            = "insert into product (user_id, product_name, detail, color, size, pirce, stock, create_date, sell_start_date, sell_end_date) values(:user_id, :product_name, :detail, :color, :size, :pirce, :stock, :create_date, :sell_start_date, :sell_end_date)";

    private final static String SQL_SELECT_LIST
            = "select * from product";

    private final static String SQL_UPDATE
            = "update product set  product_name= :product_name, detail = :detail,color = :color, size = :size, pirce = :pirce , stock = :stock where product_id = :product_id";



    @Override
    public Integer insert(Product customer) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    customer);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }

    @Override
    public Product findByProductId(int productId) {
        System.out.println("DAO Find productId: "+ productId);
        BeanPropertyRowMapper<Product> bookRowMapper = BeanPropertyRowMapper.newInstance(Product.class);
        Product product = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, productId);
        return product;
    }
    
    
        @Override
    public List<Product> getproductList() {
        BeanPropertyRowMapper<Product> bookRowMapper = BeanPropertyRowMapper.newInstance(Product.class);
        List<Product> product = new ArrayList<Product>();
        product = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper);
        System.out.println("Get Data Table is" + product.toString());

        return product;
    }
    
    
       @Override
    public void update(Product product) {
           System.out.println("DAO Edit:" + product.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                product);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update: " + product.toString());
        System.out.println("----------DAO END update---------");
    }	
}

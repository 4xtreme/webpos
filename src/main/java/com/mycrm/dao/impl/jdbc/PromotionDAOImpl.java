/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ContactDAO;
import com.mycrm.dao.ProductDAO;
import com.mycrm.dao.PromotionDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Contact;
import com.mycrm.domain.Product;
import com.mycrm.domain.Promotion;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class PromotionDAOImpl extends NamedParameterJdbcDaoSupport implements PromotionDAO{
  
    private final static String SQL_INSERT
            = "insert into promotion (promotion_id, customer_type, topic, detail, create_date) values(:promotion_id, :customer_type, :topic, :detail, :create_date)";

private final static String SQL_SELECT_LIST
            = "select * from promotion";
private final static String SQL_SELECT
            = "select * from promotion where promotion_id = ?";
    
    @Override
    public Integer insert(Promotion promotion) {
        System.out.println("DAO" + promotion.toString());
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(promotion);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }
    
        @Override
    public List<Promotion> getpromotiontList() {
        BeanPropertyRowMapper<Promotion> bookRowMapper = BeanPropertyRowMapper.newInstance(Promotion.class);
        List<Promotion> promotion = new ArrayList<Promotion>();
         promotion = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper);
        System.out.println("Get Data Table is" + promotion.toString());

        return promotion;
    }
    
      @Override
    public Promotion findByProductId(int promotionId) {
        System.out.println("DAO Find productId: "+ promotionId);
        BeanPropertyRowMapper<Promotion> bookRowMapper = BeanPropertyRowMapper.newInstance(Promotion.class);
        Promotion promotion = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, promotionId);
        return promotion;
    }

}

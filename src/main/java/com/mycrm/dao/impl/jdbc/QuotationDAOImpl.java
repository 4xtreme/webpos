/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.ProductDAO;
import com.mycrm.dao.QuotationDAO;
import com.mycrm.domain.Billing;
import com.mycrm.domain.Product;
import com.mycrm.domain.Quotation;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class QuotationDAOImpl extends NamedParameterJdbcDaoSupport implements QuotationDAO{


    private final static String SQL_INSERT
            = "insert into quotation ( customer_id, user_id, discount, status, create_date, due_date, sent_email ) values( :customer_id, :user_id, :discount, :status, :create_date, :due_date, :sent_email )";

    private final static String SQL_SELECT_LIST
            = "select * from quotation ";
    private final static String SQL_SELECT_ID
    = "select * from quotation where `quotation_id` = ?";
        private final static String SQL_SELECT_INSERT_ID
                = "SELECT `quotation_id` FROM quotation ORDER by `quotation_id` DESC LIMIT 1";
    
    private final static String SQL_UPDATE_STATUS
            = "update quotation set  status = true  where `quotation_id` = :quotation_id";
    
    private final static String SQL_UPDATE_EMAIL
            = "update quotation set  sent_email = true  where `quotation_id` = :quotation_id";
 private final static String SQL_SELECT_LIST_IDQUOTATION
            = "select * from quotation where `quotation_id` = ? AND `user_id` = ?";
  private final static String SQL_SELECT_LIST_IDCUSTOMER
            = "select * from quotation where `customer_id` = ? AND `user_id` = ?";
      private final static String SQL_UPDATE_date
            = "update quotation set  create_date = :create_date  where `quotation_id` = :quotation_id";


    @Override
    public Integer insert(Quotation quotation) {
        int key = -1;
        
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    quotation);
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
          key = getNamedParameterJdbcTemplate().getJdbcOperations().queryForObject(SQL_SELECT_INSERT_ID, Integer.class);
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        System.out.println("Key ="+key);
        return key;
    }
    
        @Override
    public List<Quotation> getQuotationList() {
        BeanPropertyRowMapper<Quotation> bookRowMapper = BeanPropertyRowMapper.newInstance(Quotation.class);
        List<Quotation> quotation = new ArrayList<Quotation>();
        quotation = getJdbcTemplate().query(SQL_SELECT_LIST, bookRowMapper);
        System.out.println("Get Data Table is" + quotation.toString());

        return quotation;
    }
    
      @Override
     public Quotation findQuotationId(int quotationId) {
        System.out.println("DAO Find UserId: "+ quotationId);
        BeanPropertyRowMapper<Quotation> bookRowMapper = BeanPropertyRowMapper.newInstance(Quotation.class);
        Quotation user = getJdbcTemplate().queryForObject(SQL_SELECT_ID, bookRowMapper, quotationId);
        return user;
    }
    
    @Override
        public void updateStatus(int quotationId) {
        Quotation b = new Quotation();
        b.setQuotation_id(quotationId);
            System.out.println("B : " +b.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_STATUS, parameterSource);
        System.out.println("----------DAO END update---------");
    }
        
         @Override
        public void updateEmail(int quotationId) {
        Quotation b = new Quotation();
        b.setQuotation_id(quotationId);
            System.out.println("B : " +b.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_EMAIL, parameterSource);
        System.out.println("----------DAO END update---------");
    }
        
        @Override
    public List<Quotation> getQuotationID(int quotationId,int user_id) {
        BeanPropertyRowMapper<Quotation> bookRowMapper = BeanPropertyRowMapper.newInstance(Quotation.class);
        List<Quotation> quotation = new ArrayList<Quotation>();
        quotation = getJdbcTemplate().query(SQL_SELECT_LIST_IDQUOTATION, bookRowMapper,quotationId,user_id);
        System.out.println("Get Data Table is" + quotation.toString());

        return quotation;
    }
    
        @Override
    public List<Quotation> getCustomerID(int customerId,int user_id) {
        BeanPropertyRowMapper<Quotation> bookRowMapper = BeanPropertyRowMapper.newInstance(Quotation.class);
        List<Quotation> quotation = new ArrayList<Quotation>();
        quotation = getJdbcTemplate().query(SQL_SELECT_LIST_IDCUSTOMER, bookRowMapper,customerId,user_id);
        System.out.println("Get Data Table is" + quotation.toString());

        return quotation;
    }
    
        @Override
      public void setNewDate(int quotationId,Date setdate) {
        Quotation b = new Quotation();
        b.setQuotation_id(quotationId);
        b.setCreate_date(setdate);

        System.out.println("Set new date : " +b.getCreate_date());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(b);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE_date, parameterSource);
        System.out.println("----------DAO END update---------");
    }
    
    
  	
}

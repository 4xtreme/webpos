/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.dao.impl.jdbc;

import com.mycrm.dao.UserDAO;
import com.mycrm.domain.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author Lenovo
 */
public class UserDAOImpl extends NamedParameterJdbcDaoSupport implements UserDAO{
    
    private final static String SQL_SELECT
            = "select * from user where user_id = ?";
    private final static String SQL_SELECT_EMAIL
            = "select * from user where email = ?";

    private final static String SQL_INSERT
            = "insert into user (user_id, firstname, lastname, company_name, email, password, phone1, phone2, address, district, city, country, postalcode, facebook, line, last_active, last_login) values(:user_id, :firstname, :lastname, :company_name, :email, :password, :phone1, :phone2, :address, :district, :city, :country, ;postalcode, :facebook, :line, :last_active, :last_login)";

    private final static String SQL_UPDATE
            = "update user set  firstname = :firstname, lastname = :lastname, company_name = :company_name, email = :email, password = :password, phone1 = :phone1 , phone2 = :phone2, address = :address, district = :district, city = :city, country = :country, postalcode = :postalcode, facebook = :facebook, line = :line, last_active = :last_activeDate, last_login = :last_login where user_id = :user_id";

    @Override
    public Integer insert(User user) {
        int key = -1;
        try {
            SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(
                    user);
           // System.out.println("Can't insert !Need to edit SQL");
            key = getNamedParameterJdbcTemplate().update(SQL_INSERT, parameterSource);
            
            key = 0;
        } catch (Exception e) {
            System.out.println("Insert error: "+e.getMessage());
        }
        return key;
    }

    @Override
    public User findUserId(int userId) {
        System.out.println("DAO Find UserId: "+ userId);
        BeanPropertyRowMapper<User> bookRowMapper = BeanPropertyRowMapper.newInstance(User.class);
        User user = getJdbcTemplate().queryForObject(SQL_SELECT, bookRowMapper, userId);
        return user;
    }
    
    
        
    
       @Override
    public void update(User user) {
           System.out.println("DAO Edit:" + user.toString());
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(user);
        getNamedParameterJdbcTemplate().update(SQL_UPDATE, parameterSource);
        System.out.println("After update: " + user.toString());
        System.out.println("----------DAO END update---------");
    }	
    
    @Override
    public User emailfindUser(String useremail){
        System.out.println("DAO Find by Email: "+ useremail);
        BeanPropertyRowMapper<User> bookRowMapper = BeanPropertyRowMapper.newInstance(User.class);
        User user = getJdbcTemplate().queryForObject(SQL_SELECT_EMAIL, bookRowMapper, useremail);
        return user;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.util.Date;

/**
 *
 * @author Lenovo
 */
public class Billing {
 
   private int billing_id;
   private int customer_id;
   private int user_id;
   private int quotation_id;
   private int discount;
   private boolean status;
   private Date create_date;
   private Date due_date;
   private boolean sent_email;

    public Billing() {
    }

    public Billing(int billing_id, int customer_id, int user_id, int quotation_id, int discount, boolean status, Date create_date, Date due_date, boolean sent_email) {
        this.billing_id = billing_id;
        this.customer_id = customer_id;
        this.user_id = user_id;
        this.quotation_id = quotation_id;
        this.discount = discount;
        this.status = status;
        this.create_date = create_date;
        this.due_date = due_date;
        this.sent_email = sent_email;
    }

    public int getBilling_id() {
        return billing_id;
    }

    public void setBilling_id(int billing_id) {
        this.billing_id = billing_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getQuotation_id() {
        return quotation_id;
    }

    public void setQuotation_id(int quotation_id) {
        this.quotation_id = quotation_id;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public boolean isSent_email() {
        return sent_email;
    }

    public void setSent_email(boolean sent_email) {
        this.sent_email = sent_email;
    }

    @Override
    public String toString() {
        return "Billing{" + "billing_id=" + billing_id + ", customer_id=" + customer_id + ", user_id=" + user_id + ", quotation_id=" + quotation_id + ", discount=" + discount + ", status=" + status + ", create_date=" + create_date + ", due_date=" + due_date + ", sent_email=" + sent_email + '}';
    }

  

    
}

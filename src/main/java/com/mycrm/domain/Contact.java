/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.util.Date;

/**
 *
 * @author Lenovo
 */
public class Contact {
 
    private String firstname;
    private String lastname;
    private String email;
    private String address1;
    private String address2;
    private String district;
    private String city;
    private String country;
    private int postalcode;
    private String phone1;
    private String phone2;
    private String facebook;
    private String line;
    private int user_id;
    private int customer_id;
    private boolean is_delete;
    private String member_type;

    public Contact() {
    }

    public Contact(String firstname, String lastname, String email, String address1, String address2, String district, String city, String country, int postalcode, String phone1, String phone2, String facebook, String line, int user_id, int customer_id, boolean is_delete, String member_type) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address1 = address1;
        this.address2 = address2;
        this.district = district;
        this.city = city;
        this.country = country;
        this.postalcode = postalcode;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.facebook = facebook;
        this.line = line;
        this.user_id = user_id;
        this.customer_id = customer_id;
        this.is_delete = is_delete;
        this.member_type = member_type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(int postalcode) {
        this.postalcode = postalcode;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public boolean isIs_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }

    public String getMember_type() {
        return member_type;
    }

    public void setMember_type(String member_type) {
        this.member_type = member_type;
    }

    @Override
    public String toString() {
        return "Contact{" + "firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", address1=" + address1 + ", address2=" + address2 + ", district=" + district + ", city=" + city + ", country=" + country + ", postalcode=" + postalcode + ", phone1=" + phone1 + ", phone2=" + phone2 + ", facebook=" + facebook + ", line=" + line + ", user_id=" + user_id + ", customer_id=" + customer_id + ", is_delete=" + is_delete + ", member_type=" + member_type + '}';
    }

   
    
}

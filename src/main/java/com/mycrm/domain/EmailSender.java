/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author Lenovo
 */
public class EmailSender {
    private int email_sender_id;
    private int user_id;
    private String email;
    private String password;
    private int port;

    public int getEmail_sender_id() {
        return email_sender_id;
    }

    public void setEmail_sender_id(int email_sender_id) {
        this.email_sender_id = email_sender_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public EmailSender() {
    }

    public EmailSender(int email_sender_id, int user_id, String email, String password, int port) {
        this.email_sender_id = email_sender_id;
        this.user_id = user_id;
        this.email = email;
        this.password = password;
        this.port = port;
    }

    @Override
    public String toString() {
        return "EmailSender{" + "email_sender_id=" + email_sender_id + ", user_id=" + user_id + ", email=" + email + ", password=" + password + ", port=" + port + '}';
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author Lenovo
 */
public class EmailTopic {
    private int topic_id;   
    private String title;
    private String detail;
    private String type;

    public int getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EmailTopic() {
    }

    public EmailTopic(int topic_id, String title, String detail, String type) {
        this.topic_id = topic_id;
        this.title = title;
        this.detail = detail;
        this.type = type;
    }

    @Override
    public String toString() {
        return "EmailTopic{" + "topic_id=" + topic_id + ", title=" + title + ", detial=" + detail + ", type=" + type + '}';
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author pop
 */
public class OrderWithdrawlist {

    private int id;
    private int withdraw_id;
    private int index_number;
    private String broken_item;
    private String type;
    private int quantity;
    private int amount;
    private int sum_price;
    private String note;

    public OrderWithdrawlist() {
    }

    public OrderWithdrawlist(int id, int withdraw_id, int index_number, String broken_item, String type, int quantity, int amount, int sum_price, String note) {
        this.id = id;
        this.withdraw_id = withdraw_id;
        this.index_number = index_number;
        this.broken_item = broken_item;
        this.type = type;
        this.quantity = quantity;
        this.amount = amount;
        this.sum_price = sum_price;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWithdraw_id() {
        return withdraw_id;
    }

    public void setWithdraw_id(int withdraw_id) {
        this.withdraw_id = withdraw_id;
    }

    public int getIndex_number() {
        return index_number;
    }

    public void setIndex_number(int index_number) {
        this.index_number = index_number;
    }

    public String getBroken_item() {
        return broken_item;
    }

    public void setBroken_item(String broken_item) {
        this.broken_item = broken_item;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSum_price() {
        return sum_price;
    }

    public void setSum_price(int sum_price) {
        this.sum_price = sum_price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "OrderWithdrawlist{" + "id=" + id + ", withdraw_id=" + withdraw_id + ", index_number=" + index_number + ", broken_item=" + broken_item + ", type=" + type + ", quantity=" + quantity + ", amount=" + amount + ", sum_price=" + sum_price + ", note=" + note + '}';
    }
    
    
    
}

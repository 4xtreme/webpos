/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author Lenovo
 */
public class Orderlist {
        private int id;
        private int quotation_id;
        private int product_id;
        private String product_name;
        private int product_price;
        private int quantity;
        private int sum_proce;

    public Orderlist() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuotation_id() {
        return quotation_id;
    }

    public void setQuotation_id(int quotation_id) {
        this.quotation_id = quotation_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getProduct_price() {
        return product_price;
    }

    public void setProduct_price(int product_price) {
        this.product_price = product_price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSum_proce() {
        return sum_proce;
    }

    public void setSum_proce(int sum_proce) {
        this.sum_proce = sum_proce;
    }

    public Orderlist(int id, int quotation_id, int product_id, String product_name, int product_price, int quantity, int sum_proce) {
        this.id = id;
        this.quotation_id = quotation_id;
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_price = product_price;
        this.quantity = quantity;
        this.sum_proce = sum_proce;
    }

    @Override
    public String toString() {
        return "Orderlist{" + "id=" + id + ", quotation_id=" + quotation_id + ", product_id=" + product_id + ", product_name=" + product_name + ", product_price=" + product_price + ", quantity=" + quantity + ", sum_proce=" + sum_proce + '}';
    }
        
    
        
        
}

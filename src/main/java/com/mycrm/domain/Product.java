/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.util.Date;
import static javax.swing.text.StyleConstants.Size;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Lenovo
 */
public class Product {
    private int product_id;
    private int user_id;
    
     @NotNull
    @Size(min=2, max=30)
    private String product_name;
     
    private String detail;
    private String color;
    private String size;
    private int pirce;
    private int stock;
    private Date create_date;
    private Date sell_start_date;
    private Date sell_end_date;

    public Product() {
    }

    public Product(int product_id, int user_id, String product_name, String detail, String color, String size, int pirce, int stock, Date create_date, Date sell_start_date, Date sell_end_date) {
        this.product_id = product_id;
        this.user_id = user_id;
        this.product_name = product_name;
        this.detail = detail;
        this.color = color;
        this.size = size;
        this.pirce = pirce;
        this.stock = stock;
        this.create_date = create_date;
        this.sell_start_date = sell_start_date;
        this.sell_end_date = sell_end_date;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getPirce() {
        return pirce;
    }

    public void setPirce(int pirce) {
        this.pirce = pirce;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getSell_start_date() {
        return sell_start_date;
    }

    public void setSell_start_date(Date sell_start_date) {
        this.sell_start_date = sell_start_date;
    }

    public Date getSell_end_date() {
        return sell_end_date;
    }

    public void setSell_end_date(Date sell_end_date) {
        this.sell_end_date = sell_end_date;
    }

    @Override
    public String toString() {
        return "Product{" + "product_id=" + product_id + ", user_id=" + user_id + ", product_name=" + product_name + ", detail=" + detail + ", color=" + color + ", size=" + size + ", pirce=" + pirce + ", stock=" + stock + ", create_date=" + create_date + ", sell_start_date=" + sell_start_date + ", sell_end_date=" + sell_end_date + '}';
    }
    
    
    
}

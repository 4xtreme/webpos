/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.util.Date;

/**
 *
 * @author Lenovo
 */
public class Promotion {
 
   private int promotion_id;
   private String customer_type;
   private String topic;
   private String detail;  
   private Date create_date;

    public Promotion() {
    }

    public int getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(int promotion_id) {
        this.promotion_id = promotion_id;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(String customer_type) {
        this.customer_type = customer_type;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Promotion(int promotion_id, String customer_type, String topic, String detail, Date create_date) {
        this.promotion_id = promotion_id;
        this.customer_type = customer_type;
        this.topic = topic;
        this.detail = detail;
        this.create_date = create_date;
    }

    @Override
    public String toString() {
        return "Promotion{" + "promotion_id=" + promotion_id + ", customer_type=" + customer_type + ", topic=" + topic + ", detail=" + detail + ", create_date=" + create_date + '}';
    }
   


 
    
    
}

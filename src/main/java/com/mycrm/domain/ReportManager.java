/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import com.mysql.jdbc.Connection;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

/**
 *
 * @author Lenovo
 */
public class ReportManager {    

    public ReportManager() {
    }
    private String reportFunction = "";
    private String paramName = "";
    
    public void genReport(String reportFunction, String inputID,HttpServletRequest request,HttpServletResponse response){
        System.out.println("**********Start ReportManager System**********");
        System.out.println("Function call: " + reportFunction);
        this.reportFunction = reportFunction;
        String reportFileName = "";
        System.out.print("Select report: ");
    switch(reportFunction){
        case "Quotation":
            System.out.println("QuotationReport");
            reportFileName = "QuotationReport";
            paramName = "QtID";
            break;
        case "Billing":
            System.out.println("BillingReport");
            reportFileName = "BillingReport";
            paramName = "biD";
            break;
        case "Invoice":
            System.out.println("InvoiceReport");    
            reportFileName = "InvoiceReport";
            paramName = "invoiceID";
            break;
        default:
            System.out.println("Error don't have this function ");
            break;             
    }
     Connection conn = null;      
       try {
        try {
 
             Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Please include Classpath Where your MySQL Driver is located");
                e.printStackTrace();
            }   
         conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/billing","root",""); 
     if (conn != null){
         System.out.println("Database Connected");
     }
     else{
         System.out.println(" connection Failed ");
     }
           //Parameters as Map to be passed to Jasper
           HashMap<String,Object> hmParams=new HashMap<String,Object>(); 
           hmParams.put(paramName, new Integer(inputID)); 
           //hmParams.put(nameofparamIN ireport, value);
 
          JasperReport jasperReport = getCompiledFile(reportFileName, request);
          generateReportPDF(response, hmParams, jasperReport, conn); // For PDF report
 
    } catch (Exception sqlExp) { 
           System.out.println( "Exception::" + sqlExp.toString());
    } finally { 
            try { 
               if (conn != null) {
                   conn.close();
                   conn = null;
               }
           } catch (SQLException expSQL) {
               System.out.println("SQLExp::CLOSING::" + expSQL.toString());
           }
      }
        System.out.println("**********End ReportManager System**********");  
       
    }
       
    private JasperReport getCompiledFile(String fileName, HttpServletRequest request) throws JRException {
        System.out.println("path " + request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        File reportFile = new File(request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        System.out.println("reportFile.getPath()" + reportFile.getPath());
        // If compiled file is not found, then compile XML template
        if (!reportFile.exists()) {
            System.out.println("if reportFile");
            JasperCompileManager.compileReportToFile(request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jrxml"), request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
        }
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
        System.out.println("Get reportfile");
        return jasperReport;
    }
    
    private void generateReportPDF (HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn)throws JRException, NamingException, SQLException, IOException {
        byte[] bytes = null;
        try {
             bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }        

        resp.reset();    
        resp.resetBuffer();
        resp.setContentType("application/pdf");
        resp.setHeader("Content-Disposition", "attachment; filename=\""+generateFileName()+"\"");     // Delete! this line when you Don't need to show pop-up Download file.
        resp.setContentLength(bytes.length);
        ServletOutputStream ouputStream = resp.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        ouputStream.flush();
        ouputStream.close();
        System.out.println("Generate PDF file");        
    } 
    
    
    public byte[] genReportEmail(String reportFunction, String inputID,HttpServletRequest request){
        System.out.println("**********Start Email ReportManager System**********");
        System.out.println("Function call: " + reportFunction);
        String reportFileName = "";
        System.out.print("Select report: ");
    switch(reportFunction){
        case "Quotation":
            System.out.println("QuotationReport");
            reportFileName = "QuotationReport";
            paramName = "QtID";
            break;
        case "Billing":
            System.out.println("BillingReport");
            reportFileName = "BillingReport";
            paramName = "biD";
            break;
        case "Invoice":
            System.out.println("InvoiceReport");    
            reportFileName = "InvoiceReport";
            paramName = "invoiceID";
            break;
        default:
            System.out.println("Error don't have this function ");
            break;             
    }
     Connection conn = null;
     byte[] bytes = null;
       try {
        try { 
             Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Please include Classpath Where your MySQL Driver is located");
                e.printStackTrace();
            }   
         conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/billing","root",""); 
     if (conn != null){
         System.out.println("Database Connected");
     }
     else{
         System.out.println(" connection Failed ");
     }
           //Parameters as Map to be passed to Jasper
           HashMap<String,Object> hmParams=new HashMap<String,Object>(); 
           hmParams.put(paramName, new Integer(inputID)); 
           //hmParams.put(nameofparamIN ireport, value);
 
          JasperReport jasperReport = getCompiledFile(reportFileName, request);
          bytes = generatePDFforEmail(hmParams, jasperReport, conn);
    } catch (Exception sqlExp) { 
           System.out.println( "Exception::" + sqlExp.toString());
    } finally { 
            try { 
               if (conn != null) {
                   conn.close();
                   conn = null;
               }
           } catch (SQLException expSQL) {
               System.out.println("SQLExp::CLOSING::" + expSQL.toString());
           }
      }
        System.out.println("**********End ReportManager System**********");  
       return bytes;
    }
    
       private byte[] generatePDFforEmail ( Map parameters, JasperReport jasperReport, Connection conn)throws JRException, NamingException, SQLException, IOException {
        byte[] bytes = null;
        try {
             bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
        } catch (Exception e) {
            System.out.println("Error: " + e);
        } 
        System.out.println("Generate PDF file");   
        return bytes;
    } 
       
    public String generateFileName(){
        System.out.println("Generate filename");
        Date date = new Date();
        String[] getDate = date.toString().split(" ");
        getDate[3] = getDate[3].replaceAll(":", "");
        String newFileName = "CRMReport_" + reportFunction + "_" + getDate[5] + getDate[1] + getDate[2] + "_" + getDate[3].substring(0, 4) + ".pdf";
        System.out.println("File Name: " + newFileName);
        
        return newFileName;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

import java.util.Date;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Lenovo
 */
public class User {
    private int user_id;
    private String firstname;
    private String lastname;
    private String company_name;
    private String email; 
    private String password;
    private String phone1;
    private String phone2;
    private String address;
    private String district; 
    private String city; 
    private String country;
    private String postalcode;
    private String facebook;
    private String line;
    private Date last_activeDate;
    private Date last_login;

    public User() {
    }

    @Override
    public String toString() {
        return "User{" + "user_id=" + user_id + ", firstname=" + firstname + ", lastname=" + lastname + ", company_name=" + company_name + ", email=" + email + ", password=" + password + ", phone1=" + phone1 + ", phone2=" + phone2 + ", address=" + address + ", district=" + district + ", city=" + city + ", country=" + country + ", postalcode=" + postalcode + ", facebook=" + facebook + ", line=" + line + ", last_activeDate=" + last_activeDate + ", last_login=" + last_login + '}';
    }

    public User(int user_id, String firstname, String lastname, String company_name, String email, String password, String phone1, String phone2, String address, String district, String city, String country, String postalcode, String facebook, String line, Date last_activeDate, Date last_login) {
        this.user_id = user_id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.company_name = company_name;
        this.email = email;
        this.password = password;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.address = address;
        this.district = district;
        this.city = city;
        this.country = country;
        this.postalcode = postalcode;
        this.facebook = facebook;
        this.line = line;
        this.last_activeDate = last_activeDate;
        this.last_login = last_login;
    }

    

   
    public String fullName() {
        return firstname+" "+lastname;
    }
    
     public String fullPhone() {
        return phone1+" , "+phone2;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public Date getLast_activeDate() {
        return last_activeDate;
    }

    public void setLast_activeDate(Date last_activeDate) {
        this.last_activeDate = last_activeDate;
    }

    public Date getLast_login() {
        return last_login;
    }

    public void setLast_login(Date last_login) {
        this.last_login = last_login;
    }
   
}

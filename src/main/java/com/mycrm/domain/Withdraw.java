/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycrm.domain;

/**
 *
 * @author pop
 */
public class Withdraw {
    private int id;
    private int user_id;
    private int withdrawal_id;
    private int driver_id;
    private int amount;
    private int project_id;
    private int pay_type;
    private String pay_date;
    private int pay_date_count;
    private int store_id;
    private int machine_id;
    private String create_date;
    private String update_date;
    private String file;
    private String withdrawal_note;
    private String inspector_note;
    private String approvers_note;
    private int total_price;

    public Withdraw() {
    }

    public Withdraw(int id, int user_id, int withdrawal_id, int driver_id, int amount, int project_id, int pay_type, String pay_date, int pay_date_count, int store_id, int machine_id, String create_date, String update_date, String file, String withdrawal_note, String inspector_note, String approvers_note, int total_price) {
        this.id = id;
        this.user_id = user_id;
        this.withdrawal_id = withdrawal_id;
        this.driver_id = driver_id;
        this.amount = amount;
        this.project_id = project_id;
        this.pay_type = pay_type;
        this.pay_date = pay_date;
        this.pay_date_count = pay_date_count;
        this.store_id = store_id;
        this.machine_id = machine_id;
        this.create_date = create_date;
        this.update_date = update_date;
        this.file = file;
        this.withdrawal_note = withdrawal_note;
        this.inspector_note = inspector_note;
        this.approvers_note = approvers_note;
        this.total_price = total_price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getWithdrawal_id() {
        return withdrawal_id;
    }

    public void setWithdrawal_id(int withdrawal_id) {
        this.withdrawal_id = withdrawal_id;
    }

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getPay_type() {
        return pay_type;
    }

    public void setPay_type(int pay_type) {
        this.pay_type = pay_type;
    }

    public String getPay_date() {
        return pay_date;
    }

    public void setPay_date(String pay_date) {
        this.pay_date = pay_date;
    }

    public int getPay_date_count() {
        return pay_date_count;
    }

    public void setPay_date_count(int pay_date_count) {
        this.pay_date_count = pay_date_count;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public int getMachine_id() {
        return machine_id;
    }

    public void setMachine_id(int machine_id) {
        this.machine_id = machine_id;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getWithdrawal_note() {
        return withdrawal_note;
    }

    public void setWithdrawal_note(String withdrawal_note) {
        this.withdrawal_note = withdrawal_note;
    }

    public String getInspector_note() {
        return inspector_note;
    }

    public void setInspector_note(String inspector_note) {
        this.inspector_note = inspector_note;
    }

    public String getApprovers_note() {
        return approvers_note;
    }

    public void setApprovers_note(String approvers_note) {
        this.approvers_note = approvers_note;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    @Override
    public String toString() {
        return "withdraw{" + "id=" + id + ", user_id=" + user_id + ", withdrawal_id=" + withdrawal_id + ", driver_id=" + driver_id + ", amount=" + amount + ", project_id=" + project_id + ", pay_type=" + pay_type + ", pay_date=" + pay_date + ", pay_date_count=" + pay_date_count + ", store_id=" + store_id + ", machine_id=" + machine_id + ", create_date=" + create_date + ", update_date=" + update_date + ", file=" + file + ", withdrawal_note=" + withdrawal_note + ", inspector_note=" + inspector_note + ", approvers_note=" + approvers_note + ", total_price=" + total_price + '}';
    }
    
    
    
}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->


    <div class="content-wrapper">
    <div class="container">
        
            <div class="jumbotron">
                <div class="input  "><input type="text" name="search" placeholder="Search.."></div>
                <div class="block-1"><a href="${pageContext.request.contextPath}/billing/addcustomer"><button type="button" class="btn btn-success">+ Add CUSTOMER</button></a></div>
                <div class="block-2"><a href="${pageContext.request.contextPath}/billing/addcustomerbase"><button type="button" class="button button4">นำเข้า</button></a></div>
                <div class="block-3"><a href="#"><button type="button" class="button button4">ส่งออก</button></a></div>
                
                <div class="container">
                    <table class="table table-responsive-md table-hover "  width="100%" >
                    <thead>
                        <tr>
                            <th><div align="center"><input name="CheckAll" type="checkbox" id="chk_all" ></div></th>
                            <th><div align="center">ลูกค้า</div></th>
                            <th><div align="center">รายชื่อผู้ติดต่อ</div></th>
                            <th><div align="center">เยียมชมครั้งแรก</div></th>
                            <th><div align="center">เยี่ยมชมล่าสุด</div></th>
                            <th><div align="center">เข้าชมทั้งหมด</div></th>
                            <th><div align="center">Total spent</div></th>
                            <th><div align="center">แต้มสะสม</div></th> 
                        </tr>
                    </thead>
                    <tbody>
                       
                            <tr >
                                <td>
                                   <input type="checkbox" name="chkDel[]" id="chk_list">
                                </td>
                                <td><div align="center"></div></td>
                                <td><div align="center"></div></td>
                                <td><div align="center"></div></td>
                                <td><div align="center"></div></td>
                                <td><div align="center"></div></td>
                                <td><div align="center"></div></td>
                                <td><div align="center"></div></td>
                                
                                
                            </tr>
                       
                    </tbody>
                </table>
                </div>
               
                <script language="JavaScript">
                   $("#chk_all").click(function(){
                       $('input:checkbox').not(this).prop('checked', this.checked)
                   });
                </script>
                <div class="block-12"><a href="#"><button name="btnDelete" type="button" class="button button4">ลบ</button></a></div>
                
                
                <nav aria-label="Page navigation example">
                     <ul class="pagination">
                     <li class="page-item">
                          <a class="page-link" href="Customerbase.php?page=1" aria-label="Previous">
                              <span aria-hidden="true">&laquo;</span>
                          </a>
                     </li>
                          
                     <li class="page-item">
                         <a class="page-link" href="Customerbase.php?page=<?php echo $re; ?>"></a></li>
                           
                     <li class="page-item">
                         <a class="page-link" href="Customerbase.php?page=<?php echo $total_page;?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                         </a>
                     </li>
                     </ul>
                </nav>
                
            </div>

        </div>
        </div>



    
<!-- End Page Content -->
<jsp:include page="../footer_1.jsp" />
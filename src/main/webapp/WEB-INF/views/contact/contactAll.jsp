<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
        <div id="page-wrapper">
        <div class="container-fluid">
		<!-- start Content -->
            <div class="row">
				<div class="col-lg-12">
                        <h1 class="page-header">All Contacts</h1>
                </div>
			</div>
			

		
										
										<div class="row">
										<div class="col-lg-12">
										<div class="table-responsive">
                                    	<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#ID</th>
                                                <th class="text-center">Name	</th>
                                                <th class="text-center">Email</th>
												<th class="text-center">Phone</th>
                                                                                                <th class="text-center">Member Type</th>
												<th class="text-center">Line</th>
                                                                                                <th class="text-center">Facebook</th>
                                                                                                <th class="text-center">Address</th>
                                                                                                 <th class="text-center">Sub-District</th>
                                                                                                  <th class="text-center">District</th>
                                                                                                   <th class="text-center">Province</th>
												<th class="text-center">Edit</th>
                                                                                                <th class="text-center">Delete</th>
												
                                            </tr>
                                        </thead>
                                        <tbody>
                                              
                                             <c:forEach var="contactlist" items="${contactlists}" varStatus="loop">
                                            <tr>
                                                <td class="text-center">${contactlist.customer_id}</td>
                                                <td class="text-center">${contactlist.firstname} &nbsp;&nbsp;${contactlist.lastname}</td>
                                                <td class="text-center">${contactlist.email}</td>
		                                <td class="text-center">${contactlist.phone1},&nbsp;&nbsp;${contactlist.phone2}</td>
                                                <td class="text-center">${contactlist.member_type}</td>
                                                <td class="text-center">${contactlist.line}</td>
						<td class="text-center">${contactlist.facebook}</td>
                                                <td class="text-center">${contactlist.address1}</td>
                                                <td class="text-center">${contactlist.district}</td>
                                                <td class="text-center">${contactlist.city}</td>
                                                <td class="text-center">${contactlist.country}</td>
                                                
					        <td class="text-center">
                                                    <form  action="selectedit" method="post" >
                                                            <input type="hidden" value="${contactlist.customer_id}" name="editid"/>
                                                            <button type="submit" class="btn btn-warning btn-circle "><i class="fa fa-edit"></i></button>
                                                        </form>
                                                </td>
                                                 <td class="text-center">
                                                        <form  id="delete_${contactlist.customer_id}" action="delete" method="post" >                                                                  
                                                            <input type="hidden" value="${contactlist.customer_id}" name="id"/>
                                                            <div class="btn btn-danger btn-circle " onclick="chkConfirm(${contactlist.customer_id});" ><i class="fa fa-remove"></i></div>
                                                        </form>
                                                    </td>
                                            </tr>
                                             </c:forEach>
                                              
                                         
                                        </tbody>
                                    </table>
                                	</div>
									</div>
									</div>
									
                                    </div>
                               

				  </div>
		
			<!-- End Content -->
  <script language="JavaScript">
    function chkConfirm(customer_id){
	 bootbox.confirm({
        message: "Are you sure you want to delete?"+customer_id,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {  
            if(result){
                $("#delete_"+customer_id).submit();
            }
            console.log('This was logged in the callback: ' + result+"#delete_"+customer_id);
        }
    });}
</script>
 <jsp:include page="../footer.jsp" />

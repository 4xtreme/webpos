<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<link href="${pageContext.request.contextPath}/css/categorie.css" rel="stylesheet">
 <!-- Page Content -->
 <div class="content-wrapper">
     <div class="container">       
              <div class="btn-group book-1">
                <button type="button" class="btn">วันนี้</button>
                <button type="button" class="btn">เมื่อวานนี้</button>
                <button type="button" class="btn">สัปดาห์นี้</button>
                <button type="button" class="btn">เดือนนี้</button>
              </div>  
            <div class="book-2" id="widget">
                        <div id="widgetField">
                            <span>28 July, 2008 - 31 July, 2008</span>
                            <a href="#">Select date range</a>
                        </div>
                            <div id="widgetCalendar"></div>
                    </div>     

               <div class="btn-group book-3">
                    <button class="btn" type="button" data-toggle="dropdown">ตลอดทั้งวัน &nbsp;&nbsp;<i class="fa fa-clock-o "></i></button>
                        <ul class="dropdown-menu">
                           <li><a href="#" class="btn ">ตลอดทั้งวัน</a></li>
                           <li><a href="#" class="btn " >ระยะเวลาที่กำหนด</a></li>                   
                        </ul>
               </div> 
            
              <div class="btn-group book-4">
                    <button class="btn" type="button" data-toggle="dropdown">พนักงานทั้งหมด &nbsp;&nbsp;</button>
                        <ul class="dropdown-menu">
                           <li><a href="#" class="btn"><input name="checkbox[]" type="checkbox" id="checkbox" value="limit"/> พนักงานทั้งหมด</a></li>                
                        </ul>
            </div>
            
                <div class="btn-group book-5">
                    <button type="button" class="btn">ส่งออก</button>
                </div>
            
            </div>  <br><br><br>
     
     <div class="jumbotron">
              <div class="container">
              
             
                <div class="container">
                     <div class="table-responsive">
                <table  class="table table-hover" widht="100%" height="100%" align="center">
                    
                    <thead>
                        <tr>
                            <th><div align="center">ชื่อ</div></th>
                            <th><div align="center">ยอดขาย</div></th>
                            <th><div align="center">คืนเงิน</div></th>
                            <th><div align="center">ส่วนลด</div></th>
                            <th><div align="center">ยอดขายสุทธิ์</div></th>
                            <th><div align="center">ใบเสร็จ</div></th>
                            <th><div align="center">ราคาขายเฉลี่ย</div></th>
                            <th><div align="center">ลูกค้าที่ลงทะเบียน</div></th>
                        </tr>
                    </thead>
                    
                </table> 
                </div> 
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="employeereport.php?page=1" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="employeereport.php?page=<?php echo $result; ?>"><?php echo $result; ?></a></li>
               
                    <li class="page-item">
                        <a class="page-link" href="employeereport.php?page=<?php echo $total_page;?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
                </div>
            </div>

        </div>
 </div>
 
 
  <jsp:include page="../footer_1.jsp" />
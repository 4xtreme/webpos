<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->


        <div class="content-wrapper">
      <div class="container">
          <div class="row">
          <div class="btn-group bck-1">
            <button type="button" class="btn">วันนี้</button>
            <button type="button" class="btn">เมื่อวานนี้</button>
            <button type="button" class="btn">สัปดาห์นี้</button>
            <button type="button" class="btn">เดือนนี้</button>
          </div>

            <div class="bck-2" id="widget">
                <div id="widgetField">
                    <span>28 July, 2008 - 31 July, 2008</span>
                    <a href="#">Select date range</a>
                </div>
                    <div id="widgetCalendar"></div>
            </div>     

            <div class="btn-group bck-3">
                <button class="btn" type="button" data-toggle="dropdown">ตลอดทั้งวัน &nbsp;&nbsp;<i class="fa fa-clock-o "></i></button>
                    <ul class="dropdown-menu">
                       <li><a href="#" class="btn ">ตลอดทั้งวัน</a></li>
                       <li><a href="#" class="btn " >ระยะเวลาที่กำหนด</a></li>                   
                    </ul>
            </div> 

             <div class="btn-group bck-4">
                <button class="btn" type="button" data-toggle="dropdown"> พนักงานทั้งหมด &nbsp;&nbsp;</button>
                    <ul class="dropdown-menu">
                       <li><a href="#" class="btn"><input name="checkbox[]" type="checkbox" id="checkbox" value="limit"/> พนักงานทั้งหมด</a></li>                
                    </ul>
            </div>

            <div class="btn-group bck-5">
                <button type="button" class="btn">ส่งออก</button>

            </div>

        </div>  
              
         
          <div class="container">
            <div class="jumbotron1">
                <div class="jumbotron1">
                <div class="row">
                    <div class=" col-md-4" style="background-color: #E6E6FA">
                        <div class="row table1">
                            <div class="xo1">
                                <h8>สินค้า 5 อันดับแรก</h8>
                                
                            </div>
                            <div class="xo2">
                                <h8>ยอดเงิน</h8>
                                
                            </div>
                        </div>
                        <hr><p>
                        <div class="padding-0-15">
                           
                            <div class="xo1"></div>
                            <div class="xo2"></div>
                                
                        </div>
                    
                    </div>
                    
                    <div class="col-md-8" style="background-color: #E6E6FA">
                        <div class="row tabal">
                            <div class="xo3">
                                <h8>ยอดขาย แยกตาม สินค้า (แผนภาพ)</h8>
                                <img >
                            </div>
                            <div class="xo7">
                                <div>
                                    <a href="#"><div  role="button" class=" xo4 fa fa-area-chart" style="font-size:24px" ></div></a>
                                    <a href="#"><div  role="button" class=" xo5 fa fa-bar-chart" style="font-size:24px"></div></a>
                                    <a href="#"><div  role="button" class=" xo6 fa fa-pie-chart" style="font-size:24px"></div></a>
                                </div>
                            </div>
                            <div class="xo8" style="width: 150px">
                                 <select class="form-control">
                                        <option value="0">กลุ่ม:</option>
                                        <option value="1">รายชั่วโมง:</option>
                                        <option value="2">รายวัน</option>
                                        <option value="3">ร่ายสัปดาห์</option>
                                        <option value="4">รายเดือน</option>
                                        <option value="5">รายไตรมาส</option>
                                        <option value="6">รายปี</option>
                                    </select>
                             </div>    
                        </div>
                        <hr><p>
                        <div class="height-100">
                            <div style="height:300px" class="height-100">
                                <div class="text-center">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="table-info">
                        <div ng-if="(windowWidth > 800)" class="">
                            <table class="width-100">
                                <tbody>
                                    <tr class="">
                                        <td colspan="7" class=""></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                
                </div>
              
            </div>
                </div>
          </div>
          
          
          
          
          
<script>
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
</script>          
        
        
        
        
          
          
          
          
     </div> 
  </div>



    
<!-- End Page Content -->
<jsp:include page="../footer_1.jsp" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->


<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="btn-group bck-1">
                <button type="button" class="btn">วันนี้</button>
                <button type="button" class="btn">เมื่อวานนี้</button>
                <button type="button" class="btn">สัปดาห์นี้</button>
                <button type="button" class="btn">เดือนนี้</button>
            </div>

            <div class="bck-2" id="widget">
                <div id="widgetField">
                    <span>28 July, 2008 - 31 July, 2008</span>
                    <a href="#">Select date range</a>
                </div>
                <div id="widgetCalendar"></div>
            </div>     

            <div class="btn-group bck-3">
                <button class="btn" type="button" data-toggle="dropdown">ตลอดทั้งวัน &nbsp;&nbsp;<i class="fa fa-clock-o "></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#" class="btn ">ตลอดทั้งวัน</a></li>
                    <li><a href="#" class="btn " >ระยะเวลาที่กำหนด</a></li>                   
                </ul>
            </div> 

            <div class="btn-group bck-4">
                <button class="btn" type="button" data-toggle="dropdown"> พนักงานทั้งหมด &nbsp;&nbsp;</button>
                <ul class="dropdown-menu">
                    <li><a href="#" class="btn"><input name="checkbox[]" type="checkbox" id="checkbox" value="limit"/> พนักงานทั้งหมด</a></li>                
                </ul>
            </div>

            <div class="btn-group bck-5">
                <button type="button" class="btn">ส่งออก</button>

            </div>









        </div>       
    </div>
</div>


    
<!-- End Page Content -->
<jsp:include page="../footer_1.jsp" />
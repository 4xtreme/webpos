<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->
<div class="content-wrapper"> 
    <div class="container" id="mainpage" style="width: 100%">  
        <div class="container">
            <div class="row">
                <div class="btn-group bck-1">
                    <button type="button" class="btn">วันนี้</button>
                    <button type="button" class="btn">เมื่อวานนี้</button>
                    <button type="button" class="btn">สัปดาห์นี้</button>
                    <button type="button" class="btn">เดือนนี้</button>
                </div>

                <div class="bck-2" id="widget">
                    <div id="widgetField">
                        <span>28 July, 2008 - 31 July, 2008</span>
                        <a href="#">Select date range</a>
                    </div>
                    <div id="widgetCalendar"></div>
                </div>     

                <div class="btn-group bck-3">
                    <button class="btn" type="button" data-toggle="dropdown">ตลอดทั้งวัน &nbsp;&nbsp;<i class="fa fa-clock-o "></i></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="btn ">ตลอดทั้งวัน</a></li>
                        <li><a href="#" class="btn " >ระยะเวลาที่กำหนด</a></li>                   
                    </ul>
                </div> 

                <div class="btn-group bck-4">
                    <button class="btn" type="button" data-toggle="dropdown"> พนักงานทั้งหมด &nbsp;&nbsp;</button>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="btn"><input name="checkbox[]" type="checkbox" id="checkbox" value="limit"/> พนักงานทั้งหมด</a></li>                
                    </ul>
                </div>

                <div class="btn-group bck-5">
                    <button type="button" class="btn">ส่งออก</button>

                </div>
            </div>       
        </div>
    </div>

    <br>
    <div class="container" id="mainpage" style="width: 100%">
        <div class="jumbotron" >
            <div></div>
            <div class="row">
                <div class="col-sm-4">
                    <div > 
                        <p> <span class="glyphicon glyphicon-usd"></span>&nbsp;ยอดขายสุทธิ : </p>
                        <div>

                        </div>
                    </div>
                    <div>
                        <p> <span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;ยอดขาย : </p>
                        <div>

                        </div>
                    </div>
                    <div>
                        <p> <span class="glyphicon glyphicon-share-alt"></span>&nbsp;คืนเงิน : </p>
                        <div>

                        </div>
                    </div>
                    <div>
                        <p> <span class="glyphicon glyphicon-tag"></span>&nbsp;ส่วนลด : </p>
                        <div>

                        </div>
                    </div>

                </div>
                <hr>
                <div class="col-sm-8">
                    <div class="container">
                        <table style="width: 100%">
                            <tr>
                                <td><label style="font-size: medium">รายได้สำหรับช่วงเวลา</label></td>
                                <td> <button class="btn1"><i class="fa fa-line-chart"></i></button>
                                <button class="btn1"><i class="fa fa-bar-chart"></i></button></td>
                                <td>
                                    <button class="btn1 btn1-secondary btn1-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        กลุ่ม รายวัน
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">รายชั่วโมง</a>
                                        <a class="dropdown-item" href="#">รายวัน</a>
                                        <a class="dropdown-item" href="#">ายสัปดาห์</a>
                                        <a class="dropdown-item" href="#">รายเดือน</a>
                                        <a class="dropdown-item" href="#">รายไตมาส</a>
                                        <a class="dropdown-item" href="#">รายปี</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="container" id="mainpage" style="width: 100%">
        <div class="jumbotron" >
            <table  class="table table-striped table-bordered table-hover table-condensed " >
                <thead align="center">
                    <tr>
                        <th>
                            วันที่
                        </th>
                        <th>
                            ยอดขาย
                        </th>
                        <th>
                            คืนเงิน
                        </th>
                        <th>
                            ส่วนลด
                        </th>
                        <th>
                            ยอดขายสุทธิ
                        </th>

                    </tr>
                </thead>
                <tbody align="center">

                </tbody>
            </table>
            <script language="JavaScript">
                $("#chk_all").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked)
                });
            </script>
        </div>

    </div>
</div>

<jsp:include page="../footer_1.jsp" />


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->
   

            <div class="content-wrapper">
      <div class="container">
          <div class="row">
          <div class="btn-group bck-1">
            <button type="button" class="btn">วันนี้</button>
            <button type="button" class="btn">เมื่อวานนี้</button>
            <button type="button" class="btn">สัปดาห์นี้</button>
            <button type="button" class="btn">เดือนนี้</button>
          </div>

            <div class="bck-2" id="widget">
                <div id="widgetField">
                    <button class="btn" type="button" id="widgetField" ><span>28 July, 2008 - 31 July, 2008</span></button>
                    <i class="fa fa-calendar"></i>
                    <a href="#">Select date range</a>
                </div>
                    <div id="widgetCalendar"></div>
            </div>     

            <div class="btn-group bck-3">
                <button class="btn" type="button" data-toggle="dropdown">ตลอดทั้งวัน &nbsp;&nbsp;<i class="fa fa-clock-o "></i></button>
                    <ul class="dropdown-menu">
                       <li><a href="#" class="btn ">ตลอดทั้งวัน</a></li>
                       <li><a href="#" class="btn " >ระยะเวลาที่กำหนด</a></li>                   
                    </ul>
            </div> 

             <div class="btn-group bck-4">
                <button class="btn" type="button" data-toggle="dropdown"> พนักงานทั้งหมด &nbsp;&nbsp;</button>
                    <ul class="dropdown-menu">
                       <li><a href="#" class="btn"><input name="checkbox[]" type="checkbox" id="checkbox" value="limit"/> พนักงานทั้งหมด</a></li>                
                    </ul>
            </div>

            <div class="btn-group bck-5">
                <button type="button" class="btn">ส่งออก</button>

            </div>

        
          
          <div class="container">
          <div class="jumbotron1">
              <div class="row">
                  <ul class="v_men" >
                  <li class="">
                      <div class="bv1">
                          <div class="bn" id="div1">
                          <img src="${pageContext.request.contextPath}/images/a1.png">
                          </div>
                          <div class="bn" id="div2">
                              <div>
                                  <h4>0.00</h4>
                              </div>
                              <div>
                                  <h7>รายได้ที่ต้องเสียภาษี</h7>
                              </div>
                          </div>
                      </div>
                  </li>
                  <li class="">
                      <div class="bv1">
                          <div class="bn" id="div1">
                          <img src="${pageContext.request.contextPath}/images/a2.png">
                          </div>
                          <div class="bn" id="div2">
                              <div>
                                  <h4>0.00</h4>
                              </div>
                              <div>
                                  <h7>การขายที่ไม่ต้องเสียภาษี</h7>
                              </div>
                          </div>
                      </div>
                  </li>
                  <li class="">
                      <div class="bv1">
                          <div class="bn" id="div1">
                          <img src="${pageContext.request.contextPath}/images/a3.png">
                          </div>
                          <div class="bn" id="div2">
                              <div>
                                  <h4>0.00</h4>
                              </div>
                              <div>
                                  <h7>ยอดขายรวมสุทธิ</h7>
                              </div>
                          </div>
                      </div>
                  </li>
                  
                  
              </ul>
          </div>
          </div>
          </div>
          
        
        <div class="container">
             <table class="table table-responsive-md table-hover "  width="100%" >
                 <thead>
                        <tr>
                            <th><div align="center"><h7>ชื่อภาษี</h7></div></th>
                            <th><div align="center"><h7>รายได้ที่ต้องเสียภาษี</h7></div></th>
                            <th><div align="center"><h7>ยอดเงินภาษี</h7></div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><div align="center"></div></td>
                            <td><div align="center"></div></td>
                            <td><div align="center"></div></td>
<!--                            <td><div align="center"><?php echo $re['link'];?></div></td>
                            <td><div align="center"><?php echo $re['linkin'];?></div></td>
                            <td><div align="center"><?php echo $re['into'];?></div></td>
                            <td><div align="center"><?php echo $re['total'];?></div></td>-->
                        </tr>
                        
                    </tbody>
            </table>
        
        </div>
          
          
          
          
      
  </div>
</div> 
    
                          
</div>
        



    
<!-- End Page Content -->
<jsp:include page="../footer_1.jsp" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
    <div id="page-wrapper">
       <div class="container-fluid">
		<!-- start Content -->
            <div class="row">
				<div class="col-lg-12">
                        <h1 class="page-header">Dashboard </h1>
                </div>
			</div>
			
	             <div class="row">
             <div class="col-lg-12">
                  <h4 class="page-header">Quotation </h4>
               <div class="table-responsive">
                                    	<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Quatation ID</th>
                                                <th class="text-center">Customer ID	</th>
                                                <th class="text-center">Due Date</th>
                                                <th class="text-center">Status</th>
                                               
                                                <th class="text-center">Send Email</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                    <c:if test="${quatationlist != null}">
                                     
                                    <c:forEach var="quatationlists" items="${quatationlist}" varStatus="loop">
                                        <c:if test="${quatationlists.sent_email != true or quatationlists.status != true}">
                                    <tr>
                                                <td class="text-center">${quatationlists.quotation_id}</td>
                                                <td class="text-center">${quatationlists.customer_id}</td>                                                
                                                <td class="text-center">${quatationlists.due_date}</td>
                                                  <td class="text-center">
                                                    
                                                    <c:if test="${quatationlists.status == true}">
                                                           <button type="botton" class="btn btn-success btn-circle "><i class="fa fa-check"></i></button>                               
                                                    </c:if>
                                                    <c:if test="${quatationlists.status == false}">
                                                           <button type="botton" class="btn btn-danger btn-circle "><i class="fa fa-close"></i></button>                                 
                                                    </c:if>
                                                        
                                                </td>
                                               
                                                 <td class="text-center"> 
                                                     <c:if test="${quatationlists.sent_email == false}">
                                                    <button type="botton" class="btn btn-danger btn-circle "><i class="fa fa-close"></i></button> 
                                                        
                                                </c:if>
                                                      <c:if test="${quatationlists.sent_email == true}">
                                                   <button type="botton" class="btn btn-success btn-circle "><i class="fa fa-check"></i></button>  
                                                </c:if>
                                                </td>
                                            </tr> 
                                            </c:if>
                                     </c:forEach>
                                            
                                
                                 </c:if>
                                        </tbody>
                                                                           
                                    </table>
                </div>
                 <!-- End Content -->
             </div>
             <!-- End Page Content -->

         </div>         
              
			<!-- End Table -->
                        <div class="row">
             <div class="col-lg-12">
                  <h4 class="page-header">Billing </h4>
               <div class="table-responsive">
                                    	<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Billing ID</th>
                                                <th class="text-center">Customer ID	</th>
                                                <th class="text-center">Due Date</th>
                                                <th class="text-center">Status</th>
                                               
                                                <th class="text-center">Send Email</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                    <c:if test="${billinglist != null}">  
                                    <c:forEach var="billinglist" items="${billinglist}" varStatus="loop">
                                        <c:if test="${billinglist.sent_email != true or billinglist.status != true}">
                                    <tr>
                                                <td class="text-center">${billinglist.billing_id}</td>
                                                <td class="text-center">${billinglist.customer_id}</td>                                                
                                                <td class="text-center">${billinglist.due_date}</td>
                                                  <td class="text-center">
                                                    
                                                    <c:if test="${billinglist.status == true}">
                                                           <button type="botton" class="btn btn-success btn-circle "><i class="fa fa-check"></i></button>                               
                                                    </c:if>
                                                    <c:if test="${billinglist.status == false}">
                                                             <button type="botton" class="btn btn-danger btn-circle "><i class="fa fa-close"></i></button>                                
                                                    </c:if>
                                                        
                                                </td>
                                               
                                                 <td class="text-center"> 
                                                     <c:if test="${billinglist.sent_email == false}">
                                                       <button type="botton" class="btn btn-danger btn-circle "><i class="fa fa-close"></i></button> 
                                                        
                                                </c:if>
                                                      <c:if test="${billinglist.sent_email == true}">
                                                     <button type="botton" class="btn btn-success btn-circle "><i class="fa fa-check"></i></button>  
                                                </c:if> 
                                                </td>
                                            </tr>  
                                            </c:if>
                                     </c:forEach>
                                            
                                
                                 </c:if>
                                        </tbody>
                                                                           
                                    </table>
                </div>
                 <!-- End Content -->
             </div>
             <!-- End Page Content -->

         </div>         
              
			<!-- End Table -->
                        
                                  <div class="row">
             <div class="col-lg-12">
                  <h4 class="page-header">Invoice </h4>
               <div class="table-responsive">
                                    	<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Invoice ID</th>
                                                <th class="text-center">Customer ID	</th>
                                                <th class="text-center">Due Date</th>
                                                <th class="text-center">Status</th>
                                               
                                                <th class="text-center">Send Email</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                    <c:if test="${invoicelist != null}">  
                                    <c:forEach var="invoicelist" items="${invoicelist}" varStatus="loop">
                                        <c:if test="${invoicelist.sent_email != true or invoicelist.status != true}">
                                    <tr>
                                        
                                                <td class="text-center">${invoicelist.invoice_id}</td>
                                                <td class="text-center">${invoicelist.customer_id}</td>                                                
                                                <td class="text-center">${invoicelist.due_date}</td>
                                                  <td class="text-center">
                                                    
                                                    <c:if test="${invoicelist.status == true}">
                                                          <button type="botton" class="btn btn-success btn-circle "><i class="fa fa-check"></i></button>                                 
                                                    </c:if>
                                                    <c:if test="${invoicelist.status == false}">
                                                       <button type="botton" class="btn btn-danger btn-circle "><i class="fa fa-close"></i></button>                          
                                                    </c:if>
                                                        
                                                </td>
                                               
                                                 <td class="text-center"> 
                                                     <c:if test="${invoicelist.sent_email == false}">
                                                     <button type="botton" class="btn btn-danger btn-circle "><i class="fa fa-close"></i></button> 
                                                        
                                                </c:if>
                                                      <c:if test="${invoicelist.sent_email == true}">
                                                   <button type="botton" class="btn btn-success btn-circle "><i class="fa fa-check"></i></button>  
                                                </c:if>
                                                </td>
                                            </tr> 
                                            </c:if>
                                     </c:forEach>
                                            
                                
                                 </c:if>
                                        </tbody>
                                                                           
                                    </table>
                </div>
                 <!-- End Content -->
             </div>
             <!-- End Page Content -->

         </div>         
              
			<!-- End Table -->
        </div>
	<!-- End Page Content -->

</div>
 <jsp:include page="../footer.jsp" />
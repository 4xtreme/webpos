<%-- 
    Document   : footer
    Created on : Jan 11, 2017, 1:40:02 PM
    Author     : chatcharin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/js/bootbox.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/metisMenu.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/startmin.js"></script>

<!-- Custom search-filters -->
<script src="${pageContext.request.contextPath}/js/search-filters.js"></script>
<!--Date Time -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/locales/bootstrap-datetimepicker.th.js" charset="UTF-8"></script>

<!-- Grap -->

 <script type="text/javascript" src="${pageContext.request.contextPath}/js/raphael.min.js"></script>
 <script type="text/javascript" src="${pageContext.request.contextPath}/js/morris.min.js"></script>
<!-- toggle --> 
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

 <!-- text area -->
 <script src="${pageContext.request.contextPath}/js/tinymce/jquery.tinymce.min.js"></script>
  <script src="${pageContext.request.contextPath}/js/tinymce/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textareaTest' });</script>

<script type="text/javascript" >
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
    }, 4000);
    
    function rand(filter, length, current) {
  current = current ? current : '';
  var types = {
    number: "0123456789",
    uppercase: "ABCDEFGHIJKLMNOPQRSTUVWXTZ",
    lowercase: "abcdefghiklmnopqrstuvwxyz"
  };
  var r = (filter[0] === "all" ? Object.keys(types) : filter).reduce((s, t) => {
    s += types[t];
    return s
  }, "")
  return length ? rand(filter, --length
  , r.charAt(Math.floor(Math.random() * r.length)) + current) : current;
}


   
  $('.datetime').datetimepicker({
        language:  'th',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1
    });
    
    $('.form_date').datetimepicker({
        language:  'th',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    


</script>
</body>
</html>


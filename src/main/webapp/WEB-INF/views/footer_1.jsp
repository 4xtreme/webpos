<%-- 
    Document   : footer
    Created on : Jan 11, 2017, 1:40:02 PM
    Author     : chatcharin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
 
    <!-- Bootstrap core JavaScript-->
            <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.bundle.min.js"></script>
            <!-- Core plugin JavaScript-->
            <script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>
            <!-- Page level plugin JavaScript-->
            <script src="${pageContext.request.contextPath}/js/Chart.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
            <script src="${pageContext.request.contextPath}/js/dataTables.bootstrap4.js"></script>
            <!-- Custom scripts for all pages-->
            <script src="${pageContext.request.contextPath}/js/sb-admin.min.js"></script>
            <!-- Custom scripts for this page-->
            <script src="${pageContext.request.contextPath}/js/sb-admin-datatables.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/sb-admin-charts.min.js"></script>

            <script src="${pageContext.request.contextPath}/js/custom-file-input.js"></script>
                       
            <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
            <script type="text/javascript" src="${pageContext.request.contextPath}/js/datepicker.js"></script>
            <script type="text/javascript" src="${pageContext.request.contextPath}/js/eye.js"></script>
            <script type="text/javascript" src="${pageContext.request.contextPath}/js/utils.js"></script>
            <script type="text/javascript" src="${pageContext.request.contextPath}/js/layout.js?ver=1.0.2"></script>

            <script>(function (e, t, n) {
                                var r = e.querySelectorAll("html")[0];
                                r.className = r.className.replace(/(^|\s)no-js(\s|$)/, "$1js$2")
                            })
                                    (document, window, 0);
            </script>
           </div>
    </div>
</div> 
</body>
</html>


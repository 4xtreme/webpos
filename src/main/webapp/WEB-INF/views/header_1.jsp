<%-- 
    Document   : header
    Created on : Jan 11, 2017, 1:38:57 PM
    Author     : chatcharin
--%>


<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Maintenance Expense</title>

         <title>HOME</title>
  <!-- Bootstrap core CSS-->
  <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
  <!--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
  <link href="${pageContext.request.contextPath}/css/bootstrap-grid.css" rel="stylesheet">
  <!--<link href="vendor/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet">-->
  <link href="${pageContext.request.contextPath}/css/bootstrap-reboot.css" rel="stylesheet">
  <!--<link href="vendor/bootstrap/css/bootstrap-reboot.min.css" rel="stylesheet">-->
  <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="${pageContext.request.contextPath}/css/dataTables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="${pageContext.request.contextPath}/css/sb-admin.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/css/simple-sidebar.css" rel="stylesheet">
  <link rel="shortcut icon" href="${pageContext.request.contextPath}favicon.ico">
  <meta name="description" content="Demo for the tutorial: Styling and Customizing File Inputs the Smart Way" />
<meta name="keywords" content="cutom file input, styling, label, cross-browser, accessible, input type file" />
<meta name="author" content="Osvaldas Valutis for Codrops" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/component.css" />
    <link href="${pageContext.request.contextPath}/css/cssSheet.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/datepicker.css" type="text/css" />
    <link rel="stylesheet" media="screen" type="text/css" href="${pageContext.request.contextPath}/css/layout.css" />
    
    </head>
    <body class="fixed-nav sticky-footer bg-dark" id="page-top"  >

        

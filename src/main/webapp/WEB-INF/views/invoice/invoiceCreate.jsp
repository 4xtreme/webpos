<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->



        <div class="row">
            <div class="col-lg-12">

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="Profile">


                        <hr>
                        <h4>To</h4>

                        <select class="form-control " id="secustomer"  onchange="eiei()" >
                              <option  value="0" disabled=" " selected="">Select Customer</option>
                            <c:forEach var="contactlist" items="${contactlists}" varStatus="loop">
                                <option value="${contactlist.customer_id}">${contactlist.firstname} ${contactlist.lastname}</option>
                            </c:forEach>                                           
                        </select>
                        <br>
                        <br>
                        <dt>Deadlines </dt> <dd>
                                                            <div class='input-group date' >
                                                                <input  class="form-control datetime" onchange="duedateSet()" id="duedate" required=""/>
                                                                <!--form:input class="form-control datetime" path="sell_start_date"  required=""/--> <!--form:errors path="sell_start_date" /-->
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>                                                            
                                                           </dd>
                        <p></p>



                    </div>

                </div>
                

            </div>
        </div>




        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> Add Product  </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table  class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>

                                <th class="text-center">Product	</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Amount Product </th>
                                <th class="text-center">Add</th>

                            </tr>
                        </thead>
                        <tbody>



                            <tr  >
                                <td class="text-center">

                                    <select  class="form-control " id="seproduct"   onchange="showPrice()"> 
                                        <option  value="" disabled=" " selected="">Select product</option>
                                        <c:forEach var="productgetlists" items="${productgetlist}" varStatus="loop">
                                            <option value="${productgetlists.product_name}">${productgetlists.product_name}</option>

                                        </c:forEach>                                           
                                    </select> 
                                </td>

                                <td class="text-center"><label  class="form-control" id="seprice"> </label></td>
                                <td class="text-center"><input  class="form-control"  id="secount"></td>


                                <td class="text-center">          
                                    <button  onclick="createRowSE()" type="botton" class="btn btn-warning btn-circle "><i class="fa fa-edit"></i></button>

                                </td>
                            </tr>                           
                        <td class="text-center"><input id="product" class="form-control"   ></td>
                        <td class="text-center"><input id="price" class="form-control"   ></td>
                        <td class="text-center"><input id="count"  class="form-control"   ></td>

                        <td class="text-center">          
                            <button onclick="createRow()"   type="botton" class="btn btn-warning btn-circle "><i class="fa fa-edit"></i></button>

                        </td>
                        

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> Item </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                     <form action="invoicetablelist" method="POST" >
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr>
                                <th class="text-center">#ID</th>
                                <th class="text-center">Product</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Amount Product</th>
                                <th class="text-center">Total</th>
                            </tr>
                        </thead>
                       
                        <tbody   id="tableToModify">
                            <!-- สร้างตารางใหม่ -->

                        </tbody>
                    </table>
                         <input hidden="" name="duedate" id="duedateIN"  />
                         <input hidden="" name="selectcontact" id="contactid" required="" oninvalid="this.setCustomValidity('Please Select Customer')"/>
                        <div class="form-group text-right"><button type="submit" class="btn btn-primary"> <span class="fa fa-save "></span> Save</button></div>
                    </form>
                </div>
            </div>
        </div>

        
        <!-- End Content -->
    </div>
    <!-- End Page Content -->

</div>


<jsp:include page="../footer.jsp" />
<script language="JavaScript">
    var idcount =0;
    
    function showPrice() {
        
        
        //pirce
        var i = document.getElementById("seproduct").selectedIndex;
        var peopleList = new Array();

    <c:forEach items="${productgetlist}" var="ppl">
        var people = new Object();

        people.pirce = '${ppl.pirce}';

        peopleList.push(people);
    </c:forEach>
        document.getElementById("seprice").innerHTML = peopleList[i - 1].pirce;
        $('#seprice').val(peopleList[i - 1].pirce);
        //alert(peopleList[i-1].name);

    }
    function createRowSE() {
        idcount+=1;
        var seproduct = document.getElementById("seproduct").value;
        var seprice = document.getElementById("seprice").value;
        var secount = document.getElementById("secount").value;

        var sesumCount = seprice * secount;

        var row = document.createElement('tr'); // create row node
        var col = document.createElement('td'); // create column node
        var col2 = document.createElement('td'); // create second column node
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var col5 = document.createElement('td');
        
        
        row.appendChild(col); // append first column to row
        row.appendChild(col2); // append second column to row
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(col5);
        col.innerHTML = idcount; // put data in first column
        col2.innerHTML = seproduct;
        col3.innerHTML = seprice;
        col4.innerHTML = secount;
        col5.innerHTML = sesumCount;// put data in second column
        var table = document.getElementById("tableToModify"); // find table to append to
        table.appendChild(row); // append row to table
        
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "testcreatename");
        input.setAttribute("value", [idcount,seproduct,seprice,secount,sesumCount,calproduct(),"#"]);
        table.appendChild(input);
    }

    function createRow() {
        idcount+=1;
       
        var product = document.getElementById("product").value;
        var price = document.getElementById("price").value;
        var count = document.getElementById("count").value;

        var sumCount = price * count;

        var row = document.createElement('tr'); // create row node
        var col = document.createElement('td'); // create column node
        var col2 = document.createElement('td'); // create second column node
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var col5 = document.createElement('td');
        row.appendChild(col); // append first column to row
        row.appendChild(col2); // append second column to row
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(col5);
        col.innerHTML = idcount; // put data in first column
        col2.innerHTML = product;
        col3.innerHTML = price;
        col4.innerHTML = count;
        col5.innerHTML = sumCount;// put data in second column
        var table = document.getElementById("tableToModify"); // find table to append to
        table.appendChild(row); // append row to table
        
         var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "testcreatename");
        input.setAttribute("value", [idcount,product,price,count,sumCount,0,"#"]);
        table.appendChild(input);
    }
    
     function calproduct() {
        //pirce
        var i = document.getElementById("seproduct").selectedIndex;
        
        var productList = new Array();
            <c:forEach items="${productgetlist}" var="ppl">
                 var product = new Object();
                product.pid = '${ppl.product_id}';
                productList.push(product);
            </c:forEach>
                
        return productList[i - 1].pid;
    }
      function eiei() {
           var t = document.getElementById("secustomer").value;
         
            $('#contactid').val(t);
        

    }
    function duedateSet() {
           var d = document.getElementById("duedate").value;
         
            $('#duedateIN').val(d);
            
        

    }
    
  
</script>

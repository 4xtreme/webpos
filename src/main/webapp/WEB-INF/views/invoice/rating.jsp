<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->
<div class="content-wrapper">
<div class="container" id="mainpage" style="width: 80%">
            <div class="jumbotron">
            <div class="row">
                <div class="col col-md-3">
                    <label>รวม</label>
                    <img src="${pageContext.request.contextPath}/img/feel2.png" style="">
                </div>
                <div class="col col-md-3">
                    <label>ยอดเยี่ยม</label>
                    <img src="${pageContext.request.contextPath}/img/good.png" style="">
                    
                </div>
                <div class="col col-md-3">
                    <label>ปานกลาง</label>
                    <img src="${pageContext.request.contextPath}/img/med.png" style="">
                    
                </div>
                <div class="col col-md-3"> 
                    <label>ต่ำ</label>
                    <img src="${pageContext.request.contextPath}/img/bad.png" style="">
                   
                </div>
            </div>
        </div>
        </div>
        <div class="container" id="mainpage" style="width: 80%">
            <div class="jumbotron">
                <p>ผลตอบรับ</p>
               
                <table  class="table table-striped table-bordered table-hover table-condensed " >
                    <thead>
                        <tr>
                             <th>
                                   <div align="center"><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                             </th>
                            <th>
                                <label>DateTime</label>
                            </th>
                            <th>
                                <label >เลขที่ใบเสร็จรับเงิน</label>
                            </th>
                            <th>
                                <label >ชื่อลูกค้า</label>
                            </th>
                            <th>
                                <label >รายชื่อผู้ติดต่อ</label>
                            </th>
                            <th>
                                <label >ผลตอบรับ</label>
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                   
                    </tbody>
                </table>
                 <script language="JavaScript">
                    $("#chk_all").click(function(){
                        $('input:checkbox').not(this).prop('checked', this.checked)
                    });
                </script>
               
                
            </div>

        </div>
</div>
<jsp:include page="../footer_1.jsp" />


<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title> Login </title>
   <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
   <link href="${pageContext.request.contextPath}/css/regis-form.css" rel="stylesheet" />
</head>
 <body>
    <div class="container">
       
        <div class="card card-container">
          
            <div class="row">
                <div class="col-lg-12">
                    <center><h1 class="page-header">MyCRM </h1></center>
                </div>
            </div>
			
   
            <form class="form-signup" action="login" method="post">
                <span id="reauth-email" class="reauth-email"></span>
                <input id="inputEmail" name="email" class="form-control" placeholder="Email address" required="" autofocus="" type="email">
                <input id="inputPassword" name="password" class="form-control" placeholder="Password" required="" type="password">
                <button class="btn btn-lg btn-primary btn-block btn-signup" type="submit">Login</button>
            </form><!-- /form -->

        </div><!-- /card-container -->
    </div><!-- /container -->
   <!-- jQuery -->
<script src="js/jquery.min.js"></script>  

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>
</html>

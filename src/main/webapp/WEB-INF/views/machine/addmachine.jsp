<%-- 
    Document   : addmachine
    Created on : Nov 21, 2017, 1:23:44 PM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Machine</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <form:form action="addmachine" method="POST" modelAttribute="machine" enctype="multipart/form-data"  >
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-8" >
                                        <div class="dl-horizontal">

                                            <div class="form-group">
                                                <span class="label_title">Machine name:</span>
                                                <form:input type="text"  path="machine_name" /><form:errors path="machine_name"/>
                                            </div>

                                            <div class="form-group">
                                                <span class="label_title">Car Registration :</span>
                                                <form:input type="text" path="car_regis_num" /><form:errors path="car_regis_num"/>
                                            </div>

                                            <div class="form-group">
                                                <span>Brand:</span>
                                                <form:input type="text" path="brand"  /><form:errors path="brand"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>Model:</span>
                                                <form:input type="text" path="mechine_model" /><form:errors path="mechine_model"/>
                                            </div>

                                            <div class="form-group">
                                                <span>Engine Size:</span>
                                                <form:input type="text" path="engine_size" /><form:errors path="engine_size"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <form:select path="size_type">
                                                    <option>Liter</option>
                                                </form:select><form:errors path="size_type"/>
                                                <button class="btn btn-circle-plus">+</button>
                                            </div>

                                            <div class="form-group">
                                                <span>Machine number:</span>
                                                <form:input type="text" path="machine_num" /><form:errors path="machine_num"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>Body number:</span> 
                                                <form:input type="text" path="body_num" /><form:errors path="body_num"/>
                                            </div>

                                            <div class="form-group">
                                                <span>Type:</span>
                                                <form:select path="type">
                                                    <option >Car</option>
                                                </form:select><form:errors path="type"/>
                                                <button class="btn btn-circle-plus">+</button>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>Category/Group:</span>
                                                <form:select path="category">
                                                    <option>Heavy Engine</option>
                                                </form:select><form:errors path="category"/>
                                                <button class="btn btn-circle-plus">+</button>
                                            </div>

                                            <div class="form-group">
                                                <div class="row margin_top10"> 
                                                    <div class="col-xs-3 " style="margin-left: 15px">
                                                        <label class="fileContainer">
                                                            <i class="fa fa-link fa-flip-horizontal attach-file" aria-hidden="true"></i>
                                                            <input type="file" id="uploadBtn" path="machine_file"  name="machinefile" />
                                                        </label>
                                                        <span style="font-size: 20px; color: skyblue; line-height: 60px">Attach File</span>
                                                    </div>

                                                    <div class="col-xs-6">
                                                        <div class="file_upload_box">
                                                            <div class="col-xs-6">
                                                                <input id="uploadFile" class="form-control-sm no-border width_full file_dir" type="text" value="No file selected."  disabled="disabled"  /><form:errors path="machine_file" /><br>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>		
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="dl-horizontal">
                                            <center>
                                                <div class="imageupload picture_frame" id="imageuploaded" >
                                                    <div class="file-tab panel-body">
                                                        <label class="btn btn-default btn-file">
                                                            <!-- The file is stored here. -->
                                                            <span>Browse</span>
                                                            <input type="file" name="machinepicture" path="machine_pic" />
                                                        </label>
                                                        <button type="button" class="btn btn-default">Remove</button>
                                                    </div>
                                                    <div class="url-tab panel-body">
                                                        <div class="input-group">
                                                            <div class="input-group-btn">
                                                                <button type="button" class="btn btn-default">Submit</button>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-default">Remove</button>
                                                        <!-- The URL is stored here. -->
                                                        <input type="hidden" name="machinepicture">
                                                    </div>  
                                                </div>   
                                            </center>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                </br>
                                </br>
                                <div class="row">
                                    <div class="col-lg-8" style="border-top: solid; border-width: 1px; ">
                                        </br></br>
                                        <div class="dl-horizontal">
                                            <div class="form-group">
                                                <span>Start date: </span>
                                                <i class="fa fa-calendar " style="font-size: 20px"></i>
                                                <form:input  type="text" class="datetime"  path="start_date" />  <form:errors path="start_date" />               
                                                <span>Machine life 2 year 3 month 20 day</span>
                                            </div>
                                            <div class="form-group">
                                                <span>Buy from:</span>
                                                <form:select path="buy_form">
                                                    <option>
                                                        xxxxxxxxxxxxx
                                                    </option>
                                                </form:select><form:errors path="buy_form" />   

                                                <button class="btn btn-circle-plus">+</button>
                                            </div>                 
                                            <div class="form-group">
                                                <span>Driver: </span>
                                                <form:select path="driver">
                                                    <option>
                                                        xxxxxxxxxxxxx
                                                    </option>
                                                </form:select><form:errors path="driver" />    
                                                <button class="btn btn-circle-plus">+</button>
                                            </div>
                                            <div class="form-group">
                                                <span>Machine status: </span>
                                                <form:select path="machine_status">
                                                    <option>Avaliable</option>
                                                </form:select> <form:errors path="machine_status" />                                                   
                                                <button  class="btn btn-circle-plus">+</button>
                                            </div>
                                        </div>
                                    </div>
                                    <right>
                                        <div class="col-lg-2 margin_left10 ">
                                            <div class="dl-horizontal">
                                                <label>Machine Status :  </label>
                                                <span>Avaliable</span></br>
                                                <label>Machine Number : </label>
                                                <span>xxxxxxxxxxxx</span></br>
                                                <label>Last update date : </label>
                                                <span>xxxxxxxxxxxx</span>
                                            </div>
                                        </div>
                                    </right>
                                </div>
                                </br>
                                </br>
                                </br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <center>
                                            <button class="btn  button-size-cancel">Cancel</button>
                                            <button class="btn  button-size-save"style="margin-left: 20px;margin-right: 20px">Save</button>
                                            <button class="btn  button-size-sucess" type="submit" >Submit</button>
                                        </center>
                                    </div>
                                </div>
                            </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/img/bootstrap-imageupload.js"></script>
<script>
    var $imageupload = $('.imageupload');
    $imageupload.imageupload();

</script>
<script>
    document.getElementById("uploadBtn").onchange = function () {
        var split = this.value;
        split = split.substr(12, );
        document.getElementById("uploadFile").value = split;
    };

</script>
<script>
    $(document).ready(function () {
        $("#sub_mainmenu").show();
        $("#side-menu").hide();
        $("#back-btn").click(function () {
            $("#side-menu").show(500);
            $("#sub_mainmenu").hide(1000);
            $("#bottom-nav").hide(250);
            $("#bottom-nav").show(100);

        });
    });
</script>
<jsp:include page="../footer.jsp" />

<%-- 
    Document   : header
    Created on : Jan 11, 2017, 1:38:57 PM
    Author     : chatcharin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="${pageContext.request.contextPath}/dashboard">Maintenance Expense</a>
    </div>

    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"> </span>
        <span class="icon-bar"> </span>
        <span class="icon-bar"> </span>
    </button>

    <!-- Top Navigation: Left Menu -->
    <ul class="nav navbar-nav navbar-left navbar-top-links" >
        <!-- <li><a href="#"><i class="fa fa-home fa-fw"></i> Home</a></li>-->

    </ul>

    <!-- Top Navigation: Right Menu -->
    <ul class="nav navbar-right navbar-top-links">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> myacc <b class="caret"></b>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="${pageContext.request.contextPath}/setting/profile"><i class="fa fa-user fa-fw"></i> userprofile</a>
                </li>
                <li><a href="${pageContext.request.contextPath}/setting"><i class="fa fa-gear fa-fw"></i> setting</a>
                </li>
                <li class="divider"></li>
                <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> logout</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-globe fa-fw"></i> Th <b class="caret"></b>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="?locale=th"><img src="img/Thailand_flat16.png"> Th</a>


                <li><a href="?locale=en"><img src="img/United-Kingdom_flat16.png"> EN</a>
                </li>

            </ul>
        </li>
    </ul>
    <!-- Sidebar -->

    <div class="navbar-default sidebar "  style="height: 768px;"  role="navigation" >
        <div id="open-btn" class="close-btn"><i class="fa fa-angle-double-right" style="font-size: 30px" aria-hidden="true"></i></div>
        <div id="close-btn" class="close-btn"><i class="fa fa-times"style="font-size: 30px" aria-hidden="true"></i></div>
        </br>
        </br>
        </br>
        <div id="navbar" style="border-bottom:solid 1px; height: 500px ">
            <div class="sidebar-nav navbar-collapse"  id="side-menu" >
                <ul class="nav" > 
                    <li>
                        <a href="${pageContext.request.contextPath}/addwithdraw"">เบิกค่าซ่อมบำรุง</a> 
                    </li>
                    <li>
                        <a href="#" id="mainmenu"> Main Menu </a>
                    </li>
                    <li>
                        <a href="#" > Contact </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/contactAll">All Contacts</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/contactCreate">Create Contact</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" > Products </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/product/productList">Product List</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/product/productCreate">Create </a>
                            </li>
                        </ul>
                    </li> 
                    <li>
                        <a href="#" > Quotations </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/quotation/quotationList">Quotation List</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/quotation/quotationCreate">Create Quotations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" > Billing </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/billinglist">Billing List</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/billingcreate">Create Billing</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" > Invoice </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/invoicelist">Invoice List</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/invoicecreate">Create Invoice</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/setting"> Setting </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${pageContext.request.contextPath}/setting/profile">profile</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/setting/editprofile">Edit profile</a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/setting/emailsenderedit">Email Sender</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="sidebar-nav navbar-collapse"id="sub_mainmenu" style="display: none" >
                <div id="back-btn" class="close-btn"><label><i class="fa fa-angle-double-left" style="font-size: 30px" aria-hidden="true"></i><span style="font-size: 20px" >  Back</span></label></div>
                </br>
                </br>
                </br>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="${pageContext.request.contextPath}/promotion">Promotion Create</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/productlist">Promotion List </a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/addmachine">Add machine</a>
                    </li>
                </ul>
            </div>

        </div>
        <div id="bottom-nav">
            <div class="sidebar-nav navbar-collapse" style="border-bottom: solid 1px">
                </br>
                
                <ul class="nav">
                    <li>
                        <a href="#">Admin</a>
                    </li>
                    <li>
                        <a href="#">Setting </a>
                    </li>

                </ul>
                
                </br>
            </div>
            <div class="sidebar-nav navbar-collapse">
                 </br>
                <ul class="nav" >
                    <li>
                        <a href="#">Logout </a>
                    </li>

                </ul>
            </div>     
        </div>
    </div>
</nav>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $("#close-btn").show();
        $("#open-btn").hide();
        $("#mainmenu").click(function () {
            $("#side-menu").hide(500);
            $("#sub_mainmenu").show(1000);
        });
        $("#close-btn").click(function () {
            $("#navbar").hide(1000);
            $("#open-btn").show(500);
            $("#close-btn").hide(1000);
            $("#bottom-nav").hide(1000);
        });
        $("#open-btn").click(function () {
            $("#navbar").show(500);
            $("#open-btn").hide(1000);
            $("#close-btn").show(500);
            $("#bottom-nav").show(500);

        });

    });


</script>
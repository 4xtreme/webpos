<%-- 
    Document   : header
    Created on : Jan 11, 2017, 1:38:57 PM
    Author     : chatcharin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="#"> MENU</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        
        <span class="navbar-toggler-icon"></span>
    </button>
    
    
    
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav  " id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="login">
          <a class="nav-link" href="${pageContext.request.contextPath}/jsp/login">
            <i class="fa fa-fw fa-sign-out"></i>
            <span class="nav-link-text"><h8>login</h8></span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="รายงาน">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExample" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text"><h8>รายงาน</h8></span>
          </a>
            <ul class="sidenav-second-level collapse" id="collapseExample">
            <li>
                <a href="${pageContext.request.contextPath}/contact/showcontact"><h7>สรุปยอดขาย</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/contact/product_sales"><h7>ยอดขายตามสินค้า</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/contact/salesby_category"><h7>ยอดขาย แยกตาม หมวดหมู่</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/contact/employeereport"><h7>ยอดขาย แยกตาม พนักงาน</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/contact/salesby_payment"><h7>ยอดขาย แยกตาม ประเภทการชำระเงิน</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/contact/receipt"><h7>ใบเสร็จรับเงิน</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/contact/salesby_additional"><h7>ยอดขาย แยกตาม ตัวเลือกเพิ่มเติม</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/contact/discount"><h7>ส่วนลด</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/contact/tax"><h7>ภาษี</h7></a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="รายการสินค้า">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExampleP" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-shopping-basket"></i>
            <span class="nav-link-text"><h8>รายการสินค้า</h8></span>
          </a>
            <ul class="sidenav-second-level collapse" id="collapseExampleP">
            <li>
                <a href="${pageContext.request.contextPath}/product/dashboardnew"><h7>รายการสินค้า</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/product/categories"><h7>หมวดหมู่</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/product/modifiers"><h7>ตัวเลือกเพิ่มเติม</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/product/discountlist"><h7>ส่วนลด</h7></a>
            </li>
          </ul>
        </li>
        
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inventory">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#Inventory" data-parent="#exampleAccordion">
            <i class="fa fa-shopping-cart"></i>
            <span class="nav-link-text"><h8>Inventory Management</h8></span>
          </a>
            <ul class="sidenav-second-level collapse" id="Inventory">
            <li>
              <a href="${pageContext.request.contextPath}/withdraw/purchase"><h7>คำสั่งซื้อ</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/withdraw/transfer"><h7>Transfer orders</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/withdraw/adjustment"><h7>Stock adjustments</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/withdraw/counts"><h7>Inventory counts</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/withdraw/productionslist"><h7>Productions</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/withdraw/supplierlist"><h7>ผู้จัดจำหน่าย</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/withdraw/history"><h7>Inventory history</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/withdraw/valuation"><h7>Inventory valuation</h7></a>
            </li>
          </ul>
        </li>
        
        
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="พนักงาน">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text"><h8>พนักงาน</h8></span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="${pageContext.request.contextPath}/quotation/employee"><h7>รายชื่อพนักงาน</h7></a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/quotation/employeeStatus"><h7>สิทธิการเข้าถึง</h7></a>
            </li>
            
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="ลูกค้า">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-users"></i>
            <span class="nav-link-text"><h8>ลูกค้า</h8></span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
                <a href="${pageContext.request.contextPath}/billing/customerbase"><h7>ฐานลูกค้า</h7></a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/billing/notificationsystem"><h7>ระบบแจ้งเตือน</h7></a>
            </li>
            
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="ผลตอบรับ">
          <a class="nav-link" href="${pageContext.request.contextPath}/invoice/rating">
            <i class="fa fa-fw fa-star"></i>
            <span class="nav-link-text"><h8>ผลตอบรับ</h8></span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="ตั้งค่า">
          <a class="nav-link" href="${pageContext.request.contextPath}/setting/profilex ">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text"><h8>ตั้งค่า</h8></span>
          </a>
          
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto"> 
          
      </ul>
    </div>
  </nav>
     
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
 <!-- Page Content -->
   <div class="content-wrapper">
       <div class="content-wrapper">
              <div class="jumbotron" style="width:75%;height: auto"> 
                  <a href="${pageContext.request.contextPath}/product/categorycreate">
                      <button type="submit" class="btn btn-default" style = "background-color:#9ACD32;color:white">
                          <i class="fa fa-plus"></i> เพิ่มหมวดหมู่ 
                      </button>
                  </a><br><br>
                    <div class="text-center"> <i class="fa fa-exclamation-triangle fa-5x" style="color:#FF3399"></i></div><br>   
              </div>
       </div>
   </div>


 <jsp:include page="../footer_1.jsp" />
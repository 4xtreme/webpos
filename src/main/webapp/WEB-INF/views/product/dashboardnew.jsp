<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<link href="${pageContext.request.contextPath}/css/categorie.css" rel="stylesheet">
 <!-- Page Content -->
  <div class="content-wrapper">

            
      <div class="container">
            <div class="jumbotron" style="width:90%;height: auto">
                 <input class="k7" type="text"  placeholder="Search..">
                    <div class="k1"><a href="${pageContext.request.contextPath}/product/goods">
                            <button type="button" class="btn" style="color:white; background-color:#9ACD32">
                                <i class="fa fa-plus"></i> เพิ่มสินค้า
                            </button></a></div>
                    <div class="k2"><a href="#"><button type="button" class="btn btn-default">นำเข้า</button></a></div>
                    <div class="k3"><a href="#"><button type="button" class="btn btn-default">ส่งออก</button></a></div>
                        <div class="k4">ร้านค้า
                              <select >
                                <option value="a">ร้านค้าทั้งหมด</option>
                              </select>
                        </div>
                        <div class="k5">ประเภท
                              <select >
                                <option value="a">ไม่มีหมวดหมู่</option>
                                <option value="b">รายการทั้หมด</option>
                              </select>
                        </div>
                         <div class=" k6">แจ้งเตือนสต็อก

                              <select>
                                <option value="c">รายการทั้งหมด</option>
                                <option value="d">ปริมาณสต็อกเหลือน้อย</option>
                                <option value="e">หมดสต็อก</option>                  
                              </select>
                         </div>
                                
                <div class="container">
                     <div class="table-responsive">
                <table  class="table table-hover" widht="100%" height="100%" align="center">
                    
                    <thead>
                        <tr>      
                        <th >
                            <div align="center"> <input name="CheckAll" type="checkbox" id="chk_all"></div>
                        </th> <br>
                            <th><div align="center">ชื่อสินค้า</div></th>
                            <th><div align="center">ประเภท</div></th>
                            <th><div align="center">ราคาขาย</div></th>
                            <th><div align="center">ต้นทุน</div></th>
                            <th><div align="center">ผลต่าง</div></th>
                            <th><div align="center">อยู่ในคลัง</div></th>
                        </tr>
                    </thead>
                   
                </table> 
                
                 
   
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="dashboard.php?page=1" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item">
                            </li>
                            <li class="page-item">
                            <a class="page-link" href="dashboard.php?page=<?php echo $total_page;?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            </a>
                            </li>
                        </ul>
                    </nav>

                        </div>
            </div> 
        </div>
  </div>
      
  </div>
 
 <jsp:include page="../footer_1.jsp" />
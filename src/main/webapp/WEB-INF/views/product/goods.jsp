<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<link href="${pageContext.request.contextPath}/css/categorycreate.css" rel="stylesheet">
 <!-- Page Content -->
 
 
  <style>
    .wrapper{
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	width: 100px;
	margin: 50vh auto 0;
	-ms-flex-wrap: wrap;
	    flex-wrap: wrap;
	-webkit-transform: translateY(-50%);
	        transform: translateY(-50%);
}

.switch_box{
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	max-width: 60px;
	min-width: 60px;
	-webkit-box-pack: center;
	    -ms-flex-pack: center;
	        justify-content: center;
	-webkit-box-align: center;
	    -ms-flex-align: center;
	        align-items: center;
	-webkit-box-flex: 1;
	    -ms-flex: 1;
	        flex: 1;
}

/* Switch 1 Specific Styles Start */

input[type="checkbox"].switch_1{
	font-size: 15px;
	-webkit-appearance: none;
	   -moz-appearance: none;
	        appearance: none;
	width: 3.5em;
	height: 1.5em;
	background: #ddd;
	border-radius: 3em;
	position: relative;
	cursor: pointer;
	outline: none;
	-webkit-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
  }
  
  input[type="checkbox"].switch_1:checked{
	background: #0ebeff;
  }
  
  input[type="checkbox"].switch_1:after{
	position: absolute;
	content: "";
	width: 1.5em;
	height: 1.5em;
	border-radius: 50%;
	background: #fff;
	-webkit-box-shadow: 0 0 .25em rgba(0,0,0,.3);
	        box-shadow: 0 0 .25em rgba(0,0,0,.3);
	-webkit-transform: scale(.7);
	        transform: scale(.7);
	left: 0;
	-webkit-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
  }
  
  input[type="checkbox"].switch_1:checked:after{
	left: calc(100% - 1.5em);
  }
	
/* Switch 1 Specific Style End */

#div6 {
    float: right;
    
}
</style>
<script type="text/javascript">
	function ck_frm(){
		var ck = document.getElementById('ckk');
		if(ck.checked == true){
		document.getElementById('frm_txt').style.display = "";
		}else{
		document.getElementById('frm_txt').style.display = "none";
		}

	}
</script>
<script type="text/javascript">
	function ckk_frm(){
		var ckk = document.getElementById('ckkk');
		if(ckk.checked == true){
		document.getElementById('frmm_txt').style.display = "";
		}else{
		document.getElementById('frmm_txt').style.display = "none";
		}

	}
</script>
<script type="text/javascript">
	function mm_frm(){
		var mm = document.getElementById('mm');
		if(mm.checked == true){
		document.getElementById('mm_txt').style.display = "";
		}else{
		document.getElementById('mm_txt').style.display = "none";
		}

	}
</script>
 
 
 
 
 
     <div class="content-wrapper" style="background-color:#F5F5F5;">
       
      <div  style="padding: 15px;">
        <div  style="background-color:white; height:auto; width:60%; padding: 15px;">
            <div class="w3-half"> 
                <label>คำนำหน้าชื่อ</label>     
                <input type="text" style="width:100%;"> 
            </div>
            ประเภท <br><select style="width:200px;"><option value="c" > ไม่มีหมวดหมู่ </option></select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <input name="checkbox[]" type="checkbox" id="checkbox" value="limit"/>  มีรายการสินค้าพร้อมจำหน่าย <br><br>
            ขายโดย: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name= "Gender" type="radio" id="radio2" value="male" checked="checked"/>  แต่ละ  &nbsp;&nbsp;&nbsp;  
            <input name= "Gender" type="radio" id="radio2" value="female" />  น้ำหนัก <br><br>
          
                <label>ราคาขาย</label>     
                <input type="text" style="width:100%;" placeholder="เว้นฟิลด์นี้ว่างเพื่อแสดงราคาเมื่อขาย"> <br>
                 <label>ต้นทุน</label>     
                 <input type="text" style="width:100%;" placeholder="  0.00"> <br>
                 <label>SKU</label>     
                 <input type="text" style="width:100%;" placeholder="10002"> <br>
                 <label>บาร์โค๊ด</label>     
                 <input type="text" style="width:100%;"> 
                 <br><br><br><br><br>
        </div>
   
            
            <br> 
             <div style="background-color:white; width: 60%; padding: 15px;">
            
            <h4>คลังสินค้า</h4>
		<div >
            <div class="aa">
                
                    <h7>รายการคอมโพสิต</h7>
               
                <div id="div6">
                    <input type="checkbox" class="switch_1" name="ckk" id="ckk" onclick="ck_frm();">
                </div>
            </div>
            
             
            <div id="frm_txt" style="display:none;" >
                  <form name="frmAdmin" id="frmAdmin" action="#" method="post" width="100%">
                      <div id="div6">                    
                          Use production
                           <input type="checkbox" class="switch_1" name="ckkk"  >
                     </div>
                      <table width="100%">
                          <thead>
                              <tr>
                                  <th align="left"><div>Component</div></th>
                                  <th align="right"><div>จำนวน</div></th>
                                  <th align="right"><div>ต้นทุน</div></th>
                              </tr>
                          </thead>
                      </table>
                      <input type="text" name="txtnum" value="" style="width:100%;" placeholder="ค้นหาสินค้า"/><br/>
                  </form>
              </div>
            </div>  
            <br><br>
                 <div >
            <div>
                    <h7>ติดตามสินค้าคงคลัง</h7>
                <div id="div6">
                    <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ckk_frm();">
                </div>
            </div>
     <div id="frmm_txt" style="display:none;" >
           <form name="frmmAdmin" action="#" method="post" width="100%">
               <table width="100%">
                   <thead>
                       <tr>
                           <th align="left"><div>Primary supplier</div></th>
                           <th align="right"><div>Default purchase cost</div></th>
                           
                       </tr>
                   </thead>
               </table>
               <input type="text" value="" style="width:100%;"/><br/>
           </form>
       </div>       <br><br>
    </div>
        </div>
            
   
            
            
            <br>
        <div style="background-color:white; width: 60%; padding: 15px;">
            <h4>ตัวแปร</h4>
            <div style="color: #D3D3D3"><p>Available only with Loyverse POS for iOS v. 1.35 or newer</p></div>
                <p>Use variants if an item has different sizes, colors or other options</p>
                ชื่อตัวเลือก: <input > &nbsp;&nbsp;&nbsp;
                <input placeholder="Please to add the value"><br><br>
                 ชื่อตัวเลือก: <input > &nbsp;&nbsp;&nbsp;
                <input placeholder="Please to add the value"> <br><br>
                <a href="#"><button type="submit" name="btn1" class="btn btn-default"> ยกเลิก </button></a>
                <a href="#"><button type="submit" name="btn1" class="btn btn-default"> บันทึก </button></a>
           <br><br>
        </div><br>
        
        
        
            <div style="background-color:white; width: 60%; padding: 15px;">
                <h4>ร้านค้า</h4>
                  <input name="checkbox[]" type="checkbox" id="checkbox" value="limit"/> มีรายการสินค้าพร้อมจำหน่าย <br><br>
                       
                <div class="container">
                     <div class="table-responsive">
                <table  class="table table-hover" widht="100%" height="100%" align="center">
                    <thead>
                        <tr>      
                        <th >
                            <div align="center"> <input name="CheckAll" type="checkbox" id="chk_all"></div>
                        </th> 
                            <th><div align="center">พร้อมใช้งาน</div></th>
                            <th><div align="center">ร้านค้า</div></th>
                            <th><div align="center">ราคาขาย</div></th>
                            <th><div align="center">อยู่ในคลัง</div></th>
                            <th><div align="center">ปริมาณสต็อกเหลือน้อย</div></th>
                            <th><div align="center">Optimal stock</div></th>
                        </tr>
                    </thead>
                </table> 
               

                        </div>
                    </div> 
                </div>
        
            <br>
            
            
        <div  style="background-color:white; width: 60%; padding: 15px;"> 
            <h4>แสดงบน PDS </h4>   
            <label class="container b-1">
              <input type="radio" checked="checked" name="radio">
              <span class="checkmark"></span>
            </label>
            <label class="container1 b-2">
              <input type="radio" name="radio">
              <span class="checkmark1"></span>
            </label>
            <label class="container2 b-3">
              <input type="radio" name="radio">
              <span class="checkmark2 "></span>
            </label>
            <label class="container3  b-4">
              <input type="radio" name="radio">
              <span class="checkmark3"></span>
            </label>
            <label class="container4 b-5">
              <input type="radio" name="radio">
              <span class="checkmark4"></span>
            </label>
            <label class="container5  b-6">
              <input type="radio" name="radio">
              <span class="checkmark5"></span>
            </label>
           <label class="container6  b-7">
              <input type="radio" name="radio">
              <span class="checkmark6"></span>
            </label>
            <label class="container7  b-8">
              <input type="radio" name="radio">
              <span class="checkmark7"></span>
            </label>
               <a href="#"><button type="submit" name="btn1" class="btn btn-default"> ยกเลิก </button></a>
               <a href="#"><button type="submit" name="btn1" class="btn btn-default"> บันทึก </button></a>
    </div>
        </div>

</div>
     
     
     
  <jsp:include page="../footer_1.jsp" />
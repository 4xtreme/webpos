<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header text-center">Promotion List</h1>
            </div>
        </div>
               <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                                    	<table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Promotion ID</th>
                                                <th class="text-center">Customer Type	</th>
                                                
                                                <th class="text-center">Topic</th>
                                                <th class="text-center">Create Date</th>
                                                <th class="text-center">View Detail</th>
                                              
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <c:forEach var="promotionlist" items="${promotionlist}" varStatus="loop">
                                    <tr>
                                                <td class="text-center">${promotionlist.promotion_id}</td>
                                                <td class="text-center">${promotionlist.customer_type}</td>
                                                
                                                <td class="text-center">${promotionlist.topic}</td>
                                                <td class="text-center">${promotionlist.create_date}</td>
                                                <td class="text-center">
                                                    <form  action="selecteditpromotion" method="post" >
                                                 <button type="submit" name="editid" value="${promotionlist.promotion_id}" class="btn btn-success btn-circle " ><i class="fa fa-edit"></i></button>
                                                </form>
                                                    </td>
                                               
                                            </tr>  
                                     
                                            </c:forEach>
                                
                              
                                        </tbody>
                                                                           
                                    </table>
                </div>
            </div>
        </div>
                 </div>
</div>
 <jsp:include page="../footer.jsp" />

    
    
  

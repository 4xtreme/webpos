<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->
<div class="content-wrapper">
<div class="container" id="mainpage" style="width: 80%">
            <div class="jumbotron" >
                <a href="${pageContext.request.contextPath}/quotation/employeeAdd"><button type="button" class="btn btn-success">+ Add employee</button></a><br><br>
               
                    <table  class="table table-striped table-bordered table-hover table-condensed " >
                        <thead>
                            <tr>
                                <th>
                                   <div align="center"><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                                </th>
                                <th>
                                    <label>ID</label>

                                </th>
                                <th>
                                    <label >Name</label>
                                </th>
                                <th>
                                    <label >Email</label>
                                </th>
                                <th>
                                    <label >Tel number</label>
                                </th>
                                <th>
                                    <label >Status</label>
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>
                <script language="JavaScript">
                    $("#chk_all").click(function(){
                        $('input:checkbox').not(this).prop('checked', this.checked)
                    });
                </script>
               
            </div>

        </div>
</div>
<jsp:include page="../footer_1.jsp" />


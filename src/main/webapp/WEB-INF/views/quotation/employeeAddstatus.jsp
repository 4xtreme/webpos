<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->
<style>
.wrapper{
display: -webkit-box;
display: -ms-flexbox;
display: flex;
width: 100px;
margin: 50vh auto 0;
-ms-flex-wrap: wrap;
flex-wrap: wrap;
-webkit-transform: translateY(-50%);
transform: translateY(-50%);
}

.switch_box{
display: -webkit-box;
display: -ms-flexbox;
display: flex;
max-width: 60px;
min-width: 60px;
-webkit-box-pack: center;
-ms-flex-pack: center;
justify-content: center;
-webkit-box-align: center;
-ms-flex-align: center;
align-items: center;
-webkit-box-flex: 1;
-ms-flex: 1;
flex: 1;
}



.box_1{
background: #E6E6FA;
}

input[type="checkbox"].switch_1{
font-size: 15px;
-webkit-appearance: none;
-moz-appearance: none;
appearance: none;
width: 3.5em;
height: 1.5em;
background: #0ebeff;
border-radius: 3em;
position: relative;
cursor: pointer;
outline: none;
-webkit-transition: all .2s ease-in-out;
transition: all .2s ease-in-out;
}

input[type="checkbox"].switch_1:checked{
background: #ddd;
}

input[type="checkbox"].switch_1:after{
position: absolute;
content: "";
width: 1.5em;
height: 1.5em;
border-radius: 50%;
background: #fff;
-webkit-box-shadow: 0 0 .25em rgba(0,0,0,.3);
box-shadow: 0 0 .25em rgba(0,0,0,.3);
-webkit-transform: scale(.7);
transform: scale(.7);
left: 0;
-webkit-transition: all .2s ease-in-out;
transition: all .2s ease-in-out;
}

input[type="checkbox"].switch_1:checked:after{
left: calc(100% - 1.5em);
}


</style>
<div class="content-wrapper">
<div class="container" id="mainpage" style="width: 40%">
            <div class="jumbotron">
                <div class="text-center">
                    <img src="${pageContext.request.contextPath}/img/user.png" style="width: 20%">
                </div>
                <br>
                <table style="width: 100%">
                    <p>สิทธิการเข้าถึง</p>
                   <hr>
                   <tr>
                       <td width="90%">
                           <p>POS</p>
                       </td>
                       <td> 
                           <div class="switch_box box_1" id="div6">

                               <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ckk_frm1();">
                               <script>
                                function ckk_frm1() {
                                    var x = document.getElementById("myDIV1");
                                    if (x.style.display === "none") {
                                        x.style.display = "block";
                                    } else {
                                        x.style.display = "none";
                                    }
                                }
                               </script>
                           </div>
                       </td>
                   </tr>
                </table>
                <div id="myDIV1">
                <table style="width: 100%">
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>ดูใบเสร็จทั้งหมด</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>ใช้ส่วนลดด้วยการจำกัดการเข้าถึง</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>ผลตอบแทนที่ได้จากการเข้าถึง</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>ผลตอบแทนที่ได้จากการซื้อ</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>จัดการตั๋วที่เปิดอยู่ทั้งหมด</p>
                        </td>
                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>ยกเลิกรายการทั้งหมดที่อยู่ในออเดอร์</p>
                        </td>
                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>View shift report</p>
                        </td>
                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>Open cash drawer without making a sale</p>
                        </td>
                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>แก้ไขสินค้า</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>การเข้าถึงการตอบรับของลูกค้า</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>เปลี่ยนการตั้งค่า: เชื่อมต่อเครื่องพิมพ์</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>เข้าถึงการสนับสนุนทางแชทสด</p>
                        </td>
                    </tr>
                </table>
                    </div>
                <hr>
                
                <table style="width: 100%">
                   <tr>
                       <td width="90%">
                           <p>ระบบหลังร้าน</p>
                       </td>
                       <td> 
                           <div class="switch_box box_1" id="div6">
                               <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ckk_frm2();">
                               <script>
                                    function ckk_frm2() {
                                        var x = document.getElementById("myDIV2");
                                        if (x.style.display === "none") {
                                            x.style.display = "block";
                                        } else {
                                            x.style.display = "none";
                                        }
                                    }
                               </script>
                           </div>
                       </td>
                   </tr>
                </table>
                <div id="myDIV2">
                <table style="width: 100%">
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>รายงาน</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>รายการสินค้า</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>พนักงาน</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>ลูกค้า</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>ผลตอบรับ</p>
                        </td>
                    </tr> 
                    
                </table>
                <table style="width: 100%">
                   <tr>
                       <td width="90%">
                           <p>การตั้งค่า</p>
                       </td>
                   </tr>
                </table>
                <table style="width: 100%">
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>แก้ไขข้อมูลส่วนตัว</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>จัดการบิลลิ่ง</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>ประเภทการชำระเงิน</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>เปลี่ยนการตั้งค่าของโปรแกรมสมาชิก</p>
                        </td>

                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>กำหนดภาษี</p>
                        </td>
                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>เปลี่ยนการตั้งค่าของร้านและ POS</p>
                        </td>
                    </tr> 
                    <tr>
                        <td width="20%"> 
                            <div align=""><label ><input name="CheckAll" type="checkbox" id="chk_all"></label></div>
                        </td>
                        <td>
                            <p>การสนับสนุน</p>
                        </td>
                    </tr> 
                </table>
                    </div>
                <hr>
                <table style="width: 100%">
                  
                    <tr>
                        <td>
                            <button type="submit">ยกเลิก</button>
                            <a href="#"><button type="submit">บันทึก</button></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
                    </div>
<jsp:include page="../footer_1.jsp" />


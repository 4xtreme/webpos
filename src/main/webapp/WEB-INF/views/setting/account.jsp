<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->


    <div class="content-wrapper">
    <div class="container">
        <div class="row">
        <div class="col-md-4">
        <div class="bloc-5">
        <div>
            <ul id="ul">
                    <li><a id="ac">
                            <div>
                            <h3>การตั้งค่า</h3>
                            <span>ตั้งค่าระบบ</span>
                            </div>
                        </a>
                    </li>
            </ul>
            
        </div>
                <div>
                  <ul id="ul">
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/profilex">ทั่วไป</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/account">Billing & subscriptions</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/paytypes">ประเภทการชำระเงิน</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/loyaltyx">ความจงรักภักดี</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/tax">ภาษี</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/tax_reciepts">ใบเสร็จรับเงิน</a></li>
                </ul>
                </div>
                <div>
                <ul id="ul">
                        <li><a id="ac">
                                <div>
                                <h3>ร้านค้า</h3>
                                <span>การตั้งค่า ร้านค้า และ POS</span>
                                </div>
                            </a>
                        </li>
                </ul>
                </div>
                <div>
                  <ul id="ul">
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/store">ร้านค้า</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/pos">อุปกรณ์ POS</a></li>
                  </ul>
                </div>
            </div>

        </div>
<!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
<!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
<!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
<!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->

        <div class="col-md-8">
            <div class="jumbotron" style="width: 100%">
                <div>
                    <h3>การสมัครสมาชิก</h3>
                </div>
                <br>
                <div class="row">
                    <div class="">
                        <i class="fa fa-credit-card-alt" style="font-size:27px"></i>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <h8>การจัดการพนักงาน</h8><br>
                        <h7>Manage access rights, track timecards </h7><br>
                            <h7>and sales by employee.</h7>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <h8>14 day free trial</h8>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <script language="JavaScript">
                            function chkConfirm()
                            {
                                    if(confirm('Please add an employee to subscribe.')==true)
                                    {
                                            alert('Please add an employee to subscribe.');
                                            window.location = '#';
                                    }
                                    else
                                    {
                                            alert('cancel.');
                                            window.location = 'setting_account.php';
                                    }
                            }
                        </script>
                            <button type="submit" class="button button7" name="btnConfirm" value="สมัครสมาชิกบริการ" OnClick="chkConfirm()">สมัครสมาชิกบริการ</button> 
                    </div>
                </div>
                <br>
                <br>
                    <div class="row">
                    <div class="">
                        <i class="fa fa-shopping-cart" style="font-size:27px"></i>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <h8>Advanced inventory</h8><br>
                        <h7>Create purchase orders,view inventory</h7><br>
                            <h7>valuation report and manage stock.</h7>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <h8>14 day free trial</h8>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <script language="JavaScript">
                            function chkkConfirm()
                            {
                                    if(confirm('Subscribe to advanced inventory....You can start a 14-day free trial without payment and credit card. After that, there is a subscription fee of $25 USD/month per store. You may also be charged tax if your billing country is in the EU, US or Canada. You can cancel at any time.')==true)
                                    {
                                            alert('Subscribe to advanced inventory....You can start a 14-day free trial without payment and credit card. After that, there is a subscription fee of $25 USD/month per store. You may also be charged tax if your billing country is in the EU, US or Canada. You can cancel at any time..');
                                            window.location = '#';
                                    }
                                    else
                                    {
                                            alert('cancel.');
                                            window.location = 'setting_account.php';
                                    }
                            }
                        </script>
                            <button type="submit" class="button button7" name="btnConfirm" value="สมัครสมาชิกบริการ" OnClick="chkkConfirm()">สมัครสมาชิกบริการ</button> 
                    </div>
                </div>
                <br>
                <br>
                <hr><p>
                   <div class="row">
                    <div class="">
                        <i class="fa fa-credit-card" style="font-size:27px"></i>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <h8>ไม่มีข้อมูลบัตรเครดิตอยู่ในแฟ้ม</h8><br>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="">
                        <script language="JavaScript">
                            function chmConfirm()
                            {
                                    if(confirm('คุณต้องการเพิ่มวิธีการชำระเงิน.')==true)
                                    {
                                            alert('คุณต้องการเพิ่มวิธีการชำระเงิน.');
                                            window.location = '${pageContext.request.contextPath}/setting/account_addpayment';
                                    }
                                    
                            }
                        </script>
                            <button type="submit" class="button button7" name="btnConfirm" value="เพิ่มวิธีการชำระเงิน" OnClick="chmConfirm()">เพิ่มวิธีการชำระเงิน</button> 
                    </div>
                </div>
                <br>
                    <div class="row">
                    <div class="">
                        <h8>แก้ไขชื่อธุรกิจของคุณ เพิ่มผู้ติดต่อเรียกเก็บเงินหรือข้อมูล</h8><br>
                        <h8>อื่น ๆ ที่คุณต้องการรวมในใบแจ้งหนี้</h8>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;
                    <div class="">
                        <script language="JavaScript">
                            function chmmConfirm()
                            {
                                    if(confirm('คุณต้องการแก้ไข.')==true)
                                    {
                                            alert('คุณต้องการแก้ไข.');
                                            window.location = '${pageContext.request.contextPath}/setting/account_editdetails';
                                    }
                                    
                            }
                        </script>
                            <button type="submit" class="button button7" name="btnConfirm" value="แก้ไขรายละเอียดการเรียกเก็บเงิน" OnClick="chmmConfirm()">แก้ไขรายละเอียดการเรียกเก็บเงิน</button> 
                    </div>
                </div>  
            
                
                
            </div>    
        </div>
             



       


<jsp:include page="../footer_1.jsp" />

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->


        <div class="content-wrapper">
    <div class="container">
            <div class="jumbotron" style="width: 60%">
                
                <div class="text-center">
                    <i class="fa fa-credit-card" style="font-size:48px;"></i>
                </div>
                <br>
                <br>
                <div class="row">
                <form action="#" method="POST" name="bt1" style="width: 90%">
                    
                    <div>
                        <i class="fa fa-credit-card" style="font-size:24px;"></i>
                        <h8>หมายเลขบัตร:</h8>
                        <input type="text" name="name" id="name" class="form-control"  style="width:90%;"><br>
                    </div>
                    
                    <br>
                    <br>
                     <div>
                        <i class="fa fa-calendar-o" style="font-size:24px"></i>
                        <h8>วันหมดอายุ:</h8>
                        <input type="text" name="name" id="name" class="form-control"  style="width:90%;" placeholder="ว/ด/ป"><br>
                    </div>
                   
                    <br>
                    <br>
                     <div>
                        <i class="fa fa-unlock-alt" style="font-size:24px"></i>
                        <h8>CVV:</h8>
                        <input type="text" name="name" id="name" class="form-control"  style="width:90%;"><br>
                    </div>
                    
                    <br>
                    <br>
                    <div>
                        <i class="fa fa-map-marker" style="font-size:24px"></i>
                        <h8>ประเทศผู้ถูกเรียกเก็บเงิน</h8>
                    </div>
                    
                    <div class="custom-select-lg" style="width:100%" >
                        <select class="form-control">
                            <option label="Deutsch" value="string:deu">Deutsch</option>
                            <option label="English" value="string:eng">English</option>
                            <option label="Español" value="string:spa">Español</option>
                            <option label="Français" value="string:fra">Français</option>
                            <option label="Italiano" value="string:ita">Italiano</option>
                            <option label="Polski" value="string:pol">Polski</option>
                            <option label="Portuguesa" value="string:por">Portuguesa</option>
                            <option label="Romana" value="string:ron">Romana</option>
                            <option label="Български" value="string:bul">Български</option>
                            <option label="Русский" value="string:rus">Русский</option>
                            <option label="日本語" value="string:jpn">日本語</option>
                            <option label="한국어" value="string:kor">한국어</option>
                            <option label="Tiếng Việt" value="string:vie">Tiếng Việt</option>
                            <option label="Eesti" value="string:est">Eesti</option>
                            <option label="اللغة العربية&lrm;" value="string:ara">اللغة العربية&lrm;</option>
                            <option label="ελληνικά" value="string:ell">ελληνικά</option>
                            <option label="漢語" value="string:zho">漢語</option>
                            <option label="Bahasa Indonesia" value="string:ind">Bahasa Indonesia</option>
                            <option label="ภาษาไทย" value="string:tha" selected="selected">ภาษาไทย</option>
                            <option label="Nederlands" value="string:nld">Nederlands</option>
                            <option label="Latviešu valoda" value="string:lav">Latviešu valoda</option>
                            <option label="Magyar" value="string:hun">Magyar</option>
                            <option label="Norsk bokmål" value="string:nor">Norsk bokmål</option>
                            <option label="Malay" value="string:msa">Malay</option>
                            <option label="हिन्दी" value="string:hin">हिन्दी</option>
                            <option label="ქართული" value="string:kat">ქართული</option>
                            <option label="Македонски" value="string:mkd">Македонски</option>
                            <option label="Türkçe" value="string:tur">Türkçe</option>
                            <option label="বাংলা" value="string:ben">বাংলা</option>
                            <option label="Suomi" value="string:fin">Suomi</option>
                        </select>
                    </div>
                    
                      <br>                       
                            <button  type="submit" name="btn1" class="button button4" onclick="JavaScript:fncSubmit('page1')" > บันทึก </button> 
                            <button  type="submit" name="btn2" class="button button4"  onclick="JavaScript:fncSubmit('page2')" > ยกเลิก </button>
                
                <br>
                    <br>
                
            </form>
            </div>
   <script language="javascript">
    function fncSubmit(strPage)
{
	if(strPage == "page1")
	{
		document.bt1.action="Customerbase.php?action=Customerbase.php";
	}
	
	if(strPage == "page2")
	{
		document.bt1.action="Customerbaseadd.php?action=Customerbaseadd.php";
	}	
	
	document.bt1.submit();
}
</script> 
<script>
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
</script>                

             



       


<jsp:include page="../footer_1.jsp" />

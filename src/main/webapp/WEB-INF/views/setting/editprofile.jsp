
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
 <div id="page-wrapper">
     <div class="container-fluid">
         <!-- start Content -->
         <div class="row">
             <div class="col-lg-12">
                 <h1 class="page-header">Setting</h1>
             </div>
         </div>

 <form:form action="editprofile" method="POST" modelAttribute="profile">
         <div class="row">
             <div class="col-lg-12">
                 
                 
                 <div class="row">
				<div class="col-lg-12">
				
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="Profile">
                                           
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <h4>Detail</h4>
                                                    <dl class="dl-horizontal">

                                                        <dt>First name:</dt> <dd><form:input class="form-control" path="firstname"  /> </dd> <br>
                                                        <dt>Last name:</dt> <dd><form:input class="form-control" path="lastname"  /> </dd>   <br> 
                                                        <dt>Company name:</dt> <dd> <form:input class="form-control" path="company_name"  /></dd>  <br> 
                                                        <dt>Phone1: </dt> <dd> <form:input class="form-control" path="phone1"  /></dd><br>
                                                        <dt>Phone2: </dt> <dd> <form:input class="form-control" path="phone2"  /></dd><br>
                                                        <dt>Email: </dt> <dd> <form:input class="form-control" path="email"  /></dd><br>
                                                        <dt>Facebook:</dt> <dd><form:input class="form-control" path="facebook"  /></dd><br>
                                                        <dt>Line:</dt> <dd> <form:input class="form-control" path="line" /></dd><br>

                                                    </dl>
                                                </div>
                                                <div class="col-lg-6">
                                                    <h4 >Address</h4>
                                                    <dl class="dl-horizontal">
                                                        <dt>Address: </dt> <dd> <form:input class="form-control" path="address" /></dd><br>
                                                        
                                                        <dt>District:  </dt> <dd> <form:input class="form-control" path="district" /></dd><br>
                                                        <dt>City: </dt> <dd> <form:input class="form-control" path="city" /></dd><br>
                                                        <dt>Country: </dt> <dd> <form:input class="form-control" path="country" /></dd><br>
                                                         <dt>Postalcode: </dt> <dd> <form:input class="form-control" path="postalcode" /></dd><br><br>
                                                    </dl>
                                                </div>
                                            </div>
                                             <div class="row">        
                                                    <div class="col-lg-6">
                                                        <h4 >Password</h4> 
                                                        <dl class="dl-horizontal">

                                                            <dt>Old password:</dt> <dd><input class="form-control" name="oldpass"  /> </dd> <br>
                                                            <dt>New password:</dt> <dd><input class="form-control" name="newpass"  /> </dd>   <br> 
                                                            <dt>Re-New password:</dt> <dd> <input class="form-control" name="renewpass"  /></dd>  <br> 


                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <dl class="text-right">
                                                            <br> <br> <br> <br> <br> <br> <br> 
                                                            ${message} 
                                                            <dt></dt> <dd><button type="submit" class="btn btn-primary"> <span class="fa fa-save "></span>  Save</button></dd> <br>
                                              
                                                        </dl>
                                                    </div>
                                                     
                                        </div>
                                            </div>
                                    </div>
                                                         

                                </div>
                        </div>


             </div>

         </div>
</form:form>
     </div>
 </div>


	<!-- End Page Content -->
 <jsp:include page="../footer.jsp" />

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
 <div id="page-wrapper">
     <div class="container-fluid">
         <!-- start Content -->
         <div class="row">
             <div class="col-lg-12">
                 <h1 class="page-header">Email Sender</h1>
                 
             </div>
         </div>


         <div class="row">
             <div class="col-lg-12">
                 
                 <form:form action="emailtopicedit" method="POST" modelAttribute="topic">
                     <div class="row">
                         <div class="col-lg-12">

                             <div class="tab-content">
                                 <div class="tab-pane fade active in" id="Profile">

                                     <div class="row">
                                         <div class="col-lg-6">
                                             <h4>Topic</h4>                                            
                                             <dl class="dl-horizontal">

                                                 
                                                 <dt>Topic:</dt> <dd ><form:input class="form-control" path="title"   /> </dd>   <br> 
                                                 <dt>Detail:</dt> <dd> <form:textarea class="form-control" path="detail" rows="7" cols="50"/></dd>   <br> 


                                             </dl>
                                         </div>

                                     </div>
                                     <div class="row">        

                                         <div class="col-lg-6">
                                             <dl class="text-right">

                                               
                                                 <dt></dt> <dd><button type="submit" class="btn btn-primary"> <span class="fa fa-save "></span>  Save</button></dd> <br>

                                             </dl>
                                         </div>

                                     </div>
                                 </div>


                             </div>


                         </div>
                     </div>
                 </form:form>

                 
                   <form:form action="emailsenderedit" method="POST" modelAttribute="profile">
                     <div class="row">
                         <div class="col-lg-12">

                             <div class="tab-content">
                                 <div class="tab-pane fade active in" id="Profile">

                                     <div class="row">
                                         <div class="col-lg-6">
                                             <h4>Email</h4>
                                            
                                             <dl class="dl-horizontal">

                                                 <dt>Email:</dt> <dd><form:input class="form-control" path="email" required="" autofocus="" type="email"/>  <label class="text-muted">*Please make sure you want to use your Email</label></dd> <br>
                                                 <dt>Password of email:</dt> <dd ><form:input class="form-control" path="password"  type="password" required=""/> </dd>   <br> 

                                                 <dt>Port:</dt> <dd> <form:input class="form-control" path="port" required="" /><label class="text-muted">Recommend port 587,465</label></dd>   <br> 


                                             </dl>
                                         </div>

                                     </div>
                                     <div class="row">        

                                         <div class="col-lg-6">
                                             <dl class="text-right">

                                                 ${message} 
                                                 <dt></dt> <dd><button type="submit" class="btn btn-primary"> <span class="fa fa-save "></span>  Save</button></dd> <br>

                                             </dl>
                                         </div>

                                     </div>
                                 </div>


                             </div>


                         </div>
                     </div>
                 </form:form>
                 
             </div>

         </div>

     </div>
 </div>


	<!-- End Page Content -->
 <jsp:include page="../footer.jsp" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
  <style>
    .wrapper{
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	width: 100px;
	margin: 50vh auto 0;
	-ms-flex-wrap: wrap;
	    flex-wrap: wrap;
	-webkit-transform: translateY(-50%);
	        transform: translateY(-50%);
}

.switch_box{
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	max-width: 60px;
	min-width: 60px;
	-webkit-box-pack: center;
	    -ms-flex-pack: center;
	        justify-content: center;
	-webkit-box-align: center;
	    -ms-flex-align: center;
	        align-items: center;
	-webkit-box-flex: 1;
	    -ms-flex: 1;
	        flex: 1;
}

/* Switch 1 Specific Styles Start */

.box_1{
	background: #E6E6FA;
}

input[type="checkbox"].switch_1{
	font-size: 15px;
	-webkit-appearance: none;
	   -moz-appearance: none;
	        appearance: none;
	width: 3.5em;
	height: 1.5em;
	background: #ddd;
	border-radius: 3em;
	position: relative;
	cursor: pointer;
	outline: none;
	-webkit-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
  }
  
  input[type="checkbox"].switch_1:checked{
	background: #0ebeff;
  }
  
  input[type="checkbox"].switch_1:after{
	position: absolute;
	content: "";
	width: 1.5em;
	height: 1.5em;
	border-radius: 50%;
	background: #fff;
	-webkit-box-shadow: 0 0 .25em rgba(0,0,0,.3);
	        box-shadow: 0 0 .25em rgba(0,0,0,.3);
	-webkit-transform: scale(.7);
	        transform: scale(.7);
	left: 0;
	-webkit-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
  }
  
  input[type="checkbox"].switch_1:checked:after{
	left: calc(100% - 1.5em);
  }
	
/* Switch 1 Specific Style End */



</style>
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->






<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="bloc-5">
                    <div>
                        <ul id="ul">
                            <li><a id="ac">
                                    <div>
                                        <h3>การตั้งค่า</h3>
                                        <span>ตั้งค่าระบบ</span>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                <div>
                  <ul id="ul">
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/profilex">ทั่วไป</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/account">Billing & subscriptions</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/paytypes">ประเภทการชำระเงิน</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/loyaltyx">ความจงรักภักดี</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/tax">ภาษี</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/tax_reciepts">ใบเสร็จรับเงิน</a></li>
                </ul>
                </div>
                <div>
                <ul id="ul">
                        <li><a id="ac">
                                <div>
                                <h3>ร้านค้า</h3>
                                <span>การตั้งค่า ร้านค้า และ POS</span>
                                </div>
                            </a>
                        </li>
                </ul>
                </div>
                <div>
                  <ul id="ul">
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/store">ร้านค้า</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/pos">อุปกรณ์ POS</a></li>
                  </ul>
                </div>
            </div>

        </div>
            <!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
            <!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
            <!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
            <!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
        <div class="col-md-8">
            <div class="jumbotron"  style="width:100%">
                <h3>การตั้งค่าสิทธิพิเศษ</h3>
                <br>
                <span>ประเภทสิทธิพิเศษ</span>
                    <h5>ระบบการให้เงินรางวัล</h5>
                <br>
                
                <form action="Customerbase.php" method="POST" name="bt1" style="width:100%;">
                    <label for="name"><h8>ร้อยละของจำนวนเงินที่ซื้อจะได้รับเครดิตเป็นคะแนนเข้าบัญชีของลูกค้า:</h8></label>
                    <input type="text" name="name" id="name" class="form-control"  style="width:100%;" placeholder="0.00"><br>
                    <label for="name"><h8>คะแนนสะสมให้กับลูกค้าสำหรับการติดตั้งแอป (รางวัลครั้งเดียว):</h8></label>
                    <input type="text" name="nname" id="nname" class="form-control"  style="width:100%;" placeholder="0.00"><br>
                    
                    
                      <br>
                      <br>
                      <br>
                <button  type="submit" name="btn1" class="button button4 " onclick="JavaScript:fncSubmit('page1')" > บันทึก </button> 
                <button  type="submit" name="btn2" class="button button4 "  onclick="JavaScript:fncSubmit('page2')" > ยกเลิก </button>
                
                <br>
                <br>
                
                 </form>
                    
                

        </div>
        </div>
            <script language="javascript">
                function fncSubmit(strPage)
                {
                    if (strPage == "page1")
                    {
                        document.bt1.action = "setting_tax.php?action=setting_tax.php";
                    }

                    if (strPage == "page2")
                    {
                        document.bt1.action = "setting_tax.php?action=setting_tax.php";
                    }

                    document.bt1.submit();
                }
            </script>
            <script>
                var x, i, j, selElmnt, a, b, c;
                /*look for any elements with the class "custom-select":*/
                x = document.getElementsByClassName("custom-select");
                for (i = 0; i < x.length; i++) {
                    selElmnt = x[i].getElementsByTagName("select")[0];
                    /*for each element, create a new DIV that will act as the selected item:*/
                    a = document.createElement("DIV");
                    a.setAttribute("class", "select-selected");
                    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
                    x[i].appendChild(a);
                    /*for each element, create a new DIV that will contain the option list:*/
                    b = document.createElement("DIV");
                    b.setAttribute("class", "select-items select-hide");
                    for (j = 0; j < selElmnt.length; j++) {
                        /*for each option in the original select element,
                         create a new DIV that will act as an option item:*/
                        c = document.createElement("DIV");
                        c.innerHTML = selElmnt.options[j].innerHTML;
                        c.addEventListener("click", function (e) {
                            /*when an item is clicked, update the original select box,
                             and the selected item:*/
                            var y, i, k, s, h;
                            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                            h = this.parentNode.previousSibling;
                            for (i = 0; i < s.length; i++) {
                                if (s.options[i].innerHTML == this.innerHTML) {
                                    s.selectedIndex = i;
                                    h.innerHTML = this.innerHTML;
                                    y = this.parentNode.getElementsByClassName("same-as-selected");
                                    for (k = 0; k < y.length; k++) {
                                        y[k].removeAttribute("class");
                                    }
                                    this.setAttribute("class", "same-as-selected");
                                    break;
                                }
                            }
                            h.click();
                        });
                        b.appendChild(c);
                    }
                    x[i].appendChild(b);
                    a.addEventListener("click", function (e) {
                        /*when the select box is clicked, close any other select boxes,
                         and open/close the current select box:*/
                        e.stopPropagation();
                        closeAllSelect(this);
                        this.nextSibling.classList.toggle("select-hide");
                        this.classList.toggle("select-arrow-active");
                    });
                }
                function closeAllSelect(elmnt) {
                    /*a function that will close all select boxes in the document,
                     except the current select box:*/
                    var x, y, i, arrNo = [];
                    x = document.getElementsByClassName("select-items");
                    y = document.getElementsByClassName("select-selected");
                    for (i = 0; i < y.length; i++) {
                        if (elmnt == y[i]) {
                            arrNo.push(i)
                        } else {
                            y[i].classList.remove("select-arrow-active");
                        }
                    }
                    for (i = 0; i < x.length; i++) {
                        if (arrNo.indexOf(i)) {
                            x[i].classList.add("select-hide");
                        }
                    }
                }
                /*if the user clicks anywhere outside the select box,
                 then close all select boxes:*/
                document.addEventListener("click", closeAllSelect);
            </script>       
       

<!-- End Page Content -->
<jsp:include page="../footer_1.jsp" />

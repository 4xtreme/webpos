<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

 <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"></h1>
            </div>
        </div>
    
                <div class="row">
                    
                </div>
               
             <form:form action="test" method="POST" modelAttribute="product">
			<div class="row">
				<div class="col-lg-12">
				
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="Profile">
                                           
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <dl class="dl-horizontal">

                                                        <dt>Product Name:</dt> <dd><form:input class="form-control" path="product_name"  required=""/></dd><br>
                                                        <dt>Color:</dt> <dd><form:input class="form-control" path="color"  required=""/></dd><br>
                                                        <dt>Size:</dt> <dd><form:input class="form-control" path="size"  required=""/></dd><br>
                                                        <dt>Pirce: </dt> <dd><form:input class="form-control" path="pirce"  required=""/></dd><br>
                                                         <dt>Stock: </dt> <dd><form:input class="form-control" path="stock"  required=""/></dd><br>
                                                          
                                                    </dl>
                                                </div>
                                                <div class="col-lg-6">
                                                    <dl class="dl-horizontal">
                                                        <dt>Start Date: </dt> <dd>
                                                            <div class='input-group date' >
                                                                 <input  class="form-control datetime" name="startdate" required=""/>
                                                                <!--form:input class="form-control datetime" path="sell_start_date"  required=""/--> <!--form:errors path="sell_start_date" /-->
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>                                                            
                                                           </dd><br>
                                                          
                                                        <dt>End Date: </dt> <dd>
                                                             <div class='input-group date' >
                                                                 <input  class="form-control datetime" name="enddate" required=""/>
                                                                 <!--form:input class="form-control datetime" path="sell_end_date"  required="" /--> <!--form:errors path="sell_end_date" /-->
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>  
                                                            
                                                           </dd><br>

                                                        <dt>Detail: </dt> <dd><form:textarea class="form-control" rows="5" path="detail"  required=""/></dd><br>
                                                       

                                                    </dl>
                                                </div>
                                            </div>
                                             
                                            

                                    </div>
                               
                                </div>
						
				  </div>
			 </div>
			</form:form>


    </div>


    <!-- End Content -->
</div>

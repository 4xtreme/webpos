<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
  <style>
    .wrapper{
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	width: 100px;
	margin: 50vh auto 0;
	-ms-flex-wrap: wrap;
	    flex-wrap: wrap;
	-webkit-transform: translateY(-50%);
	        transform: translateY(-50%);
}

.switch_box{
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	max-width: 60px;
	min-width: 60px;
	-webkit-box-pack: center;
	    -ms-flex-pack: center;
	        justify-content: center;
	-webkit-box-align: center;
	    -ms-flex-align: center;
	        align-items: center;
	-webkit-box-flex: 1;
	    -ms-flex: 1;
	        flex: 1;
}

/* Switch 1 Specific Styles Start */

.box_1{
	background: #E6E6FA;
}

input[type="checkbox"].switch_1{
	font-size: 15px;
	-webkit-appearance: none;
	   -moz-appearance: none;
	        appearance: none;
	width: 3.5em;
	height: 1.5em;
	background: #ddd;
	border-radius: 3em;
	position: relative;
	cursor: pointer;
	outline: none;
	-webkit-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
  }
  
  input[type="checkbox"].switch_1:checked{
	background: #0ebeff;
  }
  
  input[type="checkbox"].switch_1:after{
	position: absolute;
	content: "";
	width: 1.5em;
	height: 1.5em;
	border-radius: 50%;
	background: #fff;
	-webkit-box-shadow: 0 0 .25em rgba(0,0,0,.3);
	        box-shadow: 0 0 .25em rgba(0,0,0,.3);
	-webkit-transform: scale(.7);
	        transform: scale(.7);
	left: 0;
	-webkit-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
  }
  
  input[type="checkbox"].switch_1:checked:after{
	left: calc(100% - 1.5em);
  }
	
/* Switch 1 Specific Style End */



</style>
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->






          <div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
            <div class="bloc-5">
            <div>
                <ul id="ul">
                        <li><a id="ac">
                                <div>
                                <h3>การตั้งค่า</h3>
                                <span>ตั้งค่าระบบ</span>
                                </div>
                            </a>
                        </li>
                </ul>

            </div>
                <div>
                  <ul id="ul">
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/profilex">ทั่วไป</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/account">Billing & subscriptions</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/paytypes">ประเภทการชำระเงิน</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/loyaltyx">ความจงรักภักดี</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/tax">ภาษี</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/tax_reciepts">ใบเสร็จรับเงิน</a></li>
                </ul>
                </div>
                <div>
                <ul id="ul">
                        <li><a id="ac">
                                <div>
                                <h3>ร้านค้า</h3>
                                <span>การตั้งค่า ร้านค้า และ POS</span>
                                </div>
                            </a>
                        </li>
                </ul>
                </div>
                <div>
                  <ul id="ul">
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/store">ร้านค้า</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/pos">อุปกรณ์ POS</a></li>
                  </ul>
                </div>
            </div>

        </div>
    <!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
    <!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
    <!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
    <!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
        <div class="col-md-8">
            <div class="jumbotron" style="width: 100%">
                <h3>ตั้งค่าใบเสร็จ</h3>
                
                
                <br>
                <form action="Customerbase.php" method="POST" name="bt1" style="width:100%;">
                    <label for="name">อีเมล:</label>
                    <input type="text" name="name" id="name" class="form-control"  style="width:100%;"><br>
                    
                    <label for="name">ประเภท:</label><br>
                        <select class="form-control">
                            <option label="อาหารและเครื่องดื่ม" value="number:1">อาหารและเครื่องดื่ม</option>
                            <option label="ร้านกาแฟ, บาร์, ร้านอาหาร" value="number:2">ร้านกาแฟ, บาร์, ร้านอาหาร</option>
                            <option label="ความบันเทิง" value="number:3">ความบันเทิง</option>
                            <option label="สุขภาพและความงาม" value="number:4">สุขภาพและความงาม</option>
                            <option label="เสื้อผ้าและรองเท้า" value="number:5">เสื้อผ้าและรองเท้า</option>
                            <option label="เครื่องใช้ไฟฟ้าและอิเล็กทรอนิกส์" value="number:6">เครื่องใช้ไฟฟ้าและอิเล็กทรอนิกส์</option>
                            <option label="บริการ" value="number:7">บริการ</option>
                            <option label="รถยนต์" value="number:8">รถยนต์</option>
                            <option label="การท่องเที่ยว" value="number:9">การท่องเที่ยว</option>
                            <option label="เด็ก" value="number:10">เด็ก</option>
                            <option label="อื่นๆ" value="number:11" selected="selected">อื่นๆ</option>
                        </select>
                      <br>
                    
                    
                    <label for="name">ชื่อของธุรกิจ:</label>
                    <input type="text" name="nname" id="nname" class="form-control"  style="width:100%;"><br>
                    
                    
                    <label for="name">รายละเอียด:</label>
                    <input type="text" name="nname" id="nname" class="form-control"  style="width:100%;"><br>
                    
                    <div class="uplode" align="center">
                    <div class="uplode1">
                    <input type="file" name="file-1[]" id="file-6" class="inputfile inputfile-5" data-multiple-caption="{count} files selected" multiple />
                    <label for="file-6"><figure><svg xmlns="http://www.w3.org/2000/svg" width="50" height="10" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg></figure> <span></span></label>
                    
                    <input type="file" name="file-2[]" id="file-6" class="inputfile inputfile-5" data-multiple-caption="{count} files selected" multiple />
                    <label for="file-6"><figure><svg xmlns="http://www.w3.org/2000/svg" width="50" height="10" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg></figure> <span></span></label>
                    </div>
                    </div>
                    
                    
                    
                    
                    <label for="name">เว็บไซต์:</label>
                    <input type="text" name="nname" id="nname" class="form-control"  style="width:100%;"><br>
                    
                    <br>
                    <br>
                    <br>
                    <h8>เพิ่ม</h8>
                    <br>
                    <h4>ตั้งค่าทั่วไป</h4>
                    <br>
                    
                    <label for="name">ใช้กะ:</label>
                        <div class="switch_box box_1" id="div6">
                            <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ck_frm();">
                        </div>
                       
                    <br>
                    <br>
                    <label for="name">ใช้ระบบลงเวลา:</label>
                        <div class="switch_box box_1" id="div6">
                            <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ck_frm();">
                        </div>
                    <br>
                    <br>
                    <label for="name">ใช้การเปิดตั๋ว:</label>
                        <div class="switch_box box_1" id="div6">
                            <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ck_frm();">
                        </div>
                       
                    <br>
                    <br>
                    <label for="name">ได้รับอีเมล์แจ้งเตือนปริมาณสินค้าสต็อกเหลือน้อย:</label>
                        <div class="switch_box box_1" id="div6">
                            <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ck_frm();">
                        </div>
                       
                    <br>
                    <br>
                    <label for="name">ใช้เครื่องพิมพ์ในห้องครัว:</label>
                        <div class="switch_box box_1" id="div6">
                            <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ck_frm();">
                        </div>
                       
                    <br>
                    <br>
                    <label for="name">ใช้จอแสดงผลฝั่งลูกค้า:</label>
                        <div class="switch_box box_1" id="div6">
                            <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ck_frm();">
                        </div>
                       
                    <br>
                    <br>
                    <label for="name">ใช้ทางเลือกสั่งออเดอร์:</label>
                        <div class="switch_box box_1" id="div6">
                            <input type="checkbox" class="switch_1" name="ckkk" id="ckkk" onclick="ck_frm();">
                        </div>
                       
                    <br>
                    <br>
                    
                    
                    <label for="name">เขตเวลา:</label><br>
                        <select class="form-control">
                            <option label="(UTC-12:00) International Date Line West" value="string:Etc/GMT+12">(UTC-12:00) International Date Line West</option>
                            <option label="(UTC-11:00) Coordinated Universal Time -11" value="string:Etc/GMT+11">(UTC-11:00) Coordinated Universal Time -11</option>
                            <option label="(UTC-10:00) Hawaii" value="string:Pacific/Honolulu">(UTC-10:00) Hawaii</option>
                            <option label="(UTC-09:00) Alaska" value="string:America/Anchorage">(UTC-09:00) Alaska</option>
                            <option label="(UTC-08:00) Pacific Time (US and Canada)" value="string:America/Los_Angeles">(UTC-08:00) Pacific Time (US and Canada)</option>
                            <option label="(UTC-08:00) Baja California" value="string:America/Tijuana">(UTC-08:00) Baja California</option>
                            <option label="(UTC-07:00) Mountain Time (US and Canada)" value="string:America/Denver">(UTC-07:00) Mountain Time (US and Canada)</option>
                            <option label="(UTC-07:00) Chihuahua, La Paz, Mazatlan" value="string:America/Chihuahua">(UTC-07:00) Chihuahua, La Paz, Mazatlan</option>
                            <option label="(UTC-07:00) Arizona" value="string:America/Phoenix">(UTC-07:00) Arizona</option>
                            <option label="(UTC-06:00) Saskatchewan" value="string:America/Regina">(UTC-06:00) Saskatchewan</option>
                            <option label="(UTC-06:00) Central America" value="string:America/Guatemala">(UTC-06:00) Central America</option>
                            <option label="(UTC-06:00) Central Time (US and Canada)" value="string:America/Chicago">(UTC-06:00) Central Time (US and Canada)</option>
                            <option label="(UTC-06:00) Guadalajara, Mexico City, Monterrey" value="string:America/Mexico_City">(UTC-06:00) Guadalajara, Mexico City, Monterrey</option>
                            <option label="(UTC-05:00) Eastern Time (US and Canada)" value="string:America/New_York">(UTC-05:00) Eastern Time (US and Canada)</option>
                            <option label="(UTC-05:00) Bogota, Lima, Quito" value="string:America/Bogota">(UTC-05:00) Bogota, Lima, Quito</option>
                            <option label="(UTC-05:00) Indiana (East)" value="string:America/Indianapolis">(UTC-05:00) Indiana (East)</option>
                            <option label="(UTC-04:30) Caracas" value="string:America/Caracas">(UTC-04:30) Caracas</option>
                            <option label="(UTC-04:00) Atlantic Time (Canada)" value="string:America/Halifax">(UTC-04:00) Atlantic Time (Canada)</option>
                            <option label="(UTC-04:00) Cuiaba" value="string:America/Cuiaba">(UTC-04:00) Cuiaba</option>
                            <option label="(UTC-04:00) Santiago" value="string:America/Santiago">(UTC-04:00) Santiago</option>
                            <option label="(UTC-04:00) Georgetown, La Paz, Manaus, San Juan" value="string:America/La_Paz">(UTC-04:00) Georgetown, La Paz, Manaus, San Juan</option>
                            <option label="(UTC-04:00) Asuncion" value="string:America/Asuncion">(UTC-04:00) Asuncion</option>
                            <option label="(UTC-03:30) Newfoundland" value="string:America/St_Johns">(UTC-03:30) Newfoundland</option>
                            <option label="(UTC-03:00) Brasilia" value="string:America/Sao_Paulo">(UTC-03:00) Brasilia</option>
                            <option label="(UTC-03:00) Greenland" value="string:America/Godthab">(UTC-03:00) Greenland</option>
                            <option label="(UTC-03:00) Montevideo" value="string:America/Montevideo">(UTC-03:00) Montevideo</option>
                            <option label="(UTC-03:00) Cayenne, Fortaleza" value="string:America/Cayenne">(UTC-03:00) Cayenne, Fortaleza</option>
                            <option label="(UTC-03:00) Buenos Aires" value="string:America/Buenos_Aires">(UTC-03:00) Buenos Aires</option>
                            <option label="(UTC-02:00) Mid-Atlantic" value="string:Etc/GMT+2">(UTC-02:00) Mid-Atlantic</option>
                            <option label="(UTC-01:00) Azores" value="string:Atlantic/Azores">(UTC-01:00) Azores</option>
                            <option label="(UTC-01:00) Cabo Verde Is." value="string:Atlantic/Cape_Verde">(UTC-01:00) Cabo Verde Is.</option><option label="(UTC+00:00) Dublin, Edinburgh, Lisbon, London" value="string:Europe/London">(UTC+00:00) Dublin, Edinburgh, Lisbon, London</option>
                            <option label="(UTC+00:00) Monrovia, Reykjavik" value="string:Atlantic/Reykjavik">(UTC+00:00) Monrovia, Reykjavik</option>
                            <option label="(UTC+00:00) Casablanca" value="string:Africa/Casablanca">(UTC+00:00) Casablanca</option>
                            <option label="(UTC+00:00) Coordinated Universal Time" value="string:Etc/GMT">(UTC+00:00) Coordinated Universal Time</option>
                            <option label="(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague" value="string:Europe/Budapest">(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                            <option label="(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb" value="string:Europe/Warsaw">(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                            <option label="(UTC+01:00) Brussels, Copenhagen, Madrid, Paris" value="string:Europe/Paris">(UTC+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                            <option label="(UTC+01:00) West Central Africa" value="string:Africa/Lagos">(UTC+01:00) West Central Africa</option>
                            <option label="(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna" value="string:Europe/Berlin">(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                            <option label="(UTC+01:00) Windhoek" value="string:Africa/Windhoek">(UTC+01:00) Windhoek</option>
                            <option label="(UTC+03:00) Minsk" value="string:Europe/Minsk">(UTC+03:00) Minsk</option>
                            <option label="(UTC+02:00) Cairo" value="string:Africa/Cairo">(UTC+02:00) Cairo</option>
                            <option label="(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius" value="string:Europe/Kiev">(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                            <option label="(UTC+02:00) Athens, Bucharest" value="string:Europe/Bucharest">(UTC+02:00) Athens, Bucharest</option>
                            <option label="(UTC+02:00) Jerusalem" value="string:Asia/Jerusalem">(UTC+02:00) Jerusalem</option>
                            <option label="(UTC+02:00) Amman" value="string:Asia/Amman">(UTC+02:00) Amman</option>
                            <option label="(UTC+02:00) Beirut" value="string:Asia/Beirut">(UTC+02:00) Beirut</option>
                            <option label="(UTC+02:00) Harare, Pretoria" value="string:Africa/Johannesburg">(UTC+02:00) Harare, Pretoria</option>
                            <option label="(UTC+02:00) Damascus" value="string:Asia/Damascus">(UTC+02:00) Damascus</option>
                            <option label="(UTC+02:00) Istanbul" value="string:Europe/Istanbul">(UTC+02:00) Istanbul</option>
                            <option label="(UTC+03:00) Kuwait, Riyadh" value="string:Asia/Riyadh">(UTC+03:00) Kuwait, Riyadh</option>
                            <option label="(UTC+03:00) Baghdad" value="string:Asia/Baghdad">(UTC+03:00) Baghdad</option>
                            <option label="(UTC+03:00) Nairobi" value="string:Africa/Nairobi">(UTC+03:00) Nairobi</option>
                            <option label="(UTC+02:00) Kaliningrad" value="string:Europe/Kaliningrad">(UTC+02:00) Kaliningrad</option>
                            <option label="(UTC+03:30) Tehran" value="string:Asia/Tehran">(UTC+03:30) Tehran</option>
                            <option label="(UTC+03:00) Moscow, St. Petersburg, Volgograd" value="string:Europe/Moscow">(UTC+03:00) Moscow, St. Petersburg, Volgograd</option>
                            <option label="(UTC+04:00) Abu Dhabi, Muscat" value="string:Asia/Dubai">(UTC+04:00) Abu Dhabi, Muscat</option>
                            <option label="(UTC+04:00) Baku" value="string:Asia/Baku">(UTC+04:00) Baku</option>
                            <option label="(UTC+04:00) Yerevan" value="string:Asia/Yerevan">(UTC+04:00) Yerevan</option>
                            <option label="(UTC+04:00) Tbilisi" value="string:Asia/Tbilisi">(UTC+04:00) Tbilisi</option>
                            <option label="(UTC+04:00) Port Louis" value="string:Indian/Mauritius">(UTC+04:00) Port Louis</option>
                            <option label="(UTC+04:30) Kabul" value="string:Asia/Kabul">(UTC+04:30) Kabul</option>
                            <option label="(UTC+05:00) Tashkent" value="string:Asia/Tashkent">(UTC+05:00) Tashkent</option>
                            <option label="(UTC+05:00) Islamabad, Karachi" value="string:Asia/Karachi">(UTC+05:00) Islamabad, Karachi</option><option label="(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi" value="string:Asia/Calcutta">(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                            <option label="(UTC+05:30) Sri Jayawardenepura" value="string:Asia/Colombo">(UTC+05:30) Sri Jayawardenepura</option>
                            <option label="(UTC+05:45) Kathmandu" value="string:Asia/Katmandu">(UTC+05:45) Kathmandu</option>
                            <option label="(UTC+05:00) Ekaterinburg" value="string:Asia/Yekaterinburg">(UTC+05:00) Ekaterinburg</option>
                            <option label="(UTC+06:00) Astana" value="string:Asia/Almaty">(UTC+06:00) Astana</option>
                            <option label="(UTC+06:00) Dhaka" value="string:Asia/Dhaka">(UTC+06:00) Dhaka</option>
                            <option label="(UTC+06:30) Yangon (Rangoon)" value="string:Asia/Rangoon">(UTC+06:30) Yangon (Rangoon)</option>
                            <option label="(UTC+06:00) Novosibirsk" value="string:Asia/Novosibirsk">(UTC+06:00) Novosibirsk</option>
                            <option label="(UTC+07:00) Bangkok, Hanoi, Jakarta" value="string:Asia/Bangkok">(UTC+07:00) Bangkok, Hanoi, Jakarta</option>
                            <option label="(UTC+07:00) Krasnoyarsk" value="string:Asia/Krasnoyarsk">(UTC+07:00) Krasnoyarsk</option>
                            <option label="(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi" value="string:Asia/Shanghai">(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                            <option label="(UTC+08:00) Kuala Lumpur, Singapore" value="string:Asia/Singapore">(UTC+08:00) Kuala Lumpur, Singapore</option>
                            <option label="(UTC+08:00) Taipei" value="string:Asia/Taipei">(UTC+08:00) Taipei</option>
                            <option label="(UTC+08:00) Perth" value="string:Australia/Perth">(UTC+08:00) Perth</option>
                            <option label="(UTC+08:00) Ulaanbaatar" value="string:Asia/Ulaanbaatar">(UTC+08:00) Ulaanbaatar</option>
                            <option label="(UTC+08:00) Irkutsk" value="string:Asia/Irkutsk">(UTC+08:00) Irkutsk</option>
                            <option label="(UTC+09:00) Seoul" value="string:Asia/Seoul">(UTC+09:00) Seoul</option>
                            <option label="(UTC+09:00) Osaka, Sapporo, Tokyo" value="string:Asia/Tokyo">(UTC+09:00) Osaka, Sapporo, Tokyo</option>
                            <option label="(UTC+09:30) Darwin" value="string:Australia/Darwin">(UTC+09:30) Darwin</option>
                            <option label="(UTC+09:30) Adelaide" value="string:Australia/Adelaide">(UTC+09:30) Adelaide</option>
                            <option label="(UTC+09:00) Yakutsk" value="string:Asia/Yakutsk">(UTC+09:00) Yakutsk</option>
                            <option label="(UTC+10:00) Canberra, Melbourne, Sydney" value="string:Australia/Sydney">(UTC+10:00) Canberra, Melbourne, Sydney</option>
                            <option label="(UTC+10:00) Brisbane" value="string:Australia/Brisbane">(UTC+10:00) Brisbane</option>
                            <option label="(UTC+10:00) Hobart" value="string:Australia/Hobart">(UTC+10:00) Hobart</option>
                            <option label="(UTC+10:00) Guam, Port Moresby" value="string:Pacific/Port_Moresby">(UTC+10:00) Guam, Port Moresby</option>
                            <option label="(UTC+10:00) Vladivostok" value="string:Asia/Vladivostok">(UTC+10:00) Vladivostok</option>
                            <option label="(UTC+11:00) Solomon Is., New Caledonia" value="string:Pacific/Guadalcanal">(UTC+11:00) Solomon Is., New Caledonia</option>
                            <option label="(UTC+10:00) Magadan" value="string:Asia/Magadan">(UTC+10:00) Magadan</option>
                            <option label="(UTC+12:00) Fiji" value="string:Pacific/Fiji">(UTC+12:00) Fiji</option>
                            <option label="(UTC+12:00) Auckland, Wellington" value="string:Pacific/Auckland">(UTC+12:00) Auckland, Wellington</option>
                            <option label="(UTC+12:00) Coordinated Universal Time +12" value="string:Etc/GMT-12">(UTC+12:00) Coordinated Universal Time +12</option>
                            <option label="(UTC+13:00) Nuku'alofa" value="string:Pacific/Tongatapu">(UTC+13:00) Nuku'alofa</option>
                            <option label="(UTC+13:00) Samoa" value="string:Pacific/Samoa">(UTC+13:00) Samoa</option>
                            <option label="Asia/Jakarta" value="string:Asia/Jakarta" selected="selected">Asia/Jakarta</option>
                        </select>
                      <br>
                     
                    
                    
                    
                    
                      <label for="name">ภาษาของระบบ:</label><br>
                        <select class="form-control">
                          <option label="Deutsch" value="string:deu">Deutsch</option>
                            <option label="English" value="string:eng">English</option>
                            <option label="Español" value="string:spa">Español</option>
                            <option label="Français" value="string:fra">Français</option>
                            <option label="Italiano" value="string:ita">Italiano</option>
                            <option label="Polski" value="string:pol">Polski</option>
                            <option label="Portuguesa" value="string:por">Portuguesa</option>
                            <option label="Romana" value="string:ron">Romana</option>
                            <option label="Български" value="string:bul">Български</option>
                            <option label="Русский" value="string:rus">Русский</option>
                            <option label="日本語" value="string:jpn">日本語</option>
                            <option label="한국어" value="string:kor">한국어</option>
                            <option label="Tiếng Việt" value="string:vie">Tiếng Việt</option>
                            <option label="Eesti" value="string:est">Eesti</option>
                            <option label="اللغة العربية&lrm;" value="string:ara">اللغة العربية&lrm;</option>
                            <option label="ελληνικά" value="string:ell">ελληνικά</option>
                            <option label="漢語" value="string:zho">漢語</option>
                            <option label="Bahasa Indonesia" value="string:ind">Bahasa Indonesia</option>
                            <option label="ภาษาไทย" value="string:tha" selected="selected">ภาษาไทย</option>
                            <option label="Nederlands" value="string:nld">Nederlands</option>
                            <option label="Latviešu valoda" value="string:lav">Latviešu valoda</option>
                            <option label="Magyar" value="string:hun">Magyar</option>
                            <option label="Norsk bokmål" value="string:nor">Norsk bokmål</option>
                            <option label="Malay" value="string:msa">Malay</option>
                            <option label="हिन्दी" value="string:hin">हिन्दी</option>
                            <option label="ქართული" value="string:kat">ქართული</option>
                            <option label="Македонски" value="string:mkd">Македонски</option>
                            <option label="Türkçe" value="string:tur">Türkçe</option>
                            <option label="বাংলা" value="string:ben">বাংলা</option>
                            <option label="Suomi" value="string:fin">Suomi</option>
                        </select>
                    <br>
                    <br>                       
                            <button  type="submit" name="btn1" class="button button4" onclick="JavaScript:fncSubmit('page1')" > บันทึก </button> 
                            <button  type="submit" name="btn2" class="button button4"  onclick="JavaScript:fncSubmit('page2')" > ยกเลิก </button>
                
                <br>
                <br>
                
                 </form>
            </div>
        </div>
<script language="javascript">
    function fncSubmit(strPage)
{
	if(strPage == "page1")
	{
		document.bt1.action="setting_tax.php?action=setting_tax.php";
	}
	
	if(strPage == "page2")
	{
		document.bt1.action="setting_tax.php?action=setting_tax.php";
	}	
	
	document.bt1.submit();
}
</script>
<script>
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
</script> 


<jsp:include page="../footer_1.jsp" />

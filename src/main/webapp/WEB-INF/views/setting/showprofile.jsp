
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../header.jsp" />
<jsp:include page="../navigation.jsp" />
 <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
		<!-- start Content -->
            <div class="row">
				<div class="col-lg-12">
                        <h1 class="page-header">Profile</h1>
                </div>
			</div>
			

			<div class="row">
				<div class="col-lg-12">
				
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="Profile">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <dl class="dl-horizontal">

                                                        <dt>Full name:</dt> <dd> ${fullname}</dd>    
                                                        <dt>Company:</dt> <dd> ${profile.company_name}</dd>   
                                                        <dt>Phone: </dt> <dd> ${fullphone}</dd>
                                                        <dt>Email: </dt> <dd> ${profile.email}</dd>
                                                        <dt>Facebook:</dt> <dd>${profile.facebook}</dd>
                                                        <dt>Line:</dt> <dd> ${profile.line}</dd>

                                                    </dl>
                                                </div>
                                                <div class="col-lg-6">
                                                    <dl class="dl-horizontal">
                                                        <dt>Address: </dt> <dd> ${profile.address}</dd>
                                                        <dt>District: </dt> <dd> ${profile.district}</dd>
                                                        <dt>City:  </dt> <dd> ${profile.city}</dd>
                                                        <dt>Country: </dt> <dd> ${profile.country}</dd>
                                                         <dt>Postalcode: </dt> <dd> ${profile.postalcode}</dd><br><br>
                                                    </dl>
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                    </div>
                                                         

                                </div>
                        </div>
			
			<!-- End Content -->
        </div>
    </div>
	<!-- End Page Content -->
 <jsp:include page="../footer.jsp" />
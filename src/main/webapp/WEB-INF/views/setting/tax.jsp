<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->






    <div class="content-wrapper">
    <div class="container">
        <div class="row">
        <div class="col-md-4">
        <div class="bloc-5">
        <div>
            <ul id="ul">
                    <li><a id="ac">
                            <div>
                            <h3>การตั้งค่า</h3>
                            <span>ตั้งค่าระบบ</span>
                            </div>
                        </a>
                    </li>
            </ul>
            
        </div>
                <div>
                  <ul id="ul">
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/profilex">ทั่วไป</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/account">Billing & subscriptions</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/paytypes">ประเภทการชำระเงิน</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/loyaltyx">ความจงรักภักดี</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/tax">ภาษี</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/tax_reciepts">ใบเสร็จรับเงิน</a></li>
                </ul>
                </div>
                <div>
                <ul id="ul">
                        <li><a id="ac">
                                <div>
                                <h3>ร้านค้า</h3>
                                <span>การตั้งค่า ร้านค้า และ POS</span>
                                </div>
                            </a>
                        </li>
                </ul>
                </div>
                <div>
                  <ul id="ul">
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/store">ร้านค้า</a></li>
                  <li><a id="a" href="${pageContext.request.contextPath}/setting/pos">อุปกรณ์ POS</a></li>
                  </ul>
                </div>
            </div>

        </div>
<!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
<!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
<!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->
<!--  -----------------------------  แบ่งข้อมล    --------------  -------------     -->

        <div class="col-md-8">
            <div class="jumbotron" >
                <div><h3>ภาษี</h3></div>
                <div class="block-1"><a href="${pageContext.request.contextPath}/setting/tax_addtax"><button type="button" class="btn btn-success">+ เพิ่มรูปแบบภาษี</button></a></div>
                <div class="container">
                    <table class="table table-responsive-md table-hover "  width="100%" >
                    <thead>
                        <tr>
                            <th><div align="center"><input name="CheckAll" type="checkbox" id="chk_all" ></div></th>
                            <th><div align="center">ID</div></th>
                            <th><div align="center">ชื่อ</div></th>
                            <th><div align="center">ใช้กับสินค้าใหม่</div></th>
                            <th><div align="center">อัตราภาษี, %</div></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                                <td><div align="center"><input type="checkbox" name="chkDel[]" id="chk_list"></div></td>
                                <td><div align="center"></div></td>
                                <td><div align="center"></div></td>
                                
                                
                                
                            </tr>
                       
                    </tbody>
                </table>
                    <div class="block-12"><a href="#"><button name="btnDelete" type="button" class="button button4">ลบ</button></a></div>
                </div>
                
                
                <script language="JavaScript">
                   $("#chk_all").click(function(){
                       $('input:checkbox').not(this).prop('checked', this.checked)
                   });
                </script>
                
                
                
               
                
            </div>    
        </div>       
    <!-- Bootstrap core JavaScript-->
            <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.bundle.min.js"></script>
            <!-- Core plugin JavaScript-->
            <script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>
            <!-- Page level plugin JavaScript-->
            <script src="${pageContext.request.contextPath}/js/Chart.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
            <script src="${pageContext.request.contextPath}/js/dataTables.bootstrap4.js"></script>
            <!-- Custom scripts for all pages-->
            <script src="${pageContext.request.contextPath}/js/sb-admin.min.js"></script>
            <!-- Custom scripts for this page-->
            <script src="${pageContext.request.contextPath}/js/sb-admin-datatables.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/sb-admin-charts.min.js"></script>

            <script src="${pageContext.request.contextPath}/js/custom-file-input.js"></script>

            <script>(function (e, t, n) {
                                var r = e.querySelectorAll("html")[0];
                                r.className = r.className.replace(/(^|\s)no-js(\s|$)/, "$1js$2")
                            })
                                    (document, window, 0);
            </script>
           </div>
    </div>
</div> 
<!-- End Page Content -->
<jsp:include page="../footer_1.jsp" />

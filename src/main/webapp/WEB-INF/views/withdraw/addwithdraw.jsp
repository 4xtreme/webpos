<%-- 
    Document   : addwithdraw
    Created on : Nov 24, 2017, 8:33:19 AM
    Author     : pop
--%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- start Content -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Machine</h1>
            </div>
        </div>
        <div class="row" id="eiei">
            <div class="col-lg-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <form:form action="addwithdraw" modelAttribute="withdraw"   method="POST">
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-8" >
                                        <div class="dl-horizontal">

                                            <div class="form-group">
                                                <span class="label_title">ผู้ขอเบิก</span>
                                                <form:select path="withdrawal_id" name="withdrawal_id" class="form-control width_15">
                                                    <option>
                                                        สมชาย
                                                    </option>
                                                </form:select><form:errors path="withdrawal_id" /> 
                                                    <span class="label_title">คนขับ/ควบคุมเครื่อง</span>
                                                    <select path="driver_id" class="form-control width_15">
                                                        <option>
                                                            สมชาย
                                                        </option>
                                                    </select><form:errors path="driver_id" /> 
                                                </div>

                                                <div class="form-group">
                                                    <span class="label_title">จำวนวเงิน</span>
                                                    <form:input type="text" path="amount" /><form:errors path="amount"/>
                                                    <span>บาท</span>
                                                    <span class="label_title">ใช้ในโครงการ</span>
                                                    <form:select path="project_id" class="form-control width_25">
                                                        <option>
                                                            สร้างถนนสาย 123
                                                        </option>
                                                    </form:select><form:errors path="project_id" /> 
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-4">
                                                        <dt style="text-align: left; width: 100px"><span>วิธีจ่ายเงิน</span></dt> 
                                                        <dd style="text-align: left;margin-left: 100px">
                                                            <label style="display: block">
                                                                <input path="pay_type" type="radio" name="radio" value="cash" checked="true"/><label for="radio2"><span><span></span></span></label><span class="label_title"> เงินสด </span>
                                                            </label >
                                                            <label style="display: block">
                                                                <input path="pay_type" type="radio" name="radio" value="credit" /><label for="radio2"><span><span></span></span></label><span class="label_title"> เชื่อ/เครดิต </span>
                                                            </label>
                                                            <label style="display: block">
                                                                <input path="pay_type" type="radio" name="radio" value="tranfer" /><label for="radio2"><span><span></span></span></label><span class="label_title"> โอนธนาคาร </span>
                                                            </label>
                                                            <label style="display: block">
                                                                <input path="pay_type" type="radio" name="radio" value="cheque" /><label for="radio2"><span><span></span></span></label><span class="label_title"> เช็ค </span>
                                                            </label>
                                                        </dd>
                                                    </div>

                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <span>กำหนดชำระ</span>
                                                    <i class="fa fa-calendar " style="font-size: 20px"></i>
                                                    <form:input path="pay_date" type="text" class="datetime"  id="datepick"/><form:errors path="pay_date"/></br>
                                                    <span class="margin_left10" >อีก <form:input path="pay_date_count" type="text" disabled="true" class="no-border tran-bg" id="dayleft" value="0" style="width: 20px;text-align: center"/> วัน</span>
                                                </div></br></br></br></br>

                                                <div class="form-group">
                                                    <span>จ่ายให้กับ</span>
                                                    <select path="" class="form-control width_25">
                                                        <option>ร้าย XYZ อะไหล่ (2000)</option>
                                                    </select><form:errors path=""/>
                                                    <p class="margin_left10">123/4//////////////////////////////////////////</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="dl-horizontal">
                                                <div class="form-group">
                                                    <span> วันที่ </span>
                                                    <input type="text" path="" class="no-border tran-bg "/>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <span>หมายเลขใบเบิก : </span>
                                                    <input type="text" path="" class="no-border tran-bg "/>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <span>ผู้กรอกข้อมูล : </span>
                                                    <input type="text" path="" class="no-border tran-bg "/>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <span>สถานะ : </span>
                                                    <input type="text" path="" class="no-border tran-bg " value="ร่าง" disabled=""/>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <span>ปรับปรุงหลังสุดเมื่อ  : </span>
                                                    <input type="text" path="" class="no-border tran-bg "/>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <span>สำหรับเครื่องจักร หมายเลขทะเบียน/รหัส</span>
                                                <input type="text" path="" />
                                                <i type="button" class="fa fa-search" style="font-size:25px; color: skyblue;"></i>

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div class="form-group">
                                                <input type="text" path="" class="margin_left10" disabled="disable"value="TEST/TEST/TEST"/>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>หมวดเครื่องจัก : </span> 
                                                <input type="text" path="" class="no-border tran-bg " disabled="disable"value="XYZ" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>อายุใช้งาน : </span>
                                                <input type="text" path="" class="no-border tran-bg" disabled="disable"value="3/4/20"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span>ที่ตั้งปัจจุบัน : </span>
                                                <input type="text" path="" class="no-border tran-bg" disabled="disable"value="โครงการ XYZ"/>

                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                    </br>
                                    </br>
                                    <div class="row">
                                        <div class="col-lg-12" style="border-top: solid; border-width: 1px; ">
                                            </br></br>
                                            <div class="dl-horizontal">
                                                <button onclick="createRow()" id="add-btn" type="botton" >เพิ่มรายการในตาราง</button>
                                                </br>
                                                </br>
                                                <div class="form-group ">
                                                    <span>อุปกรณ์ที่เสียหาย/รายการ</span>
                                                    <select id='brokenitem'  class="form-control width_25"> 
                                                        <option  value="" disabled=" " selected="">Select product</option>
                                                        <option  value="ยาง"  >ยาง</option>

                                                    </select>

                                                </div>
                                                <div class="form-group">
                                                    <span>ประเภทการเสียซ่อม</span>
                                                    <select id="type" class="form-control width_10"> 
                                                        <option  value="" disabled=" " selected="">Select product</option>
                                                        <option  value="ซ่อม"  >ซ่อม</option>
                                                    </select>
                                                    <span class="margin_left10">จำนวน</span>
                                                    <input type="number" name="quantity" min="0"  value="0" id="count">
                                                    <span class="margin_left10">ราคาประเมินต่อหน่วย</span>
                                                    <input type="text" path="" id="price"/>
                                                    <span >บาท</span>
                                                    <span class="margin_left10">ยี่ห้อ/หมายเหตุ</span>
                                                    <input type="text" path="" id="brand"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover del-line" id="mytable">
                                                    <thead >
                                                        <tr >
                                                            <th class="text-center">ลำดับ</th>
                                                            <th class="text-center">อุปกรณที่เสีย</th>
                                                            <th class="text-center">ประเภทการเสียซ่อม</th>
                                                            <th class="text-center">จำนวน</th>
                                                            <th class="text-center">ราคา/หน่วย</th>
                                                            <th class="text-center">ราคารวม</th>
                                                            <th class="text-center">ยี่ห่อ/หมายเหตุ</th>
                                                            <th class="text-center" hidden="true" style="border: none"></th>
                                                            <th class="text-center"hidden="true" style="border: none"></th>
                                                        </tr>
                                                    </thead>

                                                    <tbody   id="tableToModify" >
                                                        <!-- สร้างตารางใหม่ -->

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <div class="row margin_top10"> 
                                                    <div class="col-xs-6 " style="margin-left: 15px">
                                                        <label class="fileContainer">
                                                            <i class="fa fa-link fa-flip-horizontal attach-file" aria-hidden="true"></i>
                                                            <input type="file" id="uploadBtn" path="file"  name="file" />
                                                        </label>
                                                        <span style="line-height: 60px">แนบไฟล์</span></br>
                                                        <input id="uploadFile" class="form-control-sm no-border width_full file_dir2" type="text" value="No file selected."  disabled="disabled"  /><br>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <span>รวมทั้งสิ้น  </span>
                                                        <input id="totalPrice" type="text" path="" class="no-border tran-bg-right width_10" disabled="disable"value="0"/>
                                                        <span>  บาท  </span>
                                                        </br>
                                                    </div>		
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="dl-horizontal">
                                                <div class="form-group">
                                                    <span>บันทึกผู้ขอเบิก&nbsp;</span>
                                                    <textarea class="custom-textarea"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="dl-horizontal">
                                                <div class="form-group">
                                                    <div style="width: 120px;display: inline-block">
                                                        <span >บันทึกผู้ตรวจสอบ / พนักงานการเงิน</span>
                                                    </div>

                                                    <textarea class="custom-textarea"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="dl-horizontal">
                                                <div class="form-group">
                                                    <span>บันทึกผู้อนุมัติ &nbsp;</span>
                                                    <textarea class="custom-textarea"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <center>
                                                <button class="btn  button-size-cancel">Cancel</button>
                                                <button class="btn  button-size-save"style="margin-left: 20px;margin-right: 20px">Save</button>
                                                <button class="btn  button-size-sucess" type="submit" >Submit</button>
                                            </center>
                                        </div>
                                    </div>
                                    </br>
                                    </br>
                                    </br>
                                </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script>
                                                    document.getElementById("uploadBtn").onchange = function () {
                                                        var split = this.value;
                                                        split = split.substr(12, );
                                                        document.getElementById("uploadFile").value = split;
                                                    };

</script>

<script>

    var idcount = 0;
    function createRow() {
        idcount += 1;

        var brokenitem = document.getElementById("brokenitem").value;
        var type = document.getElementById("type").value;
        var count = document.getElementById("count").value;
        var price = document.getElementById("price").value;
        var brand = document.getElementById("brand").value;


        var sumCount = price * count;

        var row = document.createElement('tr'); // create row node
        row.setAttribute("id", "row" + idcount);
        row.setAttribute("class", "trow");
        var col = document.createElement('td'); // create column node
        var col2 = document.createElement('td'); // create second column node
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var col5 = document.createElement('td');
        var col6 = document.createElement('td');
        col6.setAttribute("id", "colPrice" + idcount);
        col6.setAttribute("value", "0");
        var col7 = document.createElement('td');
        var col8 = document.createElement('td');
        col8.setAttribute("style", "border:0;width:50px");
        var col9 = document.createElement('td');
        col9.setAttribute("style", "border:0;width:50px");

        var editbtn = document.createElement("i");
        editbtn.setAttribute("type", "button");
        editbtn.setAttribute("class", "fa fa-edit");
        editbtn.setAttribute("style", "font-size:30px; color: skyblue;");
        editbtn.setAttribute("id", "edit-btn" + idcount);


        var deletebtn = document.createElement("input");
        deletebtn.setAttribute("type", "button");
        deletebtn.setAttribute("class", "btn btn-circle-plus");
        deletebtn.setAttribute("value", "-");
        deletebtn.setAttribute("id", "del-btn" + idcount);
        deletebtn.setAttribute("onclick", "deleteRow(this)");

        var center = document.createElement('center');

        row.appendChild(col); // append first column to row
        row.appendChild(col2); // append second column to row
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(col5);
        row.appendChild(col6);
        row.appendChild(col7);
        row.appendChild(col8);
        row.appendChild(col9);
        col.appendChild(center);
        center.innerHTML = idcount; // put data in first column
        col2.innerHTML = brokenitem;
        col3.innerHTML = type;
        col4.innerHTML = count;
        col5.innerHTML = price;// put data in second column
        col6.innerHTML = sumCount;
        col6.value = sumCount;
        col7.innerHTML = brand;
        col8.appendChild(editbtn);
        col9.appendChild(deletebtn);
        var table = document.getElementById("tableToModify"); // find table to append to
        table.appendChild(row); // append row to table

        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "itemslist");
        input.setAttribute("value", [idcount, brokenitem, type, count, price, sumCount, 0]);
        table.appendChild(input);
        var totalPrice = document.getElementById("totalPrice");
        totalPrice.value = parseInt(totalPrice.value) + sumCount;

        $("#edit-btn" + idcount).hide();
        $("#del-btn" + idcount).hide();

    }

    function deleteRow(r) {

        var i = r.parentNode.parentNode.rowIndex;
        var split = r.id;
        split = split.substr(7, );
        var colPrice = document.getElementById("colPrice" + split).value;
        var decreasePrice = document.getElementById("totalPrice");
        decreasePrice.value = parseInt(decreasePrice.value) - colPrice;

        document.getElementById("mytable").deleteRow(i);
        idcount -= 1;

        var renum = 1;
        $("tr td center").each(function () {
            $(this).text(renum);
            renum++;
        });


    }

</script>
<script>
//  show / hide edit and delete button on table
    $(document).ready(function () {
        $('#mytable').mouseover(function () {
            $('.trow').hover(function () {
                var rowId = this.id;
                var split = rowId;
                split = split.substr(3, );
                $("#edit-btn" + split).show();
                $("#del-btn" + split).show();
            }, function () {
                var rowId = this.id;
                var split = rowId;
                split = split.substr(3, );
                $("#edit-btn" + split).hide();
                $("#del-btn" + split).hide();
            });
        });

        $('.datetime').change(function () {
            var oneDay = 24 * 60 * 60 * 1000;
            var selectDate = document.getElementById("datepick");
            var split = selectDate.value.substr(0, 10);
            var firstDate = new Date(split);
            var secondDate = new Date();
            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
            document.getElementById("dayleft").value = diffDays;
        });
    });

</script>

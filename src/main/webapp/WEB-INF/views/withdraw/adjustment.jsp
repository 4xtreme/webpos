<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<link href="${pageContext.request.contextPath}/css/categorie.css" rel="stylesheet">
 <!-- Page Content -->
     
   <div class="content-wrapper">

            
      <div class="container">
            <div class="jumbotron" style="width:90%;height: auto">
                    <input class="bbc-4" type="text" placeholder="Search.">
                    <div class="bbc-1"><a href="${pageContext.request.contextPath}/product/goods">
                            <button type="button" class="btn" style="color:white; background-color:#9ACD32">
                                <i class="fa fa-plus"></i> Add transfer orders
                            </button></a></div>
                   
                        <div class="bbc-2">Reason
                              <select >
                                <option value="a">All Reason</option>
                                <option value="a">Receive items</option>
                                <option value="a">Inventory count</option>
                                <option value="a">Loss</option>
                                <option value="a">Damage</option>
                              </select>
                        </div>
                        <div class="bbc-3">ร้านค้า
                              <select >
                                <option value="a">ร้านค้าทั้งหมด</option>
                              </select>
                        </div>
                        
               
               <br><br><br><br><br><br>
                    <div class="text-center"> <i class="fa fa-exclamation-triangle fa-5x" style="color:#FF3399"></i>
                        <br>  <h5 style="color:black">  ไม่มีข้อมูลที่จะแสดง </h5></div>
              </div>
        </div>
  </div>
      
  <jsp:include page="../footer_1.jsp" />
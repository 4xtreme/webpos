<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
 
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->

    <div class="content-wrapper">
      <div class="container">
          <div class="row">
          <div class="btn-group bck-1">
            <button type="button" class="btn">วันนี้</button>
            <button type="button" class="btn">เมื่อวานนี้</button>
            <button type="button" class="btn">สัปดาห์นี้</button>
            <button type="button" class="btn">เดือนนี้</button>
          </div>

            <div class="bck-2" id="widget">
                <div id="widgetField">
                    <span>28 July, 2008 - 31 July, 2008</span>
                    <a href="#">Select date range</a>
                </div>
                    <div id="widgetCalendar"></div>
            </div>     

             <div class="btn-group bck-4">
                <button class="btn" type="button" data-toggle="dropdown"> พนักงานทั้งหมด &nbsp;&nbsp;</button>
                    <ul class="dropdown-menu">
                       <li><a href="#" class="btn"><input name="checkbox[]" type="checkbox" id="checkbox" value="limit"/> พนักงานทั้งหมด</a></li>                
                    </ul>
            </div>
            <div class="multiselect  bck-3 ">
                <div class="selectBox" onclick="showCheckboxes()">
                      <select class="form-control">
                        <option>All reasons</option>
                      </select>
                      <div class="overSelect"></div>
                    </div>
                    <div id="checkboxes">
                      <label for="bb">
                        <input type="checkbox" id="bb" />Sold</label>
                      <label for="b">
                        <input type="checkbox" id="b" />Returned</label>
                      <label for="c">
                        <input type="checkbox" id="c" />Received</label>
                      <label for="d">
                        <input type="checkbox" id="d" />Transferred</label>
                      <label for="e">
                        <input type="checkbox" id="e" />Recounted</label>
                      <label for="f">
                        <input type="checkbox" id="f" />Damaged</label>
                      <label for="g">
                        <input type="checkbox" id="g" />Lost</label>
                      <label for="h">
                        <input type="checkbox" id="h" />Item edit</label>
                      <label for="i">
                        <input type="checkbox" id="i" />Production</label>
                    </div>
            </div>

        </div>               
                  
        <div class="container">
        <div>
        <div class="jumbotron" >    
            <div class="input"><input type="text" name="search" placeholder="Search.."></div>
            <div align="left"><h3>Inventory history</h3></div>
        </div>    
        </div>
          <script>
            var expanded = false;
                function showCheckboxes() {
                  var checkboxes = document.getElementById("checkboxes");
                  if (!expanded) {
                    checkboxes.style.display = "block";
                    expanded = true;
                  } else {
                    checkboxes.style.display = "none";
                    expanded = false;
                  }
                }
          </script>
          <style>
                .multiselect {
                  width: 180px;
                  
                }

                .selectBox {
                  position: relative;
                }

                .selectBox select {
                  width: 100%;
                  font-weight: bold;
                }

                .overSelect {
                  position: absolute;
                  left: 0;
                  right: 0;
                  top: 0;
                  bottom: 0;
                }

                #checkboxes {
                  display: none;
                  border: 1px #dadada solid;
                }

                #checkboxes label {
                  display: block;
                  background-color: #fff;
                }

                #checkboxes label:hover {
                  background-color: #1e90ff;
                }
          </style>
 


<jsp:include page="../footer_1.jsp" />
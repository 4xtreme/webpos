<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<link href="${pageContext.request.contextPath}/css/categorie.css" rel="stylesheet">
 <!-- Page Content -->
     
   <div class="content-wrapper">
           
      <div class="container">
            <div class="jumbotron" style="width:90%;height: auto">
                    <input class="bbl-4" type="text" placeholder="Search.">
                    <div class="bbl-1"><a href="#">
                            <button type="button" class="btn" style="color:white; background-color:#9ACD32">
                                <i class="fa fa-plus"></i> Add productions
                            </button></a></div>
                   
                        <div class="bbl-2">ชนิด
                              <select >
                                <option value="a">ทั้งหมด</option>
                                <option value="a">Production</option>
                                <option value="a">Disassembly</option>
                              </select>
                        </div>
                        <div class="bbl-3">ร้านค้า
                              <select >
                                <option value="a">ร้านค้าทั้งหมด</option>
                              </select>
                        </div>
                        
               
               <br><br><br><br><br><br>
                    <div class="text-center"> <i class="fa fa-exclamation-triangle fa-5x" style="color:#FF3399"></i>
                        <br>  <h5 style="color:black">  ไม่มีข้อมูลที่จะแสดง </h5></div>
              </div>
        </div>
  </div>
      
  <jsp:include page="../footer_1.jsp" />
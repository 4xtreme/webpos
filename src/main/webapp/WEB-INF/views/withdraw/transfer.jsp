<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header_1.jsp" />
<jsp:include page="../navigation_1.jsp" />
<link href="${pageContext.request.contextPath}/css/categorie.css" rel="stylesheet">
 <!-- Page Content -->
     
   <div class="content-wrapper">

            
      <div class="container">
            <div class="jumbotron" style="width:90%;height: auto">
                    <input class="bbk-5" type="text" placeholder="Search.">
                    <div class="bbk-1"><a href="#">
                            <button type="button" class="btn" style="color:white; background-color:#9ACD32">
                                <i class="fa fa-plus"></i> Add transfer orders
                            </button></a></div>
                   
                        <div class="bbk-2">สถานะ
                              <select >
                                <option value="a">ทั้งหมด</option>
                                <option value="a">In transit</option>
                                <option value="a">Transferred</option>
                              </select>
                        </div>
                        <div class="bbk-3">Source store
                              <select >
                                <option value="a">ร้านค้าทั้งหมด</option>
                              </select>
                        </div>
                         <div class="bbk-4">Destination store
                              <select>
                                <option value="a">ร้านค้าทั้งหมด</option>             
                              </select>
                         </div>
                         
               
               <br><br><br><br><br><br>
                    <div class="text-center"> <i class="fa fa-exclamation-triangle fa-5x" style="color:#FF3399"></i>
                        <br>  ไม่มีข้อมูลที่จะแสดง </div>
              </div>
        </div>
  </div>
      
  </div>
  <jsp:include page="../footer_1.jsp" />
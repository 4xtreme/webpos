<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<jsp:include page="../header_1.jsp" />
 
<jsp:include page="../navigation_1.jsp" />
<!-- Page Content -->

    <div class="content-wrapper">
      <div class="container">
          <div class="">
            <div class="multiselect bz1">
                    <div class="selectBox" onclick="showCheckboxes()">
                      <select class="form-control">
                        <option>All categories</option>
                      </select>
                      <div class="overSelect"></div>
                    </div>
                    <div id="checkboxes">
                      <label for="one">
                        <input type="checkbox" id="one" />a</label>
                      <label for="two">
                        <input type="checkbox" id="two" />b</label>
                      <label for="three">
                        <input type="checkbox" id="three" />c</label>
                    </div>
            </div>
             
            <div class="bz2">
                <button type="button" class="btn btn-default">ส่งออก</button>
            </div>

        </div>               
        <div class="container">
          <div class="jumbotron1">
              <div class="row " align="center">
                    <div class="bv3" >
                         <div class="bn" id="div4" >
                              <div>
                                  <h6>Total inventory value</h6>
                              </div>
                              <div>
                                  <h4>0.00</h4>
                              </div>
                              
                          </div>
                    </div>
                    <div class="bv3" >
                         <div class="bn" id="div4" >
                              <div>
                                  <h6>Total retail value</h6>
                              </div>
                              <div>
                                  <h4>0.00</h4>
                              </div>
                              
                          </div>
                    </div>
                    <div class="bv3" >
                         <div class="bn" id="div4" >
                              <div>
                                  <h6>Potential profit</h6>
                              </div>
                              <div>
                                  <h4>0.00</h4>
                              </div>
                              
                          </div>
                    </div>
                    <div class="bv3" >
                         <div class="bn" id="div4" >
                              <div>
                                  <h6>Margin</h6>
                              </div>
                              <div>
                                  <h4>0.00</h4>
                              </div>
                              
                          </div>
                    </div>
                </div>
          </div>
          </div>
        <div class="container">
        <div class="jumbotron2" >
           <div class="container">
             <table class="table table-responsive-md table-hover "  width="100%" >
                 <thead>
                        <tr>
                            <th width="400px"><div><h7>Item</h7></div></th>
                            <th><div align="center"><h7>In stock</h7></div></th>
                            <th><div align="center"><h7>Cost</h7></div></th>
                            <th><div align="center"><h7>Inventory value</h7></div></th>
                            <th><div align="center"><h7>Retail value</h7></div></th>
                            <th><div align="center"><h7>Potential profit</h7></div></th>
                            <th><div align="center"><h7>Margin</h7></div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><div align="center"></div></td>
                            <td><div align="center"></div></td>
                            <td><div align="center"></div></td>
                        
                        
                    </tbody>
            </table>
        
        </div>
            
            
        </div>
        </div> 
        <script>
            var expanded = false;
                function showCheckboxes() {
                  var checkboxes = document.getElementById("checkboxes");
                  if (!expanded) {
                    checkboxes.style.display = "block";
                    expanded = true;
                  } else {
                    checkboxes.style.display = "none";
                    expanded = false;
                  }
                }
          </script>
          <style>
                .multiselect {
                  width: 180px;
                }

                .selectBox {
                  position: relative;
                }

                .selectBox select {
                  width: 100%;
                  font-weight: bold;
                }

                .overSelect {
                  position: absolute;
                  left: 0;
                  right: 0;
                  top: 0;
                  bottom: 0;
                }

                #checkboxes {
                  display: none;
                  border: 1px #dadada solid;
                }

                #checkboxes label {
                  display: block;
                }

                #checkboxes label:hover {
                  background-color: #1e90ff;
                }
          </style> 


<jsp:include page="../footer_1.jsp" />